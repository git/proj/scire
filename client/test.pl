#!/usr/bin/perl

use strict;
use warnings;

$| = 1;

use Scire::Job;
use Scire::Communicator;

my $job = Scire::Job->new();
$job->load_jobfile("/tmp/scirejobs/queue/39.job");
#print $job->{script_data} . "\n";
$job->set_stdout_file("/tmp/scirejobs/result/39_stdout.txt");
$job->set_stderr_file("/tmp/scirejobs/result/39_stderr.txt");
$job->set_script_file("/tmp/scirejobs/run/runjob.sh");
my $exitcode = $job->run();
print "Job complete with exit code ${exitcode}\n";

exit;

my $comm = Scire::Communicator->new( host => "localhost" );
$comm->create_connection();
$comm->close_connection();
#my ($status, $message) = $comm->send_command("QUIT");
#print "$status\n";
