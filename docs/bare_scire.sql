use scire;
-- MySQL dump 10.10
--
-- Host: localhost    Database: scire
-- ------------------------------------------------------
-- Server version	5.0.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `GLI_profiles`
--

DROP TABLE IF EXISTS `GLI_profiles`;
CREATE TABLE `GLI_profiles` (
  `profileid` int(11) NOT NULL auto_increment,
  `profile_name` varchar(255) NOT NULL default '',
  `location` varchar(255) NOT NULL default '',
  `description` varchar(255) default NULL,
  PRIMARY KEY  (`profileid`),
  UNIQUE KEY `profile_name` (`profile_name`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `GLI_profiles`
--

LOCK TABLES `GLI_profiles` WRITE;
/*!40000 ALTER TABLE `GLI_profiles` DISABLE KEYS */;
INSERT INTO `GLI_profiles` VALUES (1,'printerserver','/scire/gli_profiles/printerserver.xml','A typical printer server install profile.  has like cups and shit.'),(2,'mta','/scire/gli_profiles/mta.xml','A typical MTA install profile.  has like very little except mailers.');
/*!40000 ALTER TABLE `GLI_profiles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `clientid` int(11) NOT NULL,
  `assetid` varchar(64) default NULL,
  `cert` text,
  `hostname` varchar(64) NOT NULL default '',
  `mac` varchar(17) NOT NULL default '',
  `ip` varchar(15) NOT NULL default '',
  `gli_profile` int(11) default NULL,
  `osid` int(11) default NULL,
  `status` varchar(20) default NULL,
  `contact` int(11) default NULL,
  `installtime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `digest` varchar(128) default NULL,
  PRIMARY KEY  (`clientid`),
  UNIQUE KEY `assetid` (`assetid`),
  UNIQUE KEY `digest` (`digest`),
  KEY `osid` (`osid`),
  KEY `gli_profile` (`gli_profile`),
  KEY `contact` (`contact`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_acl`
--

DROP TABLE IF EXISTS `gacl_acl`;
CREATE TABLE `gacl_acl` (
  `id` int(11) NOT NULL default '0',
  `section_value` varchar(230) NOT NULL default 'system',
  `allow` int(11) NOT NULL default '0',
  `enabled` int(11) NOT NULL default '0',
  `return_value` text,
  `note` text,
  `updated_date` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `gacl_enabled_acl` (`enabled`),
  KEY `gacl_section_value_acl` (`section_value`),
  KEY `gacl_updated_date_acl` (`updated_date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_acl`
--

LOCK TABLES `gacl_acl` WRITE;
/*!40000 ALTER TABLE `gacl_acl` DISABLE KEYS */;
INSERT INTO `gacl_acl` VALUES (3,'system',1,1,'1','',1156105535),(2,'system',1,1,'1','',1156105485),(4,'system',1,1,'1','',1156105622),(1,'system',1,1,'1','',1156105457),(6,'system',1,1,'1','',1156041570),(7,'system',1,1,'1','',1156105434),(5,'system',1,1,'1','',1156105677);
/*!40000 ALTER TABLE `gacl_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_acl_sections`
--

DROP TABLE IF EXISTS `gacl_acl_sections`;
CREATE TABLE `gacl_acl_sections` (
  `id` int(11) NOT NULL default '0',
  `value` varchar(230) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(230) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `gacl_value_acl_sections` (`value`),
  KEY `gacl_hidden_acl_sections` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_acl_sections`
--

LOCK TABLES `gacl_acl_sections` WRITE;
/*!40000 ALTER TABLE `gacl_acl_sections` DISABLE KEYS */;
INSERT INTO `gacl_acl_sections` VALUES (1,'system',1,'System',0);
/*!40000 ALTER TABLE `gacl_acl_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_acl_seq`
--

DROP TABLE IF EXISTS `gacl_acl_seq`;
CREATE TABLE `gacl_acl_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_acl_seq`
--

LOCK TABLES `gacl_acl_seq` WRITE;
/*!40000 ALTER TABLE `gacl_acl_seq` DISABLE KEYS */;
INSERT INTO `gacl_acl_seq` VALUES (8);
/*!40000 ALTER TABLE `gacl_acl_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aco`
--

DROP TABLE IF EXISTS `gacl_aco`;
CREATE TABLE `gacl_aco` (
  `id` int(11) NOT NULL default '0',
  `section_value` varchar(240) NOT NULL default '0',
  `value` varchar(240) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `gacl_section_value_value_aco` (`section_value`,`value`),
  KEY `gacl_hidden_aco` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aco`
--

LOCK TABLES `gacl_aco` WRITE;
/*!40000 ALTER TABLE `gacl_aco` DISABLE KEYS */;
INSERT INTO `gacl_aco` VALUES (13,'scire_ui','Edit User Info',1,'Edit User Info',0),(14,'scire_ui','Add User',1,'Add User',0),(15,'scire_ui','Add Client',1,'Add Client',0),(16,'scire_ui','View Clients',1,'View Clients',0),(17,'scire_ui','View Pending Clients',1,'View Pending Clients',0),(18,'scire_ui','Approve/Reject Pending Clients',1,'Approve/Reject Pending Clients',0),(19,'scire_ui','View Jobs',1,'View Jobs',0),(20,'scire_ui','Add Job',1,'Add Job',0);
/*!40000 ALTER TABLE `gacl_aco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aco_map`
--

DROP TABLE IF EXISTS `gacl_aco_map`;
CREATE TABLE `gacl_aco_map` (
  `acl_id` int(11) NOT NULL default '0',
  `section_value` varchar(230) NOT NULL default '0',
  `value` varchar(230) NOT NULL default '',
  PRIMARY KEY  (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aco_map`
--

LOCK TABLES `gacl_aco_map` WRITE;
/*!40000 ALTER TABLE `gacl_aco_map` DISABLE KEYS */;
INSERT INTO `gacl_aco_map` VALUES (6,'scire_ui','Add User'),(7,'scire_ui','Add Client'),(1,'scire_ui','View Clients'),(2,'scire_ui','View Pending Clients'),(3,'scire_ui','Approve/Reject Pending Clients'),(4,'scire_ui','View Jobs'),(5,'scire_ui','Add Job');
/*!40000 ALTER TABLE `gacl_aco_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aco_sections`
--

DROP TABLE IF EXISTS `gacl_aco_sections`;
CREATE TABLE `gacl_aco_sections` (
  `id` int(11) NOT NULL default '0',
  `value` varchar(230) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(230) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `gacl_value_aco_sections` (`value`),
  KEY `gacl_hidden_aco_sections` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aco_sections`
--

LOCK TABLES `gacl_aco_sections` WRITE;
/*!40000 ALTER TABLE `gacl_aco_sections` DISABLE KEYS */;
INSERT INTO `gacl_aco_sections` VALUES (1,'scire_ui',1,'scire_ui',0);
/*!40000 ALTER TABLE `gacl_aco_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aco_sections_seq`
--

DROP TABLE IF EXISTS `gacl_aco_sections_seq`;
CREATE TABLE `gacl_aco_sections_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aco_sections_seq`
--

LOCK TABLES `gacl_aco_sections_seq` WRITE;
/*!40000 ALTER TABLE `gacl_aco_sections_seq` DISABLE KEYS */;
INSERT INTO `gacl_aco_sections_seq` VALUES (2);
/*!40000 ALTER TABLE `gacl_aco_sections_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aco_seq`
--

DROP TABLE IF EXISTS `gacl_aco_seq`;
CREATE TABLE `gacl_aco_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aco_seq`
--

LOCK TABLES `gacl_aco_seq` WRITE;
/*!40000 ALTER TABLE `gacl_aco_seq` DISABLE KEYS */;
INSERT INTO `gacl_aco_seq` VALUES (21);
/*!40000 ALTER TABLE `gacl_aco_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro`
--

DROP TABLE IF EXISTS `gacl_aro`;
CREATE TABLE `gacl_aro` (
  `id` int(11) NOT NULL default '0',
  `section_value` varchar(240) NOT NULL default '0',
  `value` varchar(240) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `gacl_section_value_value_aro` (`section_value`,`value`),
  KEY `gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro`
--

LOCK TABLES `gacl_aro` WRITE;
/*!40000 ALTER TABLE `gacl_aro` DISABLE KEYS */;
INSERT INTO `gacl_aro` VALUES (1,'users','root',0,'root',0),(2,'users','codeman',1,'codeman',0);
/*!40000 ALTER TABLE `gacl_aro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro_groups`
--

DROP TABLE IF EXISTS `gacl_aro_groups`;
CREATE TABLE `gacl_aro_groups` (
  `id` int(11) NOT NULL default '0',
  `parent_id` int(11) NOT NULL default '0',
  `lft` int(11) NOT NULL default '0',
  `rgt` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`,`value`),
  UNIQUE KEY `gacl_value_aro_groups` (`value`),
  KEY `gacl_parent_id_aro_groups` (`parent_id`),
  KEY `gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro_groups`
--

LOCK TABLES `gacl_aro_groups` WRITE;
/*!40000 ALTER TABLE `gacl_aro_groups` DISABLE KEYS */;
INSERT INTO `gacl_aro_groups` VALUES (1,0,1,2,'Company Name','Company Name');
/*!40000 ALTER TABLE `gacl_aro_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro_groups_id_seq`
--

DROP TABLE IF EXISTS `gacl_aro_groups_id_seq`;
CREATE TABLE `gacl_aro_groups_id_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro_groups_id_seq`
--

LOCK TABLES `gacl_aro_groups_id_seq` WRITE;
/*!40000 ALTER TABLE `gacl_aro_groups_id_seq` DISABLE KEYS */;
INSERT INTO `gacl_aro_groups_id_seq` VALUES (2);
/*!40000 ALTER TABLE `gacl_aro_groups_id_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro_groups_map`
--

DROP TABLE IF EXISTS `gacl_aro_groups_map`;
CREATE TABLE `gacl_aro_groups_map` (
  `acl_id` int(11) NOT NULL default '0',
  `group_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`acl_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro_groups_map`
--

LOCK TABLES `gacl_aro_groups_map` WRITE;
/*!40000 ALTER TABLE `gacl_aro_groups_map` DISABLE KEYS */;
INSERT INTO `gacl_aro_groups_map` VALUES (1,1),(4,1);
/*!40000 ALTER TABLE `gacl_aro_groups_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro_map`
--

DROP TABLE IF EXISTS `gacl_aro_map`;
CREATE TABLE `gacl_aro_map` (
  `acl_id` int(11) NOT NULL default '0',
  `section_value` varchar(230) NOT NULL default '0',
  `value` varchar(230) NOT NULL default '',
  PRIMARY KEY  (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro_map`
--

LOCK TABLES `gacl_aro_map` WRITE;
/*!40000 ALTER TABLE `gacl_aro_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `gacl_aro_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro_sections`
--

DROP TABLE IF EXISTS `gacl_aro_sections`;
CREATE TABLE `gacl_aro_sections` (
  `id` int(11) NOT NULL default '0',
  `value` varchar(230) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(230) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `gacl_value_aro_sections` (`value`),
  KEY `gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro_sections`
--

LOCK TABLES `gacl_aro_sections` WRITE;
/*!40000 ALTER TABLE `gacl_aro_sections` DISABLE KEYS */;
INSERT INTO `gacl_aro_sections` VALUES (1,'users',1,'users',0);
/*!40000 ALTER TABLE `gacl_aro_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro_sections_seq`
--

DROP TABLE IF EXISTS `gacl_aro_sections_seq`;
CREATE TABLE `gacl_aro_sections_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro_sections_seq`
--

LOCK TABLES `gacl_aro_sections_seq` WRITE;
/*!40000 ALTER TABLE `gacl_aro_sections_seq` DISABLE KEYS */;
INSERT INTO `gacl_aro_sections_seq` VALUES (1);
/*!40000 ALTER TABLE `gacl_aro_sections_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_aro_seq`
--

DROP TABLE IF EXISTS `gacl_aro_seq`;
CREATE TABLE `gacl_aro_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_aro_seq`
--

LOCK TABLES `gacl_aro_seq` WRITE;
/*!40000 ALTER TABLE `gacl_aro_seq` DISABLE KEYS */;
INSERT INTO `gacl_aro_seq` VALUES (2);
/*!40000 ALTER TABLE `gacl_aro_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo`
--

DROP TABLE IF EXISTS `gacl_axo`;
CREATE TABLE `gacl_axo` (
  `id` int(11) NOT NULL default '0',
  `section_value` varchar(240) NOT NULL default '0',
  `value` varchar(240) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `gacl_section_value_value_axo` (`section_value`,`value`),
  KEY `gacl_hidden_axo` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo`
--

LOCK TABLES `gacl_axo` WRITE;
/*!40000 ALTER TABLE `gacl_axo` DISABLE KEYS */;
INSERT INTO `gacl_axo` VALUES (1,'scire_ui','scire_ui',1,'scire_ui',0);
/*!40000 ALTER TABLE `gacl_axo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo_groups`
--

DROP TABLE IF EXISTS `gacl_axo_groups`;
CREATE TABLE `gacl_axo_groups` (
  `id` int(11) NOT NULL default '0',
  `parent_id` int(11) NOT NULL default '0',
  `lft` int(11) NOT NULL default '0',
  `rgt` int(11) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`,`value`),
  UNIQUE KEY `gacl_value_axo_groups` (`value`),
  KEY `gacl_parent_id_axo_groups` (`parent_id`),
  KEY `gacl_lft_rgt_axo_groups` (`lft`,`rgt`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo_groups`
--

LOCK TABLES `gacl_axo_groups` WRITE;
/*!40000 ALTER TABLE `gacl_axo_groups` DISABLE KEYS */;
INSERT INTO `gacl_axo_groups` VALUES (1,0,1,2,'Company Name','Company Name');
/*!40000 ALTER TABLE `gacl_axo_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo_groups_id_seq`
--

DROP TABLE IF EXISTS `gacl_axo_groups_id_seq`;
CREATE TABLE `gacl_axo_groups_id_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo_groups_id_seq`
--

LOCK TABLES `gacl_axo_groups_id_seq` WRITE;
/*!40000 ALTER TABLE `gacl_axo_groups_id_seq` DISABLE KEYS */;
INSERT INTO `gacl_axo_groups_id_seq` VALUES (2);
/*!40000 ALTER TABLE `gacl_axo_groups_id_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo_groups_map`
--

DROP TABLE IF EXISTS `gacl_axo_groups_map`;
CREATE TABLE `gacl_axo_groups_map` (
  `acl_id` int(11) NOT NULL default '0',
  `group_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`acl_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo_groups_map`
--

LOCK TABLES `gacl_axo_groups_map` WRITE;
/*!40000 ALTER TABLE `gacl_axo_groups_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `gacl_axo_groups_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo_map`
--

DROP TABLE IF EXISTS `gacl_axo_map`;
CREATE TABLE `gacl_axo_map` (
  `acl_id` int(11) NOT NULL default '0',
  `section_value` varchar(230) NOT NULL default '0',
  `value` varchar(230) NOT NULL default '',
  PRIMARY KEY  (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo_map`
--

LOCK TABLES `gacl_axo_map` WRITE;
/*!40000 ALTER TABLE `gacl_axo_map` DISABLE KEYS */;
INSERT INTO `gacl_axo_map` VALUES (6,'scire_ui','scire_ui'),(7,'scire_ui','scire_ui'),(1,'scire_ui','scire_ui'),(2,'scire_ui','scire_ui'),(3,'scire_ui','scire_ui'),(4,'scire_ui','scire_ui'),(5,'scire_ui','scire_ui');
/*!40000 ALTER TABLE `gacl_axo_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo_sections`
--

DROP TABLE IF EXISTS `gacl_axo_sections`;
CREATE TABLE `gacl_axo_sections` (
  `id` int(11) NOT NULL default '0',
  `value` varchar(230) NOT NULL default '',
  `order_value` int(11) NOT NULL default '0',
  `name` varchar(230) NOT NULL default '',
  `hidden` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `gacl_value_axo_sections` (`value`),
  KEY `gacl_hidden_axo_sections` (`hidden`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo_sections`
--

LOCK TABLES `gacl_axo_sections` WRITE;
/*!40000 ALTER TABLE `gacl_axo_sections` DISABLE KEYS */;
INSERT INTO `gacl_axo_sections` VALUES (2,'clients',1,'clients',0),(1,'scire_ui',2,'scire_ui',0);
/*!40000 ALTER TABLE `gacl_axo_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo_sections_seq`
--

DROP TABLE IF EXISTS `gacl_axo_sections_seq`;
CREATE TABLE `gacl_axo_sections_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo_sections_seq`
--

LOCK TABLES `gacl_axo_sections_seq` WRITE;
/*!40000 ALTER TABLE `gacl_axo_sections_seq` DISABLE KEYS */;
INSERT INTO `gacl_axo_sections_seq` VALUES (3);
/*!40000 ALTER TABLE `gacl_axo_sections_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_axo_seq`
--

DROP TABLE IF EXISTS `gacl_axo_seq`;
CREATE TABLE `gacl_axo_seq` (
  `id` int(11) NOT NULL default '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_axo_seq`
--

LOCK TABLES `gacl_axo_seq` WRITE;
/*!40000 ALTER TABLE `gacl_axo_seq` DISABLE KEYS */;
INSERT INTO `gacl_axo_seq` VALUES (2);
/*!40000 ALTER TABLE `gacl_axo_seq` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_groups_aro_map`
--

DROP TABLE IF EXISTS `gacl_groups_aro_map`;
CREATE TABLE `gacl_groups_aro_map` (
  `group_id` int(11) NOT NULL default '0',
  `aro_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`group_id`,`aro_id`),
  KEY `gacl_aro_id` (`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_groups_aro_map`
--

LOCK TABLES `gacl_groups_aro_map` WRITE;
/*!40000 ALTER TABLE `gacl_groups_aro_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `gacl_groups_aro_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_groups_axo_map`
--

DROP TABLE IF EXISTS `gacl_groups_axo_map`;
CREATE TABLE `gacl_groups_axo_map` (
  `group_id` int(11) NOT NULL default '0',
  `axo_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`group_id`,`axo_id`),
  KEY `gacl_axo_id` (`axo_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_groups_axo_map`
--

LOCK TABLES `gacl_groups_axo_map` WRITE;
/*!40000 ALTER TABLE `gacl_groups_axo_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `gacl_groups_axo_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gacl_phpgacl`
--

DROP TABLE IF EXISTS `gacl_phpgacl`;
CREATE TABLE `gacl_phpgacl` (
  `name` varchar(230) NOT NULL default '',
  `value` varchar(230) NOT NULL default '',
  PRIMARY KEY  (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gacl_phpgacl`
--

LOCK TABLES `gacl_phpgacl` WRITE;
/*!40000 ALTER TABLE `gacl_phpgacl` DISABLE KEYS */;
INSERT INTO `gacl_phpgacl` VALUES ('version','3.3.6'),('schema_version','2.1');
/*!40000 ALTER TABLE `gacl_phpgacl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hardware`
--

DROP TABLE IF EXISTS `hardware`;
CREATE TABLE `hardware` (
  `clientid` int(11) NOT NULL default '0',
  `processor` varchar(32) default NULL,
  `memory` varchar(32) default NULL,
  `hd` varchar(32) default NULL,
  `cpu` varchar(64) default NULL,
  `mhz` varchar(32) default NULL,
  PRIMARY KEY  (`clientid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hardware`
--

LOCK TABLES `hardware` WRITE;
/*!40000 ALTER TABLE `hardware` DISABLE KEYS */;
/*!40000 ALTER TABLE `hardware` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hardware_history`
--

DROP TABLE IF EXISTS `hardware_history`;
CREATE TABLE `hardware_history` (
  `clientid` int(11) NOT NULL default '0',
  `changedate` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `field_name` varchar(30) default NULL,
  `oldvalue` varchar(255) default NULL,
  `newvalue` varchar(255) default NULL,
  PRIMARY KEY  (`clientid`,`changedate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hardware_history`
--

LOCK TABLES `hardware_history` WRITE;
/*!40000 ALTER TABLE `hardware_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `hardware_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_conditions`
--

DROP TABLE IF EXISTS `job_conditions`;
CREATE TABLE `job_conditions` (
  `jobid` int(11) NOT NULL default '0',
  `clientid` int(11) NOT NULL default '0',
  `job_dependency` int(11) NOT NULL default '0',
  `start_time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `start_period` timestamp NOT NULL default '0000-00-00 00:00:00',
  `end_period` timestamp NOT NULL default '0000-00-00 00:00:00',
  `run_interval` int(11) default NULL,
  `last_run` int(11) default NULL,
  `last_run_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`jobid`,`clientid`),
  KEY `clientid` (`clientid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_conditions`
--

LOCK TABLES `job_conditions` WRITE;
/*!40000 ALTER TABLE `job_conditions` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_conditions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_history`
--

DROP TABLE IF EXISTS `job_history`;
CREATE TABLE `job_history` (
  `jobid` int(11) NOT NULL default '0',
  `clientid` int(11) NOT NULL default '0',
  `eventtime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `status` varchar(20) NOT NULL default '',
  `eventmsg` varchar(255) default NULL,
  PRIMARY KEY  (`jobid`,`clientid`,`eventtime`),
  KEY `clientid` (`clientid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_history`
--

LOCK TABLES `job_history` WRITE;
/*!40000 ALTER TABLE `job_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `job_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `jobid` int(11) NOT NULL auto_increment,
  `priority` int(11) NOT NULL default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `creator` int(11) NOT NULL default '0',
  `permission` int(11) NOT NULL default '0',
  `script` int(11) NOT NULL default '0',
  `description` varchar(255) default NULL,
  `pending` int(11) default NULL,
  `failed` int(11) default NULL,
  PRIMARY KEY  (`jobid`),
  KEY `creator` (`creator`),
  KEY `permission` (`permission`),
  KEY `script` (`script`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs_clients`
--

DROP TABLE IF EXISTS `jobs_clients`;
CREATE TABLE `jobs_clients` (
  `jobid` int(11) NOT NULL default '0',
  `clientid` int(11) NOT NULL default '0',
  `groupid` int(11) NOT NULL default '0',
  PRIMARY KEY  (`jobid`,`clientid`,`groupid`),
  KEY `groupid` (`groupid`),
  KEY `clientid` (`clientid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs_clients`
--

LOCK TABLES `jobs_clients` WRITE;
/*!40000 ALTER TABLE `jobs_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL default '0',
  `name` varchar(50) default NULL,
  `long_name` varchar(150) default NULL,
  `description` varchar(250) default NULL,
  `path` varchar(90) default NULL,
  `author` varchar(50) default NULL,
  `distribution` varchar(50) default NULL,
  `category` varchar(30) default NULL,
  `homepage` varchar(90) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'Quickstart','Quickstart Installation Mangement Daemon','Description goes here and all that good stuff.','quickstart/index.php','Preston Cody','gentoo','deployment','http://www.gentoo.org/proj/en/releng/gli/'),(2,'Exporter','Server Information Exporter','This is a utility that will take selected data from the central server about machines and export it into an XML format.','/Exporter/index.php','Preston Cody',NULL,'information','http://www.gentoo.org/proj/en/releng/gli/');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `os`
--

DROP TABLE IF EXISTS `os`;
CREATE TABLE `os` (
  `osid` int(11) NOT NULL auto_increment,
  `osname` varchar(128) NOT NULL default '',
  `update_script` int(11) default NULL,
  `install_script` int(11) default NULL,
  `uninstall_script` int(11) default NULL,
  `rollback_script` int(11) default NULL,
  `packagelist_script` int(11) default NULL,
  PRIMARY KEY  (`osid`),
  KEY `update_script` (`update_script`),
  KEY `install_script` (`install_script`),
  KEY `uninstall_script` (`uninstall_script`),
  KEY `rollback_script` (`rollback_script`),
  KEY `packagelist_script` (`packagelist_script`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `os`
--

LOCK TABLES `os` WRITE;
/*!40000 ALTER TABLE `os` DISABLE KEYS */;
INSERT INTO `os` VALUES (1,'Gentoo Linux',100,101,102,103,104);
/*!40000 ALTER TABLE `os` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `permid` int(11) NOT NULL,
  `name` varchar(128) NOT NULL default '',
  `description` varchar(255) default NULL,
  `creator` int(11) NOT NULL default '0',
  `created` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `permcategory` varchar(128) NOT NULL default 'Default',
  PRIMARY KEY  (`permid`),
  UNIQUE KEY `name` (`name`),
  KEY `creator` (`creator`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `scripts`
--

DROP TABLE IF EXISTS `scripts`;
CREATE TABLE `scripts` (
  `scriptid` int(11) NOT NULL auto_increment,
  `name` varchar(128) NOT NULL default '',
  `description` varchar(255) default NULL,
  `location` varchar(255) default NULL,
  `script_data` text,
  `log_location` varchar(255) default NULL,
  `success_code` varchar(32) default NULL,
  `run_as` varchar(255) default NULL,
  `priority` int(11) default NULL,
  `permission` int(11) default NULL,
  `pp_location` varchar(255) default NULL,
  `pp_script_data` text,
  PRIMARY KEY  (`scriptid`),
  KEY `permission` (`permission`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `scripts`
--

LOCK TABLES `scripts` WRITE;
/*!40000 ALTER TABLE `scripts` DISABLE KEYS */;
INSERT INTO `scripts` VALUES (100,'Gentoo Update Script','This script will run emerge sync and emerge -u world, logging the results.','','#!/bin/bash \n emerge sync \n emerge -u world','/var/log/scire/updatelog.txt','0','root',5,1,'',''),(101,'Gentoo Install Package Script','This script will run emerge on a given package and log the results.','','#!/bin/bash\nemerge --nocolor --nospinner --quiet -u world','/var/log/scire/installlog.txt','0','root',2,1,'',''),(102,'Gentoo Uninstall Package Script','This script will run emerge unmerge on a given package and log the results.','','#!/bin/bash \n emerge unmerge','/var/log/scire/uninstalllog.txt','0','root',2,1,'',''),(103,'Gentoo Rollback Script','This script will roll back a given package to its previous version, logging the results.','','#!/bin/bash \n emerge -C && emerge ','/var/log/scire/rollbacklog.txt','0','root',1,1,'',''),(104,'Gentoo PackageList Script','This script will return a list of packages installed on the system and the current version of those packages.','','#!/bin/bash \n %%tag1%% %%tag2%%','/var/log/scire/packagelistlog.txt','0','nobody',8,1,'',''),(105,'install_vim','This will emerge vim for gentoo','Embedded','#!/bin/bash\r\nemerge vim\r\n	','','0','root',1,0,'Embedded',''),(106,'install ufed','this installs ufed, a stupid little prog that helps with USE flags','Embedded','#!/bin/bash\r\nemerge ufed','','0','root',1,0,'Embedded','');
/*!40000 ALTER TABLE `scripts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `sessionid` varchar(255) NOT NULL default '',
  `expiration` int(10) unsigned NOT NULL default '0',
  `data` text,
  PRIMARY KEY  (`sessionid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `userid` int(11) NOT NULL default '0',
  `setting_name` varchar(64) NOT NULL default '',
  `setting_value` varchar(255) default NULL,
  PRIMARY KEY  (`userid`,`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'start_page','clients.php'),(1,'theme','std_grey'),(1,'notification','phone'),(1,'per_page','2'),(1,'clients_page_sorton','hostname'),(1,'clients_page_fields','ip,osname,username,groups,hostname,status'),(1,'jobs_page_fields','hostname,priority,created,description,username,scriptname,pending,permname,failed'),(1,'users_page_fields','username,real_name,comment,groups,status'),(1,'users_page_sorton','username'),(1,'jobs_page_sorton','hostname'),(1,'jobs_page_sortdir','asc');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software`
--

DROP TABLE IF EXISTS `software`;
CREATE TABLE `software` (
  `clientid` int(11) NOT NULL default '0',
  `package` varchar(128) NOT NULL default '',
  `current_ver` varchar(64) default NULL,
  `rollback_ver` varchar(64) default NULL,
  PRIMARY KEY  (`package`),
  KEY `clientid` (`clientid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `software`
--

LOCK TABLES `software` WRITE;
/*!40000 ALTER TABLE `software` DISABLE KEYS */;
/*!40000 ALTER TABLE `software` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(128) default NULL,
  `pager` varchar(128) default NULL,
  `real_name` varchar(255) default NULL,
  `comment` varchar(255) default NULL,
  PRIMARY KEY  (`userid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'root','444mwGdhEKuO.','root@localhost.com','','','Root User','This is the comment for the root user.'),(2,'codeman','444mwGdhEKuO.','codeman@localhost.com','','555-1212','Preston Cody','Project lead and developer.');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2007-03-26  0:59:36
