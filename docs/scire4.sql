DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `sessionid` varchar(255) character set utf8 collate utf8_bin NOT NULL default '',
  `expiration` int(10) unsigned NOT NULL default '0',
  `data` text,
  PRIMARY KEY  (`sessionid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
