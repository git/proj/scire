use moria;
INSERT INTO modules values (1,'GLIMD','Gentoo Linux Installation Mangement Daemon','Description goes here and all that good stuff.','glimd.txt','Preston Cody','gentoo','deployment','http://www.gentoo.org/proj/en/releng/gli/');
INSERT INTO modules values (2,'Exporter','Server Information Exporter','This is a utility that will take selected data from the central server about machines and export it into an XML format.','Exporter.php','Preston Cody',NULL,'information','http://www.gentoo.org/proj/en/releng/gli/');

INSERT INTO clients values (1, 'testhostlead', '00:0a:0b:0c:0d:0e', '192.168.3.4', 'printerserver', 'gentoo', 'Preston Cody', 'Installed');

INSERT INTO clients values (2, 'testhostslave', '00:0a:0a:0a:0d:0e', '192.168.3.5', 'printerserver', 'gentoo', 'Preston Cody', 'Not Installed');

INSERT INTO clients values (3, 'testhostmtalead', '0b:0b:0b:0c:0d:0e', '192.168.3.6', 'mta', 'gentoo', 'Preston Cody', 'Installed');

INSERT INTO clients values (4, 'testhostmtaslave', '0c:0c:0c:0c:0d:0e', '192.168.3.7', 'mta', 'gentoo', 'Preston Cody', 'Installed');

INSERT INTO profiles values ('printerserver', 'printerserverprofile.xml', 'Printer Servers use this profile', 'gentoo');

INSERT INTO profiles values ('mta', 'mtaprofile.xml', 'MTAs use this profile', 'gentoo');

INSERT INTO users values ('root','444mwGdhEKuO.','Root User');

INSERT INTO jobs values  (1, 1, 'Update', 5, 'emerge sync', NOW(), NOW(), NOW(), NOW(), 'Scheduled', 'root', 'Regular Update');
INSERT INTO jobs values  (2, 1, 'Install Packages', 5, 'emerge cups', NOW(), NOW(), NOW(), NOW(), 'In Progress', 'root', 'Adding cups');
INSERT INTO jobs values  (3, 2, 'Install Packages', 6, 'emerge cups', NOW(), NOW(), NOW(), NOW(), 'Failed', 'root', 'Adding cups');
INSERT INTO jobs values  (4, 3, 'Update', 2, 'glsa-check', NOW(), NOW(), NOW(), NOW(), 'In Progress', 'root', 'Security Update');
INSERT INTO jobs values  (5, 3, 'Install Packages', 5, 'emerge postrix', NOW(), NOW(), NOW(), NOW(), 'Scheduled', 'root', 'Adding postfix, cause its an mta duh.');
