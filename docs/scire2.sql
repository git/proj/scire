use scire;

INSERT INTO users (userid, username, password, email, phone, pager, real_name, comment) values (1, 'root','444mwGdhEKuO.', 'root@localhost.com', '', '', 'Root User', 'This is the comment for the root user.');
INSERT INTO users (userid, username, password, email, phone, pager, real_name, comment) values (2, 'codeman','444mwGdhEKuO.', 'codeman@localhost.com', '', '', 'Preston Cody', 'Project lead and developer.');

INSERT INTO client_status (statusid, statusname) values (1, 'Pending');
INSERT INTO client_status (statusid, statusname) values (2, 'Active');
INSERT INTO client_status (statusid, statusname) values (3, 'Inactive');
INSERT INTO client_status (statusid, statusname) values (4, 'Rejected');
INSERT INTO client_status (statusid, statusname) values (5, 'Retired');

INSERT INTO jobs_status (statusid, statusname) values (1, 'Pending');
INSERT INTO jobs_status (statusid, statusname) values (2, 'Downloaded');
INSERT INTO jobs_status (statusid, statusname) values (3, 'Running');
INSERT INTO jobs_status (statusid, statusname) values (4, 'Failed');
INSERT INTO jobs_status (statusid, statusname) values (5, 'Finished');
INSERT INTO jobs_status (statusid, statusname) values (6, 'Cancelled');

INSERT INTO `permissions` VALUES (0,'View Users','Able to list the users',1,'2007-03-26 01:29:28','Default'),(23,'Test Permission 1','this is only a test.',1,'2007-03-26 02:20:40','Default'),(24,'Add ClientGroup','In the clients section, lets you add a group of clients.',2,'2007-04-01 17:03:11','Default'),(25,'Edit ClientGroup','Allows editing of the name and parent of a clientgroup.',2,'2007-04-01 17:07:32','Default'),(26,'Delete ClientGroup','deleting of a clientgroup.  clients within move to parent.',2,'2007-04-01 17:08:39','Default'),(27,'Assign Permission','allows assigning permissions of users to clients.  This is a very important permission!',2,'2007-04-01 17:52:25','Default'),(28,'View Permissions','see the list of active permissions.',2,'2007-04-01 17:53:17','Default'),(29,'Edit Client','edit client information.',2,'2007-04-01 18:10:02','Default'),(30,'Delete Client','remove a client.',2,'2007-04-01 18:10:21','Default'),(31,'Change User Pass/Name','If set, the ability to change a user\'s password will be enabled.',1,'2007-04-01 21:16:08','Default'),(32,'Add UserGroup','allows creation of usergroups.',1,'2007-04-01 23:18:59','Default');

INSERT INTO `scripts` VALUES (100,'Gentoo Update Script','This script will run emerge sync and emerge -u world, logging the results.','','#!/bin/bash \n emerge sync \n emerge -u world','/var/log/scire/updatelog.txt','0','root',5,1,'','',0),(101,'Gentoo Install Package Script','This script will run emerge on a given package and log the results.','','#!/bin/bash\nemerge --nocolor --nospinner --quiet -u world','/var/log/scire/installlog.txt','0','root',2,1,'','',0),(102,'Gentoo Uninstall Package Script','This script will run emerge unmerge on a given package and log the results.','','#!/bin/bash \n emerge unmerge','/var/log/scire/uninstalllog.txt','0','root',2,1,'','',0),(103,'Gentoo Rollback Script','This script will roll back a given package to its previous version, logging the results.','','#!/bin/bash \n emerge -C && emerge ','/var/log/scire/rollbacklog.txt','0','root',1,1,'','',0),(104,'Gentoo PackageList Script','This script will return a list of packages installed on the system and the current version of those packages.','','#!/bin/bash \n %%tag1%% %%tag2%%','/var/log/scire/packagelistlog.txt','0','nobody',8,1,'','',0),(105,'install_vim','This will emerge vim for gentoo','Embedded','#!/bin/bash\r\nemerge vim\r\n	','','0','root',1,0,'Embedded','',0),(106,'install ufed','this installs ufed, a stupid little prog that helps with USE flags','Embedded','#!/bin/bash\r\nemerge ufed','','0','root',1,0,'Embedded','',0),(107,'get package list','lists all installed gentoo packages and their versions.  stores this info in the DB', 'Embedded', '#!/bin/bash\r\nfind /var/db/pkg -mindepth 2 -maxdepth 2 -type d | sed \'s:^/var/db/pkg/::;s:-\([0-9.]\+.*$\,0):|\1:\'','','0','nobody',5,0,'get_package_list','',0);

INSERT INTO os (osid, osname, update_script, install_script, uninstall_script, rollback_script, packagelist_script) values (1,'Gentoo Linux',100 ,101 ,102 ,103, 104 );

INSERT INTO GLI_profiles (profileid, profile_name, location, description) values (1, 'printerserver', '/scire/gli_profiles/printerserver.xml', 'A typical printer server install profile.  has like cups and shit.');
INSERT INTO GLI_profiles (profileid, profile_name, location, description) values (2, 'mta', '/scire/gli_profiles/mta.xml', 'A typical MTA install profile.  has like very little except mailers.');

INSERT INTO `clients` VALUES (3,'ASSET02','sampledigest','testclient','00:01:02:03:04:0A','1.2.3.4',1,1,2,2,'0000-00-00 00:00:00'),(5,'ASSET05','testdigest','testclient2','00:01:02:03:04:05','192.168.1.11',1,1,2,1,'0000-00-00 00:00:00');

INSERT INTO jobs (jobid, priority, created, creator, permission, script, description, pending, failed) values  (1, 1, NOW(), 2, 1, 100, 'Upgrading a package on mtalead', 1,0);
INSERT INTO jobs_clients (jobid, clientid) values (1, 3);
INSERT INTO job_conditions (jobid, job_dependency, run_schedule, validity_period) values (1, 0, '*/15 * * * *', 4);
INSERT INTO jobs_seq VALUES (10);
INSERT INTO scripts_seq VALUES (10);


INSERT INTO settings (userid, setting_name, setting_value) values (1, 'start_page', 'users.php');
INSERT INTO settings (userid, setting_name, setting_value) values (1, 'theme', 'light_blue');
INSERT INTO settings (userid, setting_name, setting_value) values (2, 'clients_page_fields', 'hostname,ip,profile_name,osname,username,installtime,status');


#INSERT INTO jobs values  (2, 1, 'Install Packages', 5, 'emerge cups', NOW(), NOW(), NOW(), NOW(), 3, 'root', 'Adding cups');
#INSERT INTO jobs values  (3, 2, 'Install Packages', 6, 'emerge cups', NOW(), NOW(), NOW(), NOW(), 4, 'root', 'Adding cups');
#INSERT INTO jobs values  (4, 3, 'Update', 2, 'glsa-check', NOW(), NOW(), NOW(), NOW(), 3, 'root', 'Security Update');
#INSERT INTO jobs values  (5, 3, 'Install Packages', 5, 'emerge postfix', NOW(), NOW(), NOW(), NOW(), 1, 'root', 'Adding postfix, cause its an mta duh.');
#INSERT INTO modules values (1,'Quickstart','Quickstart Installation Mangement Daemon','Description goes here and all that good stuff.','quickstart/index.php','Preston Cody','gentoo','deployment','http://www.gentoo.org/proj/en/releng/gli/');
#INSERT INTO modules values (2,'Exporter','Server Information Exporter','This is a utility that will take selected data from the central server about machines and export it into an XML format.','Exporter/index.php','Preston Cody',NULL,'information','http://www.gentoo.org/proj/en/releng/gli/');
