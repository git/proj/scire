drop table if exists user_pref;
create table user_pref (
user_id INT NOT NULL,
jobs_layout VARCHAR(250),
jobs_fields VARCHAR(250),
PRIMARY KEY (user_id)
) Type=MyISAM;
--example. user_id = 1234
-- jobs_layout = csv of field_ids = "5,7,8,1,2,55,24"
-- jobs_fields = csv of sort order+direction. 0 = not sorted.  = "0a,0a,1d,3a,2d,0a,0a"

drop table if exists jobs_fields;
create table jobs_fields (
field_id INT NOT NULL,
field_name VARCHAR(50),
field_desc VARCHAR(200),
PRIMARY KEY (field_id)
) Type=MyISAM;
--example (5, "Priority", "Shows the priority of the job.")

