use scire;
--
-- Dumping data for table `gacl_acl`
--

LOCK TABLES `gacl_acl` WRITE;
/*!40000 ALTER TABLE `gacl_acl` DISABLE KEYS */;
INSERT INTO `gacl_acl` VALUES (3,'system',1,1,'1','',1156105535),(14,'system',1,1,'1','',1175446075),(4,'system',1,1,'1','',1156105622),(1,'system',1,1,'1','',1156105457),(6,'system',1,1,'1','',1156041570),(7,'system',1,1,'1','',1175469155),(5,'system',1,1,'1','',1156105677),(9,'system',1,1,'1','',1174872568),(13,'system',1,1,'1','',1175444039),(15,'system',1,1,'1','',1175446991),(16,'system',1,1,'1','',1175447252),(17,'system',1,1,'1','',1175447319),(18,'system',1,1,'1','',1175449945),(19,'system',1,1,'1','',1175449997),(20,'system',1,1,'1','',1175451002),(21,'system',1,1,'1','',1175451021),(22,'system',1,1,'1','',1175451722),(23,'system',1,1,'1','',1175462168),(24,'system',1,1,'1','',1175469539);
/*!40000 ALTER TABLE `gacl_acl` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `gacl_aco` WRITE;
/*!40000 ALTER TABLE `gacl_aco` DISABLE KEYS */;
INSERT INTO `gacl_aco` VALUES (13,'scire_ui','Edit User Info',1,'Edit User',0),(14,'scire_ui','Add User',1,'Add User',0),(15,'scire_ui','Add Client',1,'Add Client',0),(16,'scire_ui','View Clients',1,'View Clients',0),(17,'scire_ui','View Pending Clients',1,'View Pending Clients',0),(18,'scire_ui','Approve/Reject Pending Clients',1,'Approve/Reject Pending Clients',0),(19,'scire_ui','View Jobs',1,'View Jobs',0),(20,'scire_ui','Add Job',1,'Add Job',0),(22,'scire_ui','View Users',1,'View Users',0),(23,'Tests','Test Permission 1',1,'Test Permission 1',0),(24,'scire_ui','Add ClientGroup',1,'Add ClientGroup',0),(25,'scire_ui','Edit ClientGroup',1,'Edit ClientGroup',0),(26,'scire_ui','Delete ClientGroup',1,'Delete ClientGroup',0),(27,'scire_ui','Assign Permission',1,'Assign Permission',0),(28,'scire_ui','View Permissions',1,'View Permissions',0),(29,'scire_ui','Edit Client',1,'Edit Client',0),(30,'scire_ui','Delete Client',1,'Delete Client',0),(31,'scire_ui','Change User Pass/Name',1,'Change User Pass/Name',0),(32,'scire_ui','Add UserGroup',1,'Add UserGroup',0);
/*!40000 ALTER TABLE `gacl_aco` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_aco_map` WRITE;
/*!40000 ALTER TABLE `gacl_aco_map` DISABLE KEYS */;
INSERT INTO `gacl_aco_map` VALUES (1,'scire_ui','View Clients'),(3,'scire_ui','Approve/Reject Pending Clients'),(4,'scire_ui','View Jobs'),(5,'scire_ui','Add Job'),(6,'scire_ui','Add User'),(7,'scire_ui','Add Client'),(9,'scire_ui','View Users'),(13,'scire_ui','Add Client'),(14,'scire_ui','View Pending Clients'),(15,'scire_ui','Add ClientGroup'),(16,'scire_ui','Edit ClientGroup'),(17,'scire_ui','Delete ClientGroup'),(18,'scire_ui','Assign Permission'),(19,'scire_ui','View Permissions'),(20,'scire_ui','Edit Client'),(21,'scire_ui','Delete Client'),(22,'scire_ui','View Clients'),(23,'scire_ui','Change User Pass/Name'),(24,'scire_ui','Add UserGroup');
/*!40000 ALTER TABLE `gacl_aco_map` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_aco_sections` WRITE;
/*!40000 ALTER TABLE `gacl_aco_sections` DISABLE KEYS */;
INSERT INTO `gacl_aco_sections` VALUES (1,'scire_ui',1,'scire_ui',0),(3,'Tests',1,'Tests',0);
/*!40000 ALTER TABLE `gacl_aco_sections` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `gacl_aro` WRITE;
/*!40000 ALTER TABLE `gacl_aro` DISABLE KEYS */;
INSERT INTO `gacl_aro` VALUES (1,'users','root',0,'root',0),(2,'users','codeman',1,'codeman',0);
/*!40000 ALTER TABLE `gacl_aro` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `gacl_aro_map` WRITE;
/*!40000 ALTER TABLE `gacl_aro_map` DISABLE KEYS */;
INSERT INTO `gacl_aro_map` VALUES (7,'users','codeman'),(14,'users','codeman'),(22,'users','codeman'),(24,'users','codeman');
/*!40000 ALTER TABLE `gacl_aro_map` ENABLE KEYS */;
UNLOCK TABLES;

LOCK TABLES `gacl_aro_groups` WRITE;
/*!40000 ALTER TABLE `gacl_aro_groups` DISABLE KEYS */;
INSERT INTO `gacl_aro_groups` VALUES (1,0,1,4,'Company Name','Company Name'),(3,1,2,3,'test','test');
/*!40000 ALTER TABLE `gacl_aro_groups` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `gacl_aro_groups_map` WRITE;
/*!40000 ALTER TABLE `gacl_aro_groups_map` DISABLE KEYS */;
INSERT INTO `gacl_aro_groups_map` VALUES (1,1),(4,1),(9,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(20,1),(21,1),(24,1);
/*!40000 ALTER TABLE `gacl_aro_groups_map` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_aro_sections` WRITE;
/*!40000 ALTER TABLE `gacl_aro_sections` DISABLE KEYS */;
INSERT INTO `gacl_aro_sections` VALUES (1,'users',1,'users',0);
/*!40000 ALTER TABLE `gacl_aro_sections` ENABLE KEYS */;
UNLOCK TABLES;




LOCK TABLES `gacl_axo` WRITE;
/*!40000 ALTER TABLE `gacl_axo` DISABLE KEYS */;
INSERT INTO `gacl_axo` VALUES (1,'scire_ui','scire_ui',1,'scire_ui',0),(3,'clients','testclient',1,'testclient',0),(5,'clients','testclient2',1,'testclient2',0);
/*!40000 ALTER TABLE `gacl_axo` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_axo_groups` WRITE;
/*!40000 ALTER TABLE `gacl_axo_groups` DISABLE KEYS */;
INSERT INTO `gacl_axo_groups` VALUES (1,0,1,2,'Company Name','Company Name');
/*!40000 ALTER TABLE `gacl_axo_groups` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_axo_map` WRITE;
/*!40000 ALTER TABLE `gacl_axo_map` DISABLE KEYS */;
INSERT INTO `gacl_axo_map` VALUES (1,'scire_ui','scire_ui'),(3,'scire_ui','scire_ui'),(4,'scire_ui','scire_ui'),(5,'scire_ui','scire_ui'),(6,'scire_ui','scire_ui'),(7,'scire_ui','scire_ui'),(9,'scire_ui','scire_ui'),(13,'scire_ui','scire_ui'),(14,'scire_ui','scire_ui'),(15,'scire_ui','scire_ui'),(16,'scire_ui','scire_ui'),(17,'scire_ui','scire_ui'),(18,'scire_ui','scire_ui'),(19,'scire_ui','scire_ui'),(20,'scire_ui','scire_ui'),(21,'scire_ui','scire_ui'),(22,'scire_ui','scire_ui'),(23,'scire_ui','scire_ui'),(24,'scire_ui','scire_ui');
/*!40000 ALTER TABLE `gacl_axo_map` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_axo_sections` WRITE;
/*!40000 ALTER TABLE `gacl_axo_sections` DISABLE KEYS */;
INSERT INTO `gacl_axo_sections` VALUES (2,'clients',1,'clients',0),(1,'scire_ui',2,'scire_ui',0);
/*!40000 ALTER TABLE `gacl_axo_sections` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_groups_aro_map` WRITE;
/*!40000 ALTER TABLE `gacl_groups_aro_map` DISABLE KEYS */;
INSERT INTO `gacl_groups_aro_map` VALUES (1,1);
/*!40000 ALTER TABLE `gacl_groups_aro_map` ENABLE KEYS */;
UNLOCK TABLES;
LOCK TABLES `gacl_groups_axo_map` WRITE;
/*!40000 ALTER TABLE `gacl_groups_axo_map` DISABLE KEYS */;
INSERT INTO `gacl_groups_axo_map` VALUES (1,3),(1,5);
/*!40000 ALTER TABLE `gacl_groups_axo_map` ENABLE KEYS */;
UNLOCK TABLES;

-- SEQUENCES

INSERT INTO `gacl_acl_seq` VALUES (24);
INSERT INTO `gacl_aco_seq` VALUES (32);
INSERT INTO `gacl_aco_sections_seq` VALUES (3);
INSERT INTO `gacl_aro_groups_id_seq` VALUES (3);
INSERT INTO `gacl_aro_sections_seq` VALUES (1);
INSERT INTO `gacl_aro_seq` VALUES (3);
INSERT INTO `gacl_axo_groups_id_seq` VALUES (2);
INSERT INTO `gacl_axo_sections_seq` VALUES (3);
INSERT INTO `gacl_axo_seq` VALUES (5);
