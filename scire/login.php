<?php 
# Configuration
$CONFIG_DIR='/etc/scire';
require_once($CONFIG_DIR.'/config.php');

# Functions
require_once('.lib/functions.php');

# Clean config
$basedir = normalize_path($basedir);
$baseurl = normalize_path($baseurl);
$smarty_dir = normalize_path($smarty_dir);

# Smarty
require_once('.lib/smarty.php');

# Database class
require_once('.lib/DB.php');
$db = new DB($db_host, $db_username, $db_password, $db_name, $db_type);
if (isset($db->error)) {
  header('Content-type: text/html; charset=UTF-8');
  $smarty->assign('title', 'Database Error');
  $smarty->assign('message', $db->error);
  $smarty->display('message.tpl');
  exit(0);
}

# Database functions
require_once('.lib/DB_functions.php');

# Session class
require_once('.lib/Session.php');
$session = new Session($db, $db_sessions_table);
if (isset($session->error)) {
  header('Content-type: text/html; charset=UTF-8');
  $smarty->assign('title', 'Session Error');
  $smarty->assign('message', $session->error);
  $smarty->display('message.tpl');
  exit(0);
}

if ($_GET['logout']) {
	unset($_SESSION['userid']);
	unset($_SESSION['username']);
}
if ($_POST['username']) {
	$user = $db->select(array('userid', 'username','password','email'), "users", "username='".$_POST['username']."'");
	if($user[0]['username']) {
		if ($_POST['passwd'] and (crypt($_POST['passwd'],447470567) == $user[0]['password'])) {
			$_SESSION['userid'] = $user[0]['userid'];
			$_SESSION['username'] = $user[0]['username'];
			$_SESSION['useremail'] = $user[0]['email'];
			$_SESSION['settings'] = get_scire_settings($_SESSION['userid']);
			header("Location: " . ($_GET['afterlogin'] ? $_GET['afterlogin'] : "index.php"));
			exit;
		} else {
			print "<h1>Invalid password supplied!</h1>";
		}
	} else {
		print "<h1>Invalid user supplied!</h1>";
	}
}

$smarty->display('login.tpl');

?>
