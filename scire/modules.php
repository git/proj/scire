<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();
$leftbar_submenu = array();
$template = "modules.tpl";
$status = "";
$smarty->assign('desc', "Modules available to Scire.");

$modules = get_scire_modules();

foreach ($modules as $module) {
	add_leftbar($leftbar_menu, $module['name'], $module['path']);
}


$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('leftbar_submenu', $leftbar_submenu);
$smarty->assign('View', $_GET['View']);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display($template);
#var_dump($_POST);
?>
