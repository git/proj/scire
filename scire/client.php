<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();

add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "clients.php");


$clientid = "";



#####################################################
#Deal with submitted forms.

###############################################
#Adding a client to clientgroups.
if ($_POST['addgroup_confirm']) {
	$ingroups = $acl->get_object_groups($_POST['clientid'],'AXO');
	$clientdata = $acl->get_object_data($_POST['clientid'], 'AXO');
	$newgroups = $_POST['addgroupid'];
	#remove first.
	foreach ($ingroups as $ingroup) {
		if (!in_array($ingroup, $newgroups)) {
			$acl->del_group_object($ingroup, $clientdata[0][0], $clientdata[0][1], 'AXO');
		}
	}
	foreach ($newgroups as $newgroup) {
		print "newgroup: $newgroup";
		$acl->add_group_object($newgroup, $clientdata[0][0], $clientdata[0][1], 'AXO');
	}
	$_GET['Action'] = "edit";
	$_GET['clientid'] = $_POST['clientid'];
}

###############################################
# Adding a client.
if ($_POST['addhostname']) {
	if ($_POST['addhostname'] and $_POST['ip'] and $_POST['mac']) {
		check_action_access("Add Client");#Access check
		#First just incase, we need to check if the object already exists (so we don't make duplicates)
		$obj_id = "";
		if ($acl->get_object_id("clients", $_POST['addhostname'], 'AXO') ) {  #client already exists
			$obj_id = $acl->get_object_id("clients", $_POST['addhostname'], 'AXO');
			$status .= "Found existing object with objectid $obj_id <br>";
		} else {
			$obj_id = $acl->add_object("clients", $_POST['addhostname'], $_POST['addhostname'], 1, 0, 'AXO');
			$status .= "Object id: $obj_id <BR>";
		}
		$result = scire_add_client($obj_id, $_POST['digest'], $_POST['addhostname'], $_POST['mac'], $_POST['ip'], $_POST['gli_profile'], $_POST['osid'], $_POST['contact'], $_POST['status'], $_POST['installtime'], $_POST['assetid']);
		if ($result == 1) { 
			$status .= "Client Addition successfull<br>"; 
		} else {
			$status .= "Error during addition!: $result <br>";
		}

		if ($_POST['groups']) {
			$status .= "Adding client to groups ".$_POST['groups'];
			foreach( $_POST['groups'] as $group) {
				$worked = $acl->add_group_object($group, "clients", $_POST['addhostname'], 'AXO');
				if ($worked) {
					$status .= "Group $group Addition successfull.<br>";
				} else {
					$status .= "ERROR adding group $group <br>";
				}
			}
		}
	} else {
		$status = "Error required fields are not filled!";
	}
	#header('Location: ' . $baseurl . 'clients.php');	
}
###############################################
# Editing a client's information.
if ($_POST['edithostname']) {
	check_action_access("Edit Client");
	$clientinfo = get_scire_client($_GET['clientid']);
	$smarty->assign('client', $clientinfo);
	
	if ($_POST['digest'] and ($_POST['digest'] != $clientinfo['digest'])) {
		$fields['digest'] = $_POST['digest'];
	}
	if ($_POST['mac'] and ($_POST['mac'] != $clientinfo['mac'])){
		$fields['mac'] = $_POST['mac'];
	}
	if ($_POST['ip'] and ($_POST['ip'] != $clientinfo['ip'])) {
		$fields['ip'] = $_POST['ip'];
	}
	if ($_POST['gli_profile'] and ($_POST['gli_profile'] != $clientinfo['gli_profile'])) {
		$fields['gli_profile'] = $_POST['gli_profile'];
	}
	if ($_POST['osid'] and ($_POST['osid'] != $clientinfo['osid'])) {
		$fields['osid'] = $_POST['osid'];
	}
	if ($_POST['contact'] and ($_POST['contact'] != $clientinfo['contact'])) {
		$fields['contact'] = $_POST['contact'];
	}
	if ($_POST['status'] and ($_POST['status'] != $clientinfo['status'])) {
		$fields['status'] = $_POST['status'];
	}
	if ($_POST['installtime'] and ($_POST['installtime'] != $clientinfo['installtime'])) {
		$fields['installtime'] = $_POST['installtime'];
	}
	if ($_POST['assetid'] and ($_POST['assetid'] != $clientinfo['assetid'])) {
		$fields['assetid'] = $_POST['assetid'];
	}
	#special case for hostname, must change the ACLs
	if ($_POST['edithostname'] and ($_POST['edithostname'] != $clientinfo['hostname'])) {
		$fields['hostname'] = $_POST['edithostname'];
		$result = $acl->edit_object($_POST['clientid'],"clients", $_POST['edithostname'], $_POST['edithostname'],1, 0, 'AXO');
	}
	$status = scire_edit_client($_POST['clientid'], $fields);
	$_GET['Action'] = "edit";
	$_GET['clientid'] = $_POST['clientid'];
}

###############################################
# Delete a client.
if ($_POST['delete_confirm']) {
	check_action_access("Delete Client");
	$status = scire_del_client($_POST['clientid']);
	$acl->del_object($_POST['clientid'], 'AXO', TRUE); #True means erase referencing objects.
	header('Location: ' . $baseurl . 'clients.php');
}

switch($_GET['Action']) {
	case "addgroup":
		$smarty->assign('desc', "Add a client to a client group.");
		$smarty->assign('clientid', $_GET['clientid']);
		$smarty->assign('groups', $acl->format_groups($acl->sort_groups('AXO'), TEXT));
		$ingroups = array();
		$ingroups = $acl->get_object_groups($_GET['clientid'],'AXO');
		var_dump($ingroups);
		$smarty->assign('ingroups', $ingroups);
	case "create":
		check_action_access("Add Client");#Access check
		$smarty->assign('desc', "Add a client.");
		$smarty->assign('groups', $acl->format_groups($acl->sort_groups('AXO'), TEXT));
		$smarty->assign('gli_profiles', get_scire_gli_profiles());
		$smarty->assign('oss', get_scire_oss());
		$smarty->assign('users', get_scire_users('username', 'desc'));
		break;
	case "edit":
		check_action_access("Edit Client");#Access check
		$acls = array();
		#make the topbar look good
		$smarty->assign('desc', "Edit client information.");
		$clientinfo = get_scire_client($_GET['clientid']);
		$smarty->assign('client', $clientinfo);
		$smarty->assign('clientid', $_GET['clientid']);
		$smarty->assign('gli_profiles', get_scire_gli_profiles());
		$smarty->assign('oss', get_scire_oss());
		$smarty->assign('users', get_scire_users('username', 'desc'));
		
		$clientgroups = array();
		$groups = $acl->get_object_groups($_GET['clientid'],'AXO');
		if ($groups) {
			foreach ($groups as $group) {
				$group_data = $acl->get_group_data($group, 'AXO');
				$clientgroups_entry['name'] = $group_data[3];
				$clientgroups_entry['id'] = $group_data[0];
				if ($group_data[1]) {
				  $parent_group_data = $acl->get_group_data($group_data[1],'AXO');
				  $clientgroups_entry['parent'] = $parent_group_data[3];
				} else {
				  $clientgroups_entry['parent'] = "";
				}
				array_push($clientgroups, $clientgroups_entry);
				$result_acl_ids = $acl->search_acl(FALSE,FALSE, 
											       FALSE,FALSE,FALSE,
												   FALSE,FALSE,$group_data[3],FALSE);
				foreach ($result_acl_ids as $acl_id) {
					array_push($acls, $acl->get_acl($acl_id));
				}
			}
		}
		$smarty->assign('groups', $clientgroups);
		
		$ugroups = $acl->format_groups($acl->sort_groups('aro'), 'ARRAY');
		$smarty->assign('ugroups', $ugroups);

		#Format of search: aco_section, aco_value, aro_section, aro_value, aro_group_name, axo_section, axo_value, axo_group_name, return_value
		$result_acl_ids = $acl->search_acl(FALSE,FALSE, 
									FALSE,FALSE,FALSE,
									'clients',$clientinfo['hostname'],FALSE,FALSE);
		foreach ($result_acl_ids as $acl_id) {
			array_push($acls, $acl->get_acl($acl_id));
		}
		#pre_var_dump($acls);
		$smarty->assign('acls',$acls);
		break;
	case "delete":
		check_action_access("Delete Client");#Access check
		$clientinfo = get_scire_client($_GET['clientid']);
		$smarty->assign('client', $clientinfo);
		$smarty->assign('clientid', $_GET['clientid']);
		break;
}

$smarty->assign('client_statuses', get_client_statuses() );
$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display('client.tpl');
#pre_var_dump($client_statuses);
?>
