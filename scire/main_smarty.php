<?php
	#This is the code to set up the SMARTY templates for the Main pages.
	# it has ./ on things and sets the leftbar to show the admin list.
	$smarty = new Smarty;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './cache';
	$smarty->config_dir = './configs';
	$smarty->assign('topbar', array(
		  array('name'=> 'Login', 'link' => 'login.php'),
		  array('name'=> 'Clients', 'link' => 'clients.php'),
		  array('name'=> 'Jobs/Tasks', 'link' => 'jobs.php'),
		  array('name'=> 'Modules', 'link' => 'modules.php'),
		  array('name'=> 'Admin', 'link' => 'admin.php')));
	$smarty->assign('left_section_header',"Module List");
	
	$module_list = array();
	$module_list = get_scire_module_list();
	#print_r($module_list);
	$smarty->assign('leftnav', $module_list);
	
	if($status) {	
		$smarty->assign('error', $status);
	}
?>
