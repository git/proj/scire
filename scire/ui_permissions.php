<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();
$leftbar_submenu = array();
add_leftbar($leftbar_menu, "&#171;Settings", "settings.php");


$userid = "";

#####################################################
#Deal with submitted forms.

###############################################
#Adding a permission.
if ($_POST['addperm_submit'] or $_POST['editperm_submit']) {
	if ($_POST['newpermname'] and $_POST['newpermdesc']) {  #Add a new permission FIXME add check of auth for this.
		$obj_id = $acl->add_object($scire_ui_section, $_POST['newpermname'], $_POST['newpermname'], 1, 0, 'ACO');
		$status .= "Obj id: $obj_id";
		$result = scire_add_permission($obj_id, $_POST['newpermname'],$_POST['newpermdesc'], $_SESSION['userid']);
		if ($result == 1) { $status .= "Permission Addition successfull. "; }
		
		$_POST['perm'] = $_POST['newpermname'];
	}
	#Permission should exist now.
	$aco_array[$scire_ui_section] = array($_POST['perm']);
	$aro_array = array();
	$axo_array[$scire_ui_section] = array($scire_ui_section);
	$axo_group_array = array();
	$aro_group_array = array();
#	if ($_POST['usertype'] == "usergroups") {
		$aro_group_array = $_POST['usergroups'];
#	} else {
		$aro_array['users'] = $_POST['users'];
#	}
	
	if (!$_POST['active']) {
		$_POST['active'] = 0;
	}
#	pre_var_dump($aco_array);
#	pre_var_dump($axo_array);
#	pre_var_dump($aro_array);
#	pre_var_dump($aro_group_array);
	if ($_POST['addperm_submit']) {
		$worked = $acl->add_acl($aco_array, $aro_array,$aro_group_array,$axo_array,$axo_group_array,$_POST['allow'],$_POST['active'],1,$_POST['note'],"system");
		if($worked) {
			$status .= "<br><b>Addition Successfull!  $worked</b>";
		} else {
			$status .= "<br><b>Addition BAD $worked</b>";
		}
	} else {#Edit
		$worked = $acl->edit_acl($_POST['editing'],$aco_array, $aro_array,$aro_group_array,$axo_array,$axo_group_array,$_POST['allow'],$_POST['active'],1,$_POST['note'],"system");
		if($worked) {
			$status .= "<br><b>Edit Successfull!  $worked</b>";
		} else {
			$status .= "<br><b>Edit BAD $worked</b>";
		}
	}
	
} 
###############################################
#Adding a permission.
elseif ($_POST['delete_confirm']) {
	$aclid = $_POST['aclid'];
	$worked = $acl->del_acl($aclid);
	if($worked) {
		$status .= "<br><b>Deletion Successfull.  $worked</b>";
	} else {
		$status .= "<br><b>BAD BAD BAD $worked</b>";
	}
	header('Location: ' . $baseurl . 'ui_permissions.php?Action=view');
}
elseif ($_POST['delete_cancel']) {
	header('Location: ' . $baseurl . 'ui_permissions.php?Action=view');
}


########################################################
switch ($_GET['Action']) {
	case "delete":
		$smarty->assign('aclid', $_GET['aclid']);
		break;
	case "edit":
		$smarty->assign('acl',$acl->get_acl($_GET['aclid']));
		#continue on to get the stuff from add.
	case "add":
		add_leftbar($leftbar_menu, "&#171;UI Permissions", "ui_permissions.php");
		$ui_permissions = $acl->get_objects($scire_ui_section, 1, 'ACO');
		$ui_permissions = $ui_permissions[$scire_ui_section];
		#pre_var_dump($ui_permissions);
		$smarty->assign('perms', $ui_permissions);
		$smarty->assign('ugroups', $acl->format_groups($acl->sort_groups('aro'), TEXT));
		break;
	case "view":
	default:
		$smarty->assign('leftbar_submenu_title', "Actions");
		add_leftbar($leftbar_submenu, "Add UI Permission", "ui_permissions.php?Action=add");
		$template = "permissions.tpl";
		$smarty->assign('type','users');
		$ugroups = $acl->format_groups($acl->sort_groups('aro'), 'ARRAY');
		$smarty->assign('ugroups', $ugroups);
		
		$acls = array();
		#Format of search: aco_section, aco_value, aro_section, aro_value, aro_group_name, axo_section, axo_value, axo_group_name, return_value
		$result_acl_ids = $acl->search_acl('UI Permissions',FALSE, 
									FALSE,FALSE,FALSE,
									'scire_ui','scire_ui',FALSE,FALSE);
		foreach ($result_acl_ids as $acl_id) {
			array_push($acls, $acl->get_acl($acl_id));
		}
		#pre_var_dump($acls);
		$smarty->assign('acls',$acls);
		break;
}






$users = get_scire_users('username', 'asc');
$smarty->assign('users', $users);


$smarty->assign('get', $_GET);
$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('leftbar_submenu', $leftbar_submenu);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display('ui_permissions.tpl');
#pre_var_dump($_POST);
?>
