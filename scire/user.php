<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();

add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "users.php");


$userid = "";

#####################################################
#Deal with submitted forms.

###############################################
#Adding a user to usergroups.
if ($_POST['addgroup_confirm']) {
	$ingroups = $acl->get_object_groups($_POST['userid'],'ARO');
	$userdata = $acl->get_object_data($_POST['userid'], 'ARO');
	$newgroups = $_POST['addgroupid'];
	#remove first.
	if ($ingroups) {
		foreach ($ingroups as $ingroup) {
			if (!in_array($ingroup, $newgroups)) {
				$acl->del_group_object($ingroup, $userdata[0][0], $userdata[0][1], 'ARO');
			}
		}
	}
	if ($newgroups) {
		foreach ($newgroups as $newgroup) {
			print "newgroup: $newgroup";
			$acl->add_group_object($newgroup, $userdata[0][0], $userdata[0][1], 'ARO');
		}
	}
	$_GET['Action'] = "edit";
	$_GET['userid'] = $_POST['userid'];
}

###############################################
# Adding a user.
if ($_POST['addusername']) {
	if ($_POST['passwd'] == $_POST['passwd2']) {
		if ($_POST['addusername'] and $_POST['passwd'] and $_POST['email']) {
			$obj_id = "";
			if ($acl->get_object_id("users", $_POST['addusername'], 'ARO') ) {  #client already exists
				$obj_id = $acl->get_object_id("users", $_POST['addusername'], 'ARO');
				$status .= "Found existing object with objectid $obj_id <br>";
			} else {
				$obj_id = $acl->add_object("users", $_POST['addusername'], $_POST['addusername'], 1, 0, 'ARO');
				$status .= "Object id: $obj_id <BR>";
			}
			$result = scire_add_user($obj_id, $_POST['addusername'], $_POST['passwd'], $_POST['real_name'], $_POST['comment'], $_POST['email'], $_POST['phone'], $_POST['pager']);
			if ($result == 1) { 
				$status .= "User Addition successfull"; 
			} else {
				$status .= "Error during addition!: $result <br>";
			}
			if ($_POST['groups']) {
				$status .= "Adding user to groups ".$_POST['groups'];
				foreach( $_POST['groups'] as $group) {
					$worked = $acl->add_group_object($group, "users", $_POST['addusername'], 'ARO');
					if ($worked) {
						$status .= "Group $group Addition successfull.<br>";
					} else {
						$status .= "ERROR adding group $group <br>";
					}
				}
			}
		} else {
			$status = "Error required fields are not filled!";
		}
	} else {
		$status = "Error passwords do not match!";
	}
	#header('Location: ' . $baseurl . 'users.php');	
}
###############################################
# Editing a user's information.
if ($_POST['editusername']) {
	$userinfo = get_scire_user($_GET['userid']);
	$smarty->assign('user', $userinfo);
	
	if ($_POST['real_name'] and ($_POST['real_name'] != $userinfo['real_name'])) {
		$fields['real_name'] = $_POST['real_name'];
	}
	if ($_POST['comment'] and ($_POST['comment'] != $userinfo['comment'])){
		$fields['comment'] = $_POST['comment'];
	}
	if ($_POST['email'] and ($_POST['email'] != $userinfo['email'])) {
		$fields['email'] = $_POST['email'];
	}
	if ($_POST['phone'] and ($_POST['phone'] != $userinfo['phone'])) {
		$fields['phone'] = $_POST['phone'];
	}
	if ($_POST['pager'] and ($_POST['pager'] != $userinfo['pager'])) {
		$fields['pager'] = $_POST['pager'];
	}
		#special case for hostname, must change the ACLs
	if ($_POST['editusername'] and ($_POST['editusername'] != $userinfo['username'])) {
		$fields['username'] = $_POST['editusername'];
		$result = $acl->edit_object($_POST['userid'],"users", $_POST['editusername'], $_POST['editusername'],1, 0, 'ARO');
	}
	
	if ($_POST['passwd'] and ($_POST['passwd'] == $_POST['passwd2']) ) {
		check_action_access("Change User Pass/Name");
		$fields['password'] = crypt($_POST['passwd'], 447470567);
	}
	$status = scire_edit_user($_POST['userid'], $fields);
	$_GET['Action'] = "edit";
	$_GET['userid'] = $_POST['userid'];
}

###############################################
# Delete a user.
if ($_POST['delete_confirm']) {
	$status = scire_del_user($_POST['userid']);
	$acl->del_object($_POST['userid'], 'ARO', TRUE); #True means erase referencing objects.
	header('Location: ' . $baseurl . 'users.php');
}

switch($_GET['Action']) {
	case "addgroup":
		$smarty->assign('userid', $_GET['userid']);
		$smarty->assign('groups', $acl->format_groups($acl->sort_groups('aro'), TEXT));
		$ingroups = array();
		$ingroups = $acl->get_object_groups($_GET['userid'],'ARO');
		#var_dump($ingroups);
		$smarty->assign('ingroups', $ingroups);
	case "create":
		$smarty->assign('desc', "Add a user.");
		$smarty->assign('groups', $acl->format_groups($acl->sort_groups('aro'), TEXT));
		break;
	case "edit":
		$acls = array();
		#make the topbar look good
		$userid = $_GET['userid'];
		$smarty->assign('desc', "Edit user information.");
		$userinfo = get_scire_user($userid);
		$smarty->assign('user', $userinfo);
		
		$usergroups = array();
		#$id = $acl->get_object_id("users",$userinfo['username'],'ARO');
		$smarty->assign('userid', $userid);
		$groups = $acl->get_object_groups($userid,'ARO');
		if ($groups) {
			foreach ($groups as $group) {
				$group_data = $acl->get_group_data($group, 'ARO');
				$usergroups_entry['name'] = $group_data[3];
				$usergroups_entry['id'] = $group_data[0];
				if ($group_data[1]) {
				  $parent_group_data = $acl->get_group_data($group_data[1],'ARO');
				  $usergroups_entry['parent'] = $parent_group_data[3];
				} else {
				  $usergroups_entry['parent'] = "";
				}
				array_push($usergroups, $usergroups_entry);
				$result_acl_ids = $acl->search_acl(FALSE,FALSE, 
											       FALSE,FALSE,FALSE,
												   FALSE,FALSE,$group_data[3],FALSE);
				foreach ($result_acl_ids as $acl_id) {
					array_push($acls, $acl->get_acl($acl_id));
				}
			}
		}
		$smarty->assign('groups', $usergroups);
				$cgroups = $acl->format_groups($acl->sort_groups('axo'), 'ARRAY');
		$smarty->assign('cgroups', $cgroups);
		#Format of search: aco_section, aco_value, aro_section, aro_value, aro_group_name, axo_section, axo_value, axo_group_name, return_value
		$result_acl_ids = $acl->search_acl(FALSE,FALSE, 
									'users',$userinfo['username'],FALSE,
									FALSE,FALSE,FALSE,FALSE);
		foreach ($result_acl_ids as $acl_id) {
			array_push($acls, $acl->get_acl($acl_id));
		}
		#pre_var_dump($acls);
		$smarty->assign('acls',$acls);
		break;
	case "delete":
		$userinfo = get_scire_user($_GET['userid']);
		$smarty->assign('user', $userinfo);
		#$id = $acl->get_object_id("users",$userinfo['username'],'ARO');
		$smarty->assign('userid', $_GET['userid']);
		break;
}


$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display('user.tpl');
#var_dump($_POST);
?>
