<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();
$leftbar_submenu = array();
$template = "clients.tpl";
$status = "";
$smarty->assign('statusfilters', array('', 'Pending','Active','Inactive','Rejected','Retired', 'All'));
check_action_access("View Clients");
############################################
## Deal with submitted forms first.
if ($_POST['addgroupname']) {  #Adding a group.
	check_action_access("Add ClientGroup");
	if (!$_POST['parentgroup']) {
		$_POST['parentgroup'] = 0;
	}
	$newgroup_id = $acl->add_group($_POST['addgroupname'], $_POST['addgroupname'], $_POST['parentgroup'], 'AXO');
	$_GET['Action'] = "added";
	$_GET['View'] = "groups";
	$status = "Client Group Added Successfully.";
}
if ($_POST['editgroupname']) {
	check_action_access("Edit ClientGroup");
	$group_id = $acl->edit_group($_POST['groupid'], $_POST['editgroupname'], $_POST['editgroupname'], $_POST['parentgroup'], 'AXO');
	$_GET['Action'] = "edited";
	$_GET['View'] = "groups";
	$status = "Client Group Edited Successfully.";
}
if($_POST['delgroup_confirm']) {
	check_action_access("Delete ClientGroup");
	#Check if the group has children.
	$result = $acl->del_group($_POST['delgroup'],TRUE, 'AXO');
	if ($result) {
		$status = "Group deleted successfully.";
	} else {
		$status = "ERROR: Group could not be successfully deleted.";
	}
	$_GET['Action'] = "deleted";
	$_GET['View'] = "groups";
}
#Admin access only.
if($_POST['pend_approve']) {
	check_action_access("Approve/Reject Pending Clients");
	foreach ($_POST['pending_ids'] as $pend_id) {
		$result = scire_approve_client($pend_id);
		if ($result) {
			$status .= "Client $pend_id approved successfully.";
			$clientinfo = get_scire_client($pend_id);
			$obj_id = $acl->add_object("clients", $clientinfo['hostname'], $clientinfo['hostname'], 1, 0, 'AXO');
			print "Object id: $obj_id <BR>";
		} else {
			$status .= "ERROR: Client $pend_id could not be successfully approved.";
		}
	}
}
if($_POST['pend_reject']) {
	check_action_access("Approve/Reject Pending Clients");
	foreach ($_POST['pending_ids'] as $pend_id) {
		$result = scire_reject_client($pend_id);
		if ($result) {
			$status .= "Client $pend_id rejected!";
		} else {
			$status .= "ERROR: Client $pend_id could not be successfully rejected.";
		}
	}
}


if($_POST['statusfilter']) {
	if ( check_access("View Pending Clients") ) {
		$_GET['View'] = $_POST['statusfilter'];
	}
}

$smarty->assign('desc', "View, create, edit, and delete clients");

########################################
# Generate content

switch($_GET['View']) {
	case "groups":
		$template = "clients_groups.tpl";
		switch($_GET['Action']) {
			case "create":
				add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "clients.php?View=groups");
				$smarty->assign('groups', $acl->format_groups($acl->sort_groups('AXO'), TEXT));
				break;
			case "edit":
				add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "clients.php?View=groups");
				$smarty->assign('groups', $acl->format_groups($acl->sort_groups('AXO'), TEXT));
				$smarty->assign('groupid', $_GET['groupid']);
				$smarty->assign('groupdata', $acl->get_group_data($_GET['groupid'], 'AXO'));
				break;
			case "delete":
				add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "clients.php?View=groups");
				$smarty->assign('groupid', $_GET['groupid']);
				break;
			default:  #Show clientgroups.
				add_leftbar($leftbar_menu, "Clients", "clients.php");
				add_leftbar($leftbar_menu, "Permissions", "clients.php?View=permissions");
				$smarty->assign('leftbar_submenu_title', "Actions");
				if ( check_access("Add ClientGroup") ) {
					add_leftbar($leftbar_submenu, "Add Client Group", "clients.php?View=groups&Action=create");
				}
				
		
				$groups = $acl->format_groups($acl->sort_groups('AXO'), HTML);
				$smarty->assign('groups', $groups);
				
			break;
		}
		break;
	case "permissions":
		$smarty->assign('leftbar_submenu_title', "Actions");
		check_action_access("View Permissions");
		if ( check_access("Assign Permission") ) {
			add_leftbar($leftbar_submenu, "Assign Permission", "assign_permission.php");
		}
		$template = "permissions.tpl";
		$smarty->assign('type','clients');
		$ugroups = $acl->format_groups($acl->sort_groups('aro'), 'ARRAY');
		$smarty->assign('ugroups', $ugroups);
		$cgroups = $acl->format_groups($acl->sort_groups('axo'), 'ARRAY');
		$smarty->assign('cgroups', $cgroups);

		#pre_var_dump($groups);
		
		$acls = array();
		#Format of search: aco_section, aco_value, aro_section, aro_value, aro_group_name, axo_section, axo_value, axo_group_name, return_value
		$result_acl_ids = $acl->search_acl(FALSE,FALSE, 
									FALSE,FALSE,FALSE,
									FALSE,FALSE,FALSE,FALSE);
		foreach ($result_acl_ids as $acl_id) {
			$this_acl = $acl->get_acl($acl_id);
			if ($this_acl['aco']['scire_ui']) {
			  next;
			} else {
			array_push($acls, $this_acl);
			}
		}
		#pre_var_dump($acls);
		$smarty->assign('acls',$acls);
		break;
	case "Pending":
	case "Inactive":
	case "Rejected":
	case "Retired":
	case "All":
		#Will need to grab only pending clients.  Will need to hardcode the field list unless making settings for it.
		add_leftbar($leftbar_menu, "View Groups", "clients.php?View=groups");
		add_leftbar($leftbar_menu, "View Permissions", "clients.php?View=permissions");
		
		#Set information about the fields (display name, sortable)
		$f_attr = get_clients_fields();
		$smarty->assign('f_attr', $f_attr);
		
		if (!$_GET['sorton']) {
			$_GET['sorton'] = 'installtime';
		}
		if (!$_GET['sortdir']) {
			$_GET['sortdir'] = 'asc';
		}
		$smarty->assign('sorton', $_GET['sorton']);
		$smarty->assign('sortdir', $_GET['sortdir']);
		
		#Grab the field list.
		$fields = explode(',', $_SESSION['settings']['clients_page_fields']);
		$fields = array("hostname", "ip", "mac", "digest", "status", "installtime");
		$smarty->assign('fields',$fields);
		
		$clients = get_scire_clients($_GET['sorton'], $_GET['sortdir'], $_GET['View']);
		$smarty->assign('clients', $clients);
		break;
	default:  #Show clients.
		add_leftbar($leftbar_menu, "View Groups", "clients.php?View=groups");
		add_leftbar($leftbar_menu, "View Permissions", "clients.php?View=permissions");
		$smarty->assign('leftbar_submenu_title', "Actions");
		if ( check_access("Add Client") ) {
			add_leftbar($leftbar_submenu, "Create Client", "client.php?Action=create");
		}
		############################################
		## Deal with sorting for the query.
		if (!$_GET['sorton']) {
			$_GET['sorton'] = $_SESSION['settings']['clients_page_sorton'];
		}
		if (!$_GET['sortdir']) {
			$_GET['sortdir'] = $_SESSION['settings']['clients_page_sortdir'];
		}
		$smarty->assign('sorton', $_GET['sorton']);
		$smarty->assign('sortdir', $_GET['sortdir']);

		$clients = get_scire_clients($_GET['sorton'], $_GET['sortdir']);
		#var_dump($clients);
		foreach ($clients as $key => $client) {
			$id = $acl->get_object_id("clients",$client['hostname'],'AXO');
			$groups = $acl->get_object_groups($id,'AXO');
			$group_string = "";
			if ($groups) {
				foreach ($groups as $group) {
					$group_data = $acl->get_group_data($group, 'AXO');
					$group_string .= $group_data[3]. " | ";
				}
			}
			$clients[$key]['groups'] = $group_string;
		}
		
		#Set information about the fields (display name, sortable)
		$f_attr = get_clients_fields();
		$smarty->assign('f_attr', $f_attr);
		
		#Grab the field list.
		if ($_SESSION['settings']['clients_page_fields']) {
			$fields = explode(',', $_SESSION['settings']['clients_page_fields']);
		} else {
			$fields = explode(',', get_default_clients_fields());
		}
		$smarty->assign('fields',$fields);
			
		$smarty->assign('clients', $clients);
		break;
}

$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('leftbar_submenu', $leftbar_submenu);
$smarty->assign('View', $_GET['View']);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display($template);
#var_dump($_POST);
?>
