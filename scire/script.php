<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();
$leftbar_submenu = array();
$smarty->assign('desc', "Manage scripts.");
#add_leftbar($leftbar_menu, "Main Settings", "settings.php");
#add_leftbar($leftbar_menu, "Clients Page", "settings.php?View=Clients");
$smarty->assign('leftbar_submenu_title', "Actions");
add_leftbar($leftbar_submenu, "Add Script", "script.php?Action=create");

if ($_POST['add_script']) {
	#What are the required fields?
	#name, description, location, script_data,log_location, success_code, run_as, priority, permission, pp_location, pp_script_data.
	if ($_POST['addname'] and $_POST['desc']) {
		$script['name'] = $_POST['addname'];
		$script['desc'] = $_POST['desc'];
	} else {
		$status .= "Error: name or description missing!";
	}
	if ($_POST['script_data_type']) {
		$script['location'] = "Embedded";
		if ($_POST['script_data_type'] == "load") {
			$script['script_data'] = $_POST['script_data_upload']; #FIXME how is this done?
		} elseif ($_POST['script_data_type'] == "manual") {
			$script['script_data'] = $_POST['script_data_manual'];
			$script['script_data'] = preg_replace('/\r\n/',"\n",$script['script_data']);
		}
	} else {
		$status .= "Error: You must select a script type (Load from file or manual input!";
	}
	if ($_POST['runas_radio']) {
		$script['run_as'] = $_POST['runas_radio'];
	} elseif ($_POST['runas']) {
		$script['run_as'] = $_POST['runas'];
	} else {
		$status .= "No run_as selected, defaulting to root!!! ";
		$script['run_as'] = "root";
	}
	if ($_POST['priority']) {
		$script['priority'] = $_POST['priority'];
	} else {
		$status .= "No priority selected, defaulting to 5. ";
		$script['priority'] = "5";
	}
	if ($_POST['success_code']) {
		$script['success_code'] = $_POST['success_code'];
	} else {
		$status .= "No success code selected, defaulting to 0. ";
		$script['success_code'] = "0";
	}
	if ($_POST['permission']) {
		$script['permission'] = $_POST['permission'];
	}
	if ($_POST['pps_type']) {
		$script['pp_location'] = "Embedded";
		if ($_POST['pps_type'] == "load") {
			$script['pp_script_data'] = $_POST['pps_load']; #FIXME how is this done?
		} elseif ($_POST['pps_type'] == "manual") {
			$script['pp_script_data'] = $_POST['pps_data'];
		}
	}
	
	#Look for tags.
	
	foreach ($_POST as $name => $value) {
		if (substr($name,0, 4) == "tag_" and $value) {
			$temp = substr($name,4);
			$script_tags[$temp] = $value;
		}
	}
	#Ok, i think we got everything, now lets add it
	$result = scire_add_script($script['name'], $script['desc'], $script['location'], $script['script_data'], $script['log_location'], $script['success_code'], $script['run_as'], $script['priority'], $script['permission'], $script['pp_location'], $script['pp_script_data'], $script_tags);
	if (!$result) { #success
		$status .= "Scipt addition successfull!";
	} else {
		$status .= "BAD BAD! Error during query. $result";
	}
}  #/if add_script
elseif ($_POST['continue']) {
	#They pressed the continue button, so just repost the fields, and then add tags as we find them.
	if ($_POST['addname']) { $smarty->assign('addname', $_POST['addname']); }
	if ($_POST['desc']) { $smarty->assign('desc', $_POST['desc']); }
	if ($_POST['runas_radio']) { $smarty->assign('runas', $_POST['runas_radio']); }
	if ($_POST['runas']) { $smarty->assign('runas', $_POST['runas']); }
	if ($_POST['priority']) { $smarty->assign('priority', $_POST['priority']); }
	if ($_POST['permission']) { $smarty->assign('permission', $_POST['permission']); }
	if ($_POST['script_data_manual']) { $script_data = $_POST['script_data_manual']; }
	#if ($_POST['script_data_upload']) { $script_data = #FIXME some way get this file and parse it. potential security issue? }
	
	preg_match_all('/%%(.*?)%%/', $script_data, $matches);
#	pre_var_dump($dyn_tag);
#	pre_var_dump($matches);
	$smarty->assign('dyn_tags', $matches[1]);
	
}

$permissions = array();
$perm_categories = array();
$smarty->assign('priorities', get_priorities() );
$all_permissions = $acl->get_objects(NULL, 1, 'ACO');
foreach ($all_permissions as $categoryname => $cat) {
	#print "Perm: $perm<br>Permname: $permname<br>";
	if ($categoryname != $scire_ui_section) {
		foreach ($cat as $perm) {
			$tmp_perm[id] = $acl->get_object_id($categoryname,$perm,'ACO');
			$tmp_perm[name] = $perm;
			array_push($permissions, $tmp_perm);
		}
		array_push($perm_categories, $categoryname);
	}
}
if (!$script_data) {
	$script_data = '#!/bin/bash' . "\n" . 'emerge sync';
}
$smarty->assign('script_data', $script_data); 

$smarty->assign('perm_categories',$perm_categories);
$smarty->assign('perms', $permissions);

$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('leftbar_submenu', $leftbar_submenu);
$smarty->assign('View', $_GET['View']);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display('script.tpl');
#var_dump($all_permissions);
?>
