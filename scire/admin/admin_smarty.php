<?php
	#This is the code to set up the SMARTY templates for the Admin pages.
	# it has ../ on things and sets the leftbar to show the admin list.
	$smarty = new Smarty;
	$smarty->template_dir = '../templates';
	$smarty->compile_dir = '../templates_c';
	$smarty->cache_dir = '../cache';
	$smarty->config_dir = '../configs';
	$smarty->assign('topbar', array(
		  array('name'=> 'Login', 'link' => '../login.php'),
		  array('name'=> 'Clients', 'link' => '../clients.php'),
		  array('name'=> 'Jobs/Tasks', 'link' => '../jobs.php'),
		  array('name'=> 'Modules', 'link' => '../modules.php'),
		  array('name'=> 'Admin', 'link' => '../admin.php')));
	$smarty->assign('left_section_header',"Admin Functions");
	$smarty->assign('leftnav', array(
	array("create_user.php", "Create a User"),
	array("edit_user.php","Edit a User"),
	array("delete_user.php","Delete a User"),
	array("",""),
	array("create_usergroup.php","Create a UserGroup"),
	array("edit_usergroup.php","Edit a UserGroup"),
	array("delete_usergroup.php","Delete a UserGroup"),
	array("",""),
	array("add_permission.php","Add Permission"),
	array("edit_permission.php","Edit Permission"),
	array("delete_permission.php","Delete Permission"),
	array("",""),
	array("add_client.php","Add Client Manually"),
	array("edit_client.php","Edit Clients"),
	array("delete_client.php","Delete Client from Scire"),
	array("",""),
	array("create_clientgroup.php","Create a ClientGroup"),
	array("edit_clientgroup.php","Edit a ClientGroup"),
	array("delete_clientgroup.php","Delete a ClientGroup"),
	array("",""),
	array("assign_permissions.php","Assign Permissions"),
	array("",""),
	));
	
	
	if($status) {	
		$smarty->assign('error', $status);
	}
?>
