<?php

	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');

	#Load Smarty Template
	include('admin_smarty.php');

	$status = "";
	if($_POST['delsubmit'] and $_POST['delclient'] and $_POST['iamsure']) {
		include('phpgacl_code.php');
		$client_id = $_POST['delclient'];
		$client_data = get_scire_client($client_id);
		
		$status = scire_del_client($client_id);
		
		$id = $gacl_api->get_object_id("clients", $client_data['hostname'], 'AXO');
		$gacl_api->del_object($id, 'AXO', TRUE);
	}

	$clients = get_all_clients();
	$smarty->assign('clients', $clients);

	$smarty->display('admin/delete_client.tpl');
	$smarty->display('leftbar.tpl');
?>
