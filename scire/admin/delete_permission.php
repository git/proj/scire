<?php
	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');
	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the GACL_API

	####################################################
	#Load Smarty Template
	include('admin_smarty.php');


	if($_POST['delperm'] and $_POST['delsubmit'] and $_POST['iamsure']) {
		#Delete the user.
		$status .= scire_del_permission($_POST['delperm']);	
		if(!$status) {$status = "User deleted from DB successfully.  ";}
		#Get the obj ID for edit
		$obj_id = $gacl_api->get_object_id("perms",$_POST['delperm'],'ACO'); 
		$result = $gacl_api->del_object($obj_id, 'ACO', TRUE);
		if($result) {
			$status .= "Permission deleted from ACLs successfully.	";
		} else {
			$status .= "ERROR: Permission could not be delted from the ACLs!  ";
		}
	}

	###################################################
	$permissions = get_scire_permissions();
	$smarty->assign('permissions', $permissions);

	$smarty->display('admin/delete_permission.tpl');


	$smarty->display('leftbar.tpl');
?>
