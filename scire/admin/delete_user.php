<?php

	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');  
	
	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the gacl_api
	
	#Load Smarty Template
	include('admin_smarty.php');
		
	if($_POST['delsubmit'] and $_POST['deluser'] and $_POST['iamsure']) {
		
		$status = scire_del_user($_POST['deluser']);
		$id = $gacl_api->get_object_id("users", $_POST['deluser'], 'ARO');
		$gacl_api->del_object($id, 'ARO', TRUE);
	}

	
	$users = get_scire_users();
	$smarty->assign('users', $users);

	$smarty->display('admin/delete_user.tpl');
	$smarty->display('leftbar.tpl');
?>
