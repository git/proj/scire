<?php
	#################################################### PHPGACL CODE
	require_once(dirname(__FILE__).'/../phpgacl/gacl.class.php');
	require_once(dirname(__FILE__).'/../phpgacl/gacl_api.class.php');
	require_once(dirname(__FILE__).'/../phpgacl/admin/gacl_admin_api.class.php');

// phpGACL Configuration file.
if ( !isset($config_file) ) {
#	$config_file = '../gacl.ini.php';
	$config_file = dirname(__FILE__).'/../phpgacl/gacl.ini.php';
}

//Values supplied in $gacl_options array overwrite those in the config file.
if ( file_exists($config_file) ) {
	$config = parse_ini_file($config_file);

	if ( is_array($config) ) {
		if ( isset($gacl_options) ) {
			$gacl_options = array_merge($config, $gacl_options);
		} else {
			$gacl_options = $config;
		}
	}
	unset($config);
}
$gacl_api = new gacl_admin_api($gacl_options);
###############################################
?>
