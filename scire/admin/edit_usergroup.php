<?php
	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');

	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the GACL_API

	#Load Smarty Template
	include('admin_smarty.php');

	### Deal with submitted form here
	if ($_POST['editgroup']) {
		$editgroup = $_POST['editgroup'];
		$editgroupdata = $gacl_api->get_group_data($editgroup, 'ARO');
	}
	
	if ($_POST['groupname'] and $_POST['parentgroup']) {
		$newgroup_id = $gacl_api->edit_group($_POST['groupid'], $_POST['groupname'], $_POST['groupname'], $_POST['parentgroup'], 'ARO');
		print "Group edited successfully, group id is: $newgroup_id<br>";
	}

	$formatted_groups = $gacl_api->format_groups($gacl_api->sort_groups('aro'), HTML);
	$formatted_groups2 = $gacl_api->format_groups($gacl_api->sort_groups('aro'), TEXT);
	
	$ids = array();
	$groupnames = array();
	foreach($formatted_groups2 as $id => $name) {
		#print "ID: $id   Name: $name  <br>";
		array_push($ids, $id);
		array_push($groupnames, $name);
	}

	#For the <select> list of groups.
	$smarty->assign('ids',$ids);
	$smarty->assign('groupnames', $groupnames);
	
	$smarty->assign('editgroup', $editgroup);
	$smarty->assign('editgroupdata', $editgroupdata);
	
	$smarty->assign('usergroups', $formatted_groups);
	
	$smarty->display('admin/edit_usergroup.tpl');
	$smarty->display('leftbar.tpl');
?>
