<?php
	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');
	
	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the GACL_API

	####################################################
	#Load Smarty Template
	include('admin_smarty.php');


	if($_POST['username']) {
		if ($_POST['passwd'] == $_POST['passwd2']) {
			#Add the user now.
			$status .= scire_add_user($_POST['username'], $_POST['passwd'], $_POST['realname'], $_POST['comment']);	
			#			$status = "User added to DB successfully.";
				$obj_id = $gacl_api->add_object("users", $_POST['username'], $_POST['username'], 1, 0, 'ARO');
				print "Object id: $obj_id <BR>";
			
			if ($_POST['groups']) {
				print "Adding user to groups ".$_POST['groups'];
				foreach( $_POST['groups'] as $group) {
					$worked = $gacl_api->add_group_object($group, "users", $_POST['username'], 'ARO');
					if ($worked) {
						print "Group $group Addition successfull.<br>";
					} else {
						print "ERROR adding group $group <br>";
					}
				}
			}
		}
		else
		{
			$status = "ERROR: Passwords do not match.";
		}
		
	}
	$formatted_groups = $gacl_api->format_groups($gacl_api->sort_groups('aro'), TEXT);
	
	$ids = array();
	$groupnames = array();
	foreach($formatted_groups as $id => $name) {
		print "ID: $id   Name: $name  <br>";
		array_push($ids, $id);
		array_push($groupnames, $name);
	}
	
	###################################################
	$smarty->assign('ids',$ids);
	$smarty->assign('groupnames', $groupnames);
	
	$smarty->display('admin/create_user.tpl');
	$smarty->display('leftbar.tpl');
?>
