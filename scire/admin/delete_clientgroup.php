<?php
	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');
	
	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the GACL_API

	#Load Smarty templates.
	include('admin_smarty.php');

	### Deal with submitted form here
	if($_POST['delsubmit'] and $_POST['delgroup'] and $_POST['iamsure']) {
		#Check if the group has children.
		$result = $gacl_api->del_group($_POST['delgroup'],TRUE, 'AXO');
		if ($result) {
			$status = "Group deleted successfully.";
		} else {
			$status = "ERROR: Group could not be successfully deleted.";
		}
		
	}

	$formatted_groups = $gacl_api->format_groups($gacl_api->sort_groups('axo'), HTML);
	$formatted_groups2 = $gacl_api->format_groups($gacl_api->sort_groups('axo'), TEXT);
	
	$ids = array();
	$groupnames = array();
	foreach($formatted_groups2 as $id => $name) {
		#print "ID: $id   Name: $name  <br>";
		array_push($ids, $id);
		array_push($groupnames, $name);
	}
	
	#For the <select> list of groups.
	$smarty->assign('ids',$ids);
	$smarty->assign('groupnames', $groupnames);
	
	$smarty->assign('editgroup', $editgroup);
	$smarty->assign('editgroupdata', $editgroupdata);
	$smarty->assign('clientgroups', $formatted_groups);
	
	$smarty->display('admin/delete_clientgroup.tpl');
	$smarty->display('leftbar.tpl');
?>
