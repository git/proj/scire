<?php
	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');
	
	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the GACL_API

	####################################################
	#Load Smarty Template
	include('admin_smarty.php');
	
	if($_POST['permname'] and $_POST['permdesc']) {
		#Add the user now.
		$status .= gimli_edit_permission($_POST['permname'], $_POST['permdesc']);	
		if(!$status) {$status = "User edited to DB successfully.";}
		#Get the obj ID for edit
		$obj_id = $gacl_api->get_object_id("perms",$_POST['permname'],'ACO'); 
		$result = $gacl_api->edit_object($obj_id, "perms", $_POST['permname'], $_POST['permname'], 1, 0, 'ACO');
		print "Object id: $obj_id <BR>";	
	}

	###################################################
	if($_POST['submit'] and $_POST['editperm'])
	{
		$permname = $_POST['editperm'];
		$smarty->assign('permname', $_POST['editperm']);
		$smarty->assign('permdesc', $_POST[$permname.'_desc']);	
	}
	else{ 
		$permissions = get_gimli_permissions();
		$smarty->assign('permissions', $permissions);
	}

	$smarty->display('admin/edit_permission.tpl');
	$smarty->display('leftbar.tpl');
?>
