<?php
	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');

	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the GACL_API
	
	####################################################
	#Load Smarty Template
	include('admin_smarty.php');

	
	if($_POST['submit']) {
		if (!$_POST['hostname']) { $status .= "ERROR: Hostname required!  "; }
		if (!$_POST['mac']) { $status .= "ERROR: Hostname required!  "; }
		if (!$_POST['ip']) { $status .= "ERROR: Hostname required!  "; }
		if ($_POST['hostname'] and $_POST['mac'] and $_POST['ip'])
		{
			#Add the client now.
			$status .= scire_add_client($_POST['hostname'], $_POST['mac'], $_POST['ip'], $_POST['profile'], $_POST['distribution'], $_POST['administrator'], $_POST['status']);	
			if (!$status) {	$status = "User added to DB successfully.";}
				$obj_id = $gacl_api->add_object("clients", $_POST['hostname'], $_POST['hostname'], 1, 0, 'AXO');
				print "Object id: $obj_id <BR>";
			
			if ($_POST['groups']) {
				print "Adding client to groups ".$_POST['groups'];
				foreach( $_POST['groups'] as $group) {
					$worked = $gacl_api->add_group_object($group, "clients", $_POST['hostname'], 'AXO');
					if ($worked) {
						print "Group $group Addition successfull.<br>";
					} else {
						print "ERROR adding group $group <br>";
					}
				}
			}
		}
	}
	
	
	$formatted_groups = $gacl_api->format_groups($gacl_api->sort_groups('axo'), TEXT);
	
	$ids = array();
	$groupnames = array();
	foreach($formatted_groups as $id => $name) {
		print "ID: $id   Name: $name  <br>";
		array_push($ids, $id);
		array_push($groupnames, $name);
	}
	
	###################################################
	$smarty->assign('ids',$ids);
	$smarty->assign('groupnames', $groupnames);
	
	$smarty->display('admin/add_client.tpl');


	$smarty->display('leftbar.tpl');
?>
