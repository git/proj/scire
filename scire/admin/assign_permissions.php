<?php
	include('Smarty.class.php');
	include('../DBInterface.php');
	include('admin_checklogin.php');
	
	# PHPGACL CODE
	$status = "";
	include('phpgacl_code.php');  #load the GACL_API
	
	####################################################
	#Load Smarty Template
	include('admin_smarty.php');
	
	###################################################
	#Deal with submitted form
	if ($_POST['id']) {
		$type = $_POST['id'][0];
		$idnum = substr($_POST['id'],2);
		print "Type: $type    ID: $idnum <br>";
		$smarty->assign('type', $type);
		$smarty->assign('idnum', $idnum);
	
		if( $type == "u") {
			print "found user";
			$selected_user_data = $gacl_api->get_object_data($idnum, 'ARO');
			$smarty->assign('selected_name', $selected_user_data[0][3]);
		}
		elseif ($type == "g") {
			$selected_group_data = $gacl_api->get_group_data($idnum, 'ARO');
			$smarty->assign('selected_name', $selected_group_data[3]);
		}
		$clients = $gacl_api->format_groups($gacl_api->sort_groups('axo'), TEXT);
		$smarty->assign('clientgroups',$clients);
		
		$client_ids = array();
		$client_groupnames = array();
		foreach($clients as $id => $name) {
			print "ID: $id   Name: $name  <br>";
			array_push($client_ids, $id);
			array_push($client_groupnames, $name);
		}
		
		
		
		#This is an ugly hack to get the users and their groups.
		$all_clients = $gacl_api->get_objects("clients",1,'AXO');
		$clientinfo = array();
		foreach ($all_clients as $client) {
			foreach ($client as $clientdata) {
				$id = $gacl_api->get_object_id("clients",$clientdata,'AXO');
				$groups = $gacl_api->get_object_groups($id,'AXO');
				array_push($clientinfo, array($id,$clientdata,$groups));
			}
		}
		$smarty->assign('clients', $clientinfo);
	
		
		
		
		
		#For the <select> list of groups.
		$smarty->assign('client_ids',$client_ids);
		$smarty->assign('client_groupnames', $client_groupnames);
		
		$permissions = $gacl_api->get_objects("perms",1, 'ACO');
		$permissionsinfo = array();
		foreach ($permissions as $eachpermission) {
			foreach ($eachpermission as $singlepermission) {
			$id = $gacl_api->get_object_id("perms",$singlepermission, 'ACO');
			array_push($permissionsinfo, array($id, $singlepermission));
			}
		}
		$smarty->assign('permissions',$permissionsinfo);
	}
	###################################################
	#Deal with part 2 submitted form
	if ($_POST['permissions'] and $_POST['clients']) {
		print "Perms: ".$_POST['permissions']." <br>User: ".$_POST['selected_name'];
		$aco_array = array();
		$aro_array = array();
		$axo_array = array();
		$axo_group_array = array();
		$aro_group_array = array();
		
		$aco_array['perms'] = $_POST['permissions'];
		if($_POST['type'] == "u") {
			$aro_array['users'] = array($_POST['selected_name']);
		} elseif ($_POST['type'] == "g") {
			$aro_group_array = array($_POST['idnum']);
		} else {
			print "ERROR NO TYPE";
			exit;
		}
		
		$axo_group_array = $_POST['clients'];
		$worked = $gacl_api->add_acl($aco_array, $aro_array,$aro_group_array,$axo_array,$axo_group_array,1,1,1,"Test ACL","system");
		if($worked) {
			print "Addition Successfull!  $worked";
		} else {
			print "BAD BAD BAD";
		}
	#	foreach( $_POST['permissions'] as $perm_to_add) {
				
	#	}
	#	foreach $_POST['clients'] as $client_to_add) {
				
	#	}
	}
	
	###################################################
	#Get the usergroups.
	$formatted_groups = $gacl_api->format_groups($gacl_api->sort_groups('aro'), HTML);
	$smarty->assign('usergroups',$formatted_groups);
	
	#This is an ugly hack to get the users and their groups.
	$all_users = $gacl_api->get_objects("users",1,'ARO');
	$userinfo = array();
	foreach ($all_users as $user) {
		foreach ($user as $userdata) {
			$id = $gacl_api->get_object_id("users",$userdata,'ARO');
			$groups = $gacl_api->get_object_groups($id,'ARO');
			array_push($userinfo, array($id,$userdata,$groups));
		}
	}
	$smarty->assign('users', $userinfo);
	
	$smarty->display('admin/assign_permissions.tpl');
	$smarty->display('leftbar.tpl');
?>
