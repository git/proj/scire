<?php

function normalize_path($path) {
  if ($path && strlen($path) > 0) {
    return preg_replace('/^(.*?)[\/]?$/', '$1/', $path);
  } else {
    return $path;
  }
}

function dumper($var) {
  ob_start();
  var_dump($var);
  return ob_get_clean();
}

function pre_var_dump($var) {
  print "<pre>";
  var_dump($var);
  print "</pre>";
}

function add_leftbar(&$leftbar_menu, $entry_name, $entry_link) {
	$temp_menu_entry['name'] = $entry_name;
	$temp_menu_entry['link'] = $entry_link;
	array_push($leftbar_menu, $temp_menu_entry);
}

function get_clients_fields() {
	$fields['hostname'] = array('name' => 'Hostname', 'sortable' => True);
	$fields['ip'] = array('name' => 'IP Address', 'sortable' => True);
	$fields['mac'] = array('name' => 'MAC Address', 'sortable' => True);
	$fields['digest'] = array('name' => 'Cert Digest', 'sortable' => False);
	$fields['profile_name'] = array('name' => 'GLI Profile', 'sortable' => True);
	$fields['osname'] = array('name' => 'OS Name', 'sortable' => True);
	$fields['username'] = array('name' => 'User Contact', 'sortable' => True);
	$fields['installtime'] = array('name' => 'Install Time', 'sortable' => True);
	$fields['status'] = array('name' => 'Status', 'sortable' => True);
	$fields['groups'] = array('name' => 'Client Groups', 'sortable' => False);
	return $fields;
}

function get_default_clients_fields() {
	return 'hostname,ip,profile_name,osname,username,status,groups';
}

function get_users_fields() {
	$fields['username'] = array('name' => 'Username', 'sortable' => True);
	$fields['real_name'] = array('name' => 'Real Name', 'sortable' => True);
	$fields['comment'] = array('name' => 'Comment', 'sortable' => False);
	$fields['status'] = array('name' => 'Status', 'sortable' => True);
	$fields['groups'] = array('name' => 'User Groups', 'sortable' => False);
	return $fields;
}

function get_default_users_fields() {
	return 'username,real_name,comment,status,groups';
}

function get_jobs_fields() {
	$fields['hostname'] = array('name' => 'Hostname', 'sortable' => True);
	$fields['priority'] = array('name' => 'Priority', 'sortable' => True);
	$fields['created'] = array('name' => 'Created', 'sortable' => True);
	$fields['username'] = array('name' => 'Created By', 'sortable' => True);
	$fields['scriptname'] = array('name' => 'Script Name', 'sortable' => True);
	$fields['pending'] = array('name' => '# Pending', 'sortable' => True);
	$fields['failed'] = array('name' => '# Failed', 'sortable' => True);
	$fields['permname'] = array('name' => 'Permission', 'sortable' => True);
	$fields['description'] = array('name' => 'Description', 'sortable' => False);
	return $fields;
}

function get_default_jobs_fields() {
	return 'hostname,scriptname,priority,created,permname,pending,failed';
}

function get_priorities() {
	return array('1','2','3','4','5','6','7','8','9');
}

function get_cron_times() {
	# Minutes array
	$minutes = array( 
#		"*" => "Every minute",
#		"*/2" => "Every other minute",
		"*/5" => "Every five minutes",
		"*/10" => "Every ten minutes",
		"*/15" => "Every fifteen minutes",
	);
	foreach (range(0,59) as $val) {
		$minutes[(string)$val] = $val;
	}
	$minutes["other"] = "other";

	# Hours array
	$hours = array(
		"*" => "Every hour",
		"*/2" => "Every other hour");
	foreach (range(0,11) as $val) {
		$hours[(string)$val] = (string)$val . " A.M.";
	}
	$hours["12"] = "12 Noon";
	foreach (range(13,23) as $val) {
		$hours[(string)$val] = (string)($val-12) . " P.M.";
	}
	$hours["other"] = "other";

	# Days
	$days = array(
		"*" => "Every day",
		"*/2" => "Every other day");
	foreach (range(1,31) as $val) {
		$days[(string)$val] = (string)($val);
	}
	$days["other"] = "other";

	# Weekdays
	$weekdays = array(
		"*" => "Every weekday",
		"0" => "Sunday",
		"1" => "Monday",
		"2" => "Tuesday", 
		"3" => "Wednesday",
		"4" => "Thursday",
		"5" => "Friday",
		"6" => "Saturday");
	$weekdays["other"] = "other";

	# Months
	$months = array("*" => "Every month",
		"1" => "January",
		"2" => "February",
		"3" => "March",
		"4" => "April",
		"5" => "May",
		"6" => "June",
		"7" => "July",
		"8" => "August",
		"9" => "October",
		"10" => "September",
		"11" => "November",
		"12" => "December");
	$months["other"] = "other";
	return array($minutes,$hours,$days,$weekdays,$months);
}

?>
