<?php

require_once($smarty_dir . 'Smarty.class.php');

$smarty = new Smarty();
$smarty->template_dir = $basedir . '.smarty/templates';
$smarty->compile_dir = $basedir . '.smarty/templates_c';
$smarty->cache_dir = $basedir . '.smarty/cache';
$smarty->config_dir = $basedir . '.smarty/config';

$smarty->assign('baseurl', $baseurl);

# Top navigation bar
$topbar_left = array(
  array('name' => 'Overview', 'link' => 'index.php'),
  array('name' => 'Jobs', 'link' => 'jobs.php'),
  array('name' => 'Clients', 'link' => 'clients.php')
);

$topbar_right = array(
  array('name' => 'Settings', 'link' => 'settings.php'),
  array('name' => 'Help', 'link' => 'help.php')
);

#if ($_SESSION['username'] eq 'is in the administrators group') {
  $topbar_left[] = array('name' => 'Users', 'link' => 'users.php');
#}
$topbar_left[] = array('name' => 'Script Library', 'link' => 'script_library.php');
$topbar_left[] = array('name' => 'Modules', 'link' => 'modules.php');

$smarty->assign('topbar_left', $topbar_left);
$smarty->assign('topbar_right', $topbar_right);

?>
