<?php

require_once($ADOdb_path . 'adodb-exceptions.inc.php');
require_once($ADOdb_path . 'adodb.inc.php');
	
class DB {
	private $dbi;
	private $error;
	

	#######################################################################
	function __construct($db_host, $db_username, $db_password, $db_name, $db_type) {


		try {
			$dbi = NewADOConnection($db_type);
			$dbi->Connect($db_host, $db_username, $db_password, $db_name);
			$dbi->SetFetchMode(ADODB_FETCH_ASSOC);
		} catch (exception $e) {
			print_r($e);
		}
		$this->dbi = $dbi;
	}
	
	#######################################################################
	function __get($name) {
		switch ($name) {
		case 'error':
			return $this->error;
			break;
		default:
			return false;
			break;
		}
	}
	
	#######################################################################
	function __set($name, $value) {
		return false;
	}
	
	function __isset($name) {
		switch ($name) {
			case 'error':
				if ($this->error && strlen($this->error) > 0) {
					return true;
				}
				break;
			default:
				return false;
				break;
		}
	}
	
	#######################################################################
	function __unset($name) {
		return false;
	}
	
  # $table  = 'table1' OR array('table1', 'table2')
  # $select = '*' OR 'column1' OR array('column1', 'column2')
  # $query  = 'SELECT * FROM table WHERE column = \'value\' OR column = (SELECT * FROM table2 WHERE column = \'value\')'
  # $set    = array('setcolumn' => 'value')
  # $update = array('updatecolumn' => 'value')
  # $where  = '`matchcolumn` = \'criteria\''
  # insert($table, $set)
  # update($table, $update)
  # update($table, $update, $where)
  # delete($table, $where)
  # select($select, $table)
  # select($select, $table, $where)
  # select($query)
  
  #######################################################################
	function __call($method, $args) {
		$dbi = $this->dbi;
		#print "DEBUG: $method <BR>";
		#print_r($args);
		
		switch ($method) {
			case 'insert':
				try {
					$result = $dbi->AutoExecute($args[0], $args[1], 'INSERT');
				} catch (exception $e) {
					print_r($e);
				}
				if (!$result) {
					print $dbi->ErrorMsg();
					return false;
				} else {
					return $result;
				}
				break;
			case 'update':
				try {
					$result = $dbi->AutoExecute($args[0], $args[1], 'UPDATE', $args[2] );
				} catch (exception $e) {
					print_r($e);
				}
				if (!$result) {
					print $dbi->ErrorMsg();
					return false;
				} else {
					return $result;
				}
				break;
				
				break;
			case 'delete':
				if ($args && count($args) == 2) {
					$query = strtoupper($method);
					$query .= ' FROM `' . $args[0] . '`';
					$query .= ' WHERE ' . $args[1];
					#print $query . "<BR>"; #for debugging.
					try {
					#	$sql = $dbi->Prepare($query);
						$recordSet = $dbi->Execute($query);
					} catch (exception $e) {
						print_r($e);
					}
					if (!$recordSet) {
						print $dbi->ErrorMsg();
						return false;
					} else {
						return $recordSet->RecordCount();
					}
				} else {
					return false;
				}
				break;
			case 'select':
				if ($args && (count($args) >= 1 && count($args) <= 3)) {
					if (count($args) == 1) {
						$query = $args[0];
					} else {
						$query = strtoupper($method);
						if (is_array($args[0])) {
							foreach ($args[0] as $value) {
								$query .= ' ' . $value . ', ';
							}
						} else {
							$query .= '*';
						}
						$query = preg_replace('/, $/', '', $query);
						$query .= ' FROM ' . $args[1] ;
						if (count($args) == 3) {
							$query .= ' WHERE ' . $args[2];
						}
					}
					#print $query; #for debugging.
					try {
						$sql = $dbi->Prepare($query);
						$recordSet = $dbi->Execute($sql);
					} catch (exception $e) {
						print_r($e);
					}
					if (!$recordSet) {
						print $dbi->ErrorMsg();
						return false;
					} else {
						$rows = array();
						while (!$recordSet->EOF) {
							$rows[] = $recordSet->fields;
							$recordSet->MoveNext();
						}    
						$recordSet->Close(); # optional
						#print_r($rows);
						return $rows;
					}

				} else {
					return false;
				}
				break;
			
			case 'query':
				if (count($args) > 2) {
					return false;
				}
				
				$query = $args[0];
				#print $query . "<BR>"; #for debugging.
				try {
					$sql = $dbi->Prepare($query);
					if (count($args) == 2) {
						$recordSet = $dbi->Execute($sql,$args[1]);
					} else {
						$recordSet = $dbi->Execute($sql);
					}
				} catch (exception $e) {
					print_r($e);
				}
				if (!$recordSet) {
					print $dbi->ErrorMsg();
					return false;
				} else {
					$rows = array();
					while (!$recordSet->EOF) {
						#print_r($recordSet->fields);
						$rows[] = $recordSet->fields;
						$recordSet->MoveNext();
					}    
					$recordSet->Close(); # optional
					return $rows;
				}
				break;

			case 'get_next_id':
				if (!$args[0]) {
					return false;
				}
				try {
					$id = $dbi->GenID($args[0],10); #Generate an ID for the table, start at 10 if not defined yet.
				} catch (exception $e) {
					print_r($e);
				}
				return $id;
			case 'now':
				try {
					$time = $dbi->DBDate(time());
				} catch (exception $e) {
					print_r($e);
				}
			default:
				$this->error = 'unknown method';
				return false;
				break;
		}
	}
}

?>
