<?php

require_once($path . './phpgacl/gacl.class.php');
require_once($path . './phpgacl/gacl_api.class.php');

$gacl_options = array(
  'db_type' => 'mysql',
  'db_host' => $db_host,
  'db_user' => $db_username,
  'db_password' => $db_password,
  'db_name' => $db_name,
  'db_table_prefix' => 'gacl_',
#  'caching' => 'FALSE',
  'smarty_dir' => preg_replace('/\/$/', '', $smarty_dir),
  'smarty_template_dir' => 'templates',
  'smarty_compile_dir' => 'templates_c'
);

$acl = new gacl_api($gacl_options);  #can be gacl_api

function check_access($permname) {
	global $acl;
	global $scire_ui_section;
	if ($_SESSION['username'] == "root") { return True; }  #godmode
	if ($acl->acl_check($scire_ui_section, $permname, "users", $_SESSION['username'],$scire_ui_section, $scire_ui_section) ) {
		return True;
	} else {
		return False;
	}
	return False;
}

function check_action_access($permname) {  #Same as above but aborts the page for you.
	global $acl;
	global $scire_ui_section;
	if ($_SESSION['username'] == "root") { return True; }  #iddqd
	$result = $acl->acl_check($scire_ui_section, $permname, "users", $_SESSION['username'],$scire_ui_section, $scire_ui_section);
	if ($result) {
		#print "Access Granted! $result";
		return True;
	} else {
		header('Location: ' . $baseurl . 'access_error.php?perm=' . urlencode($permname));
		exit(0);
		#print "DENIED! " + $result;
	}
}
?>
