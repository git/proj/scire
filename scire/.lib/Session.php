<?php

class Session {
  private $db;
  private $table;
  private $maxidle;
  private $error;
  function __construct($db, $table) {
    $this->db = $db;
    $this->table = $table;
    session_set_save_handler(array(&$this, '_open'), array(&$this, '_close'), array(&$this, '_read'), array(&$this, '_write'), array(&$this, '_destroy'), array(&$this, '_gc'));
    session_start();
  }
  function __destruct() {
    session_write_close();
  }
  function __get($name) {
    switch ($name) {
      case 'error':
        return $this->error;
        break;
      default:
        return false;
        break;
    }
  }
  function __set($name, $value) {
    return false;
  }
  function __isset($name) {
    switch ($name) {
      case 'error':
        if ($this->error && strlen($this->error) > 0) {
          return true;
        }
        break;
      default:
        return false;
        break;
    }
  }
  function __unset($name) {
    return false;
  }
  function _open($savepath, $sname) {
    #$this->maxidle = get_cfg_var('session.gc_maxlifetime');
    $this->maxidle = 3600;
  }
  function _close() {
    $this->_gc($this->maxidle);
  }
  function _read($sid) {
    $result = $this->db->select(array('data'), $this->table, '`sessionid` = \'' . $sid . '\' AND `expiration` > \'' . time() . '\'');
    if ($result && (count($result) > 0 && array_key_exists('data', $result[0]))) {
      return $result[0]['data'];
    } else {
      $this->error = $this->db->error;
      return '';
    }
  }
  function _write($sid, $sdata) {
    $expiration = time() + $this->maxidle;
	$result = $this->db->query("INSERT INTO `".$this->table."` (`sessionid`,`expiration`, `data`) VALUES (?,?,?) ON DUPLICATE KEY UPDATE `expiration`=?, `data`=?", array($sid, $expiration, $sdata, $expiration, $sdata) );
    #$result = $this->db->insert($this->table, array('sessionid' => $sid, 'expiration' => $expiration, 'data' => $sdata), array('expiration' => $expiration, 'data' => $sdata));
    if ($result) {
      return true;
    } else {
      $this->error = $this->db->error;
      return false;
    }
  }
  function _destroy($sid) {
    $username = '';
    $result = $this->db->select(array('data'), $this->table, '`sessionid` = \'' . $sid . '\'');
    if ($result && (count($result) > 0 && array_key_exists('data', $result[0]))) {
      if (preg_match('/^.*username\|s:[0-9]+:"([^"]+)".*$/', $result[0]['data'], $matches)) {
        $username = $matches[1];
      }
    }
    $result = $this->db->delete($this->table, '`sessionid` = \'' . $sid . '\'');
    if ($result) {
      if (strlen($username) > 0) {
        $result = $this->db->delete($this->table, '`data` regexp \'^.*username\|s:[0-9]+:"' . $username . '".*$\'');
        if ($result) {
          return true;
        } else {
          $this->error = $this->db->error;
          return false;
        }
      } else {
        return true;
      }
    } else {
      $this->error = $this->db->error;
      return false;
    }
  }
  function _gc($smaxidle) {
    $result = $this->db->delete($this->table, '`expiration` < \'' . time() . '\'');
    if ($result) {
      return true;
    } else {
      $this->error = $this->db->error;
      return false;
    }
  }
  function destroy() {
    $_SESSION = array();
    if (isset($_COOKIE[session_name()])) {
      setcookie(session_name(), '', time()-420000, '/');
    }
    session_destroy();
  }
}

?>
