<?php

$CONFIG_DIR='/etc/scire';

# Validate the path if it exists.
if (isset($path)) {
  if ($path != "../") {
    header('Content-type: text/html; charset=UTF-8');
    $smarty->assign('title', 'Path Error');
    $smarty->assign('message', 'invalid path.  stop trying to hack me you mofo!');
    $smarty->display('message.tpl');
    exit(0);
  }
} else {
  $path = "";
}

# Configuration

require_once($CONFIG_DIR.'/config.php');

# Functions
require_once('functions.php');

# Clean config
$basedir = normalize_path($basedir);
$baseurl = normalize_path($baseurl);
$smarty_dir = normalize_path($smarty_dir);

# Smarty
require_once('smarty.php');

# Database class
require_once('DB.php');
$db = new DB($db_host, $db_username, $db_password, $db_name, $db_type);
if (isset($db->error)) {
  header('Content-type: text/html; charset=UTF-8');
  $smarty->assign('title', 'Database Error');
  $smarty->assign('message', $db->error);
  $smarty->display('message.tpl');
  exit(0);
}

# Database functions
require_once('DB_functions.php');

# Session class
require_once('Session.php');
$session = new Session($db, $db_sessions_table);
if (isset($session->error)) {
  header('Content-type: text/html; charset=UTF-8');
  $smarty->assign('title', 'Session Error');
  $smarty->assign('message', $session->error);
  $smarty->display('message.tpl');
  exit(0);
}

# ACL class
require_once('phpGACL.php');

# Check login
if (!isset($_SESSION['username'])) {
  header('Location: ' . $baseurl . 'login.php?afterlogin=' . urlencode($_SERVER['REQUEST_URI']));
  exit(0);
} else {
  $smarty->assign('userid', $_SESSION['userid']);
  $smarty->assign('username', $_SESSION['username']);
  $smarty->assign('useremail', $_SESSION['useremail']);
  #Set Theme
  $smarty->assign('theme', $_SESSION['settings']['theme']);
}

?>
