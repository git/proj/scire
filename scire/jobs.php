<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();
$leftbar_submenu = array();
$template = "jobs.tpl";
$error_msg = "";

check_action_access("View Jobs");
############################################
## Deal with submitted forms first.

#Set information about the fields (display name, sortable)
$f_attr = get_jobs_fields();
$smarty->assign('f_attr', $f_attr);

if (!$_GET['sorton']) {
	$_GET['sorton'] = 'jobid';
}
if (!$_GET['sortdir']) {
	$_GET['sortdir'] = 'asc';
}
$smarty->assign('sorton', $_GET['sorton']);
$smarty->assign('sortdir', $_GET['sortdir']);

#Grab the field list.
if ($_SESSION['settings']['jobs_page_fields']) {
	$fields = explode(',', $_SESSION['settings']['jobs_page_fields']);
} else {
	$fields = explode(',', get_default_jobs_fields());
}
#$fields = array("hostname", "ip", "mac", "digest", "status", "installtime");
$smarty->assign('fields',$fields);

$jobs = get_scire_jobs($_GET['sorton'], $_GET['sortdir'], 'All');
$smarty->assign('jobs', $jobs);


#$jobs = $db->select(array("c.hostname", "j.*"),  #FROM
#					"clients c, jobs j, jobs_clients jc",  #WHERE 
#					"j.jobid = jc.jobid AND jc.clientid = c.clientid");
#print "<pre>";
#pre_var_dump($jobs); #For debugging.
#$client_groups = array();
foreach( $jobs as $key => $job) {
	$id = $acl->get_object_id("clients",$job['hostname'],'AXO');
	$groups = $acl->get_object_groups($id,'AXO');
	$group_string = "";
	if ($groups) {
		foreach ($groups as $group) {
			$group_data = $acl->get_group_data($group, 'AXO');
			$group_string .= $group_data[3]. " | ";
			$client_groups["$group"] = $group_data[3];
		}
	}
	$jobs[$key]['groups'] = $group_string;	
}
#print "</pre>";

$smarty->assign('jobs', $jobs);

if ($_POST['searchfield']) {
	$error_msg .= "Um, yeah... not done yet.";
}

#Gather groups of clients. it's a phpgacl command.
$smarty->assign('leftbar_submenu_title', "Actions");
if ( check_access("Add Job") ) {
	add_leftbar($leftbar_submenu, "Add Job", "add_job.php");
}
$smarty->assign('client_groups', $client_groups);
$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('leftbar_submenu', $leftbar_submenu);

$smarty->display('jobs.tpl');
?>
