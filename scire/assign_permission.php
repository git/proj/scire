<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();

add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "users.php");


$userid = "";


#####################################################
#Deal with submitted forms.

###############################################
#Adding a permission.
if ($_POST['addperm_submit'] or $_POST['editperm_submit']) {
	if ($_POST['newpermname'] and $_POST['newpermdesc']) {  #Add a new permission FIXME add check of auth for this.
		if ($_POST['perm_category'] == "new") {
			if ($_POST['newpermcat']) {  #Need to add a section
				$acl->add_object_section($_POST['newpermcat'], $_POST['newpermcat'], 1, 0, 'ACO');
				$_POST['perm_category'] = $_POST['newpermcat'];
			} else {  #BAD user
				$status .= "ERROR: No new permission category specified.  Using Default";
				$_POST['perm_category'] = "Default";
			}
		}
		$obj_id = $acl->add_object($_POST['perm_category'], $_POST['newpermname'], $_POST['newpermname'], 1, 0, 'ACO');
		$status .= "Obj id: $obj_id";
		$result = scire_add_permission($obj_id, $_POST['newpermname'],$_POST['newpermdesc'], $_SESSION['userid']);
		if ($result == 1) { $status .= "Permission Addition successfull. "; }
		$_POST['perm'] = $_POST['newpermname'];
	}
	#Permission should exist now.
	$aco_array[$_POST['perm_category']] = array($_POST['perm']);
	$aro_array = array();
	$axo_array = array();
	$axo_group_array = array();
	$aro_group_array = array();
#	if ($_POST['usertype'] == "usergroups") {
		$aro_group_array = $_POST['usergroups'];
#	} else {
		$aro_array['users'] = $_POST['users'];
#	}
	
#	if ($_POST['clienttype'] == "clientgroups") {
		$axo_group_array = $_POST['clientgroups'];
#	} else {
		$axo_array['clients'] = $_POST['clients'];
#	}
	
	if (!$_POST['active']) {
		$_POST['active'] = 0;
	}
	
	if ($_POST['addperm_submit']) {
		$worked = $acl->add_acl($aco_array, $aro_array,$aro_group_array,$axo_array,$axo_group_array,$_POST['allow'],$_POST['active'],1,$_POST['note'],"system");
		if($worked) {
			$status .= "<br><b>Addition Successfull!  $worked</b>";
		} else {
			$status .= "<br><b>BAD BAD BAD $worked</b>";
		}
	} else {#Edit
		$worked = $acl->edit_acl($_POST['editing'],$aco_array, $aro_array,$aro_group_array,$axo_array,$axo_group_array,$_POST['allow'],$_POST['active'],1,$_POST['note'],"system");
		if($worked) {
			$status .= "<br><b>Edit Successfull!  $worked</b>";
		} else {
			$status .= "<br><b>BAD BAD BAD $worked</b>";
		}
	}
	
} 
###############################################
#Adding a permission.
elseif ($_POST['delete_confirm']) {
	$aclid = $_POST['aclid'];
	$worked = $acl->del_acl($aclid);
	if($worked) {
		$status .= "<br><b>Deletion Successfull.  $worked</b>";
	} else {
		$status .= "<br><b>BAD BAD BAD $worked</b>";
	}
	header('Location: ' . $baseurl . 'users.php?View=permissions');
}
elseif ($_POST['delete_cancel']) {
	header('Location: ' . $baseurl . 'users.php?View=permissions');
}


########################################################
if ($_GET['Action'] == "delete") {
	$smarty->assign('aclid', $_GET['aclid']);
}

if ($_GET['aclid']) {  #For Editing.  Load the ACL data.
	$smarty->assign('acl',$acl->get_acl($_GET['aclid']));
} 
#$permissions = get_scire_permissions();

#foreach ($permissions as $perm) {
#	$perm_categories[$perm['permcategory']] = 1;
#}

$permissions = array();
$perm_categories = array();
$all_permissions = $acl->get_objects(NULL, 1, 'ACO');
foreach ($all_permissions as $categoryname => $cat) {
	#print "Perm: $perm<br>Permname: $permname<br>";
	if ($categoryname != $scire_ui_section) {
		foreach ($cat as $perm) {
			array_push($permissions, $perm);
		}
		array_push($perm_categories, $categoryname);
	}
}
#pre_var_dump($permissions);
#pre_var_dump($perm_categories);
$smarty->assign('perm_categories',$perm_categories);
$smarty->assign('perms', $permissions);

$smarty->assign('ugroups', $acl->format_groups($acl->sort_groups('aro'), TEXT));
$smarty->assign('cgroups', $acl->format_groups($acl->sort_groups('AXO'), TEXT));

$users = get_scire_users('username', 'asc');
$smarty->assign('users', $users);
$clients = get_scire_clients('hostname', 'asc');
$smarty->assign('clients', $clients);

$smarty->assign('get', $_GET);
$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display('assign_permission.tpl');
#pre_var_dump($_POST);
?>
