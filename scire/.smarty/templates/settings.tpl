{include file="header.tpl" title="Settings"}
<form action="" method="post" name="settingsform">

  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle">Settings</td></tr>
  <tr><td>
  
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td><label for="start_page">Start Page:</label></td>
	<td><input type="text" name="start_page" value="{$start_page}"></td>
  </tr>
  <tr>
    <td><label for="theme">Theme:</label></td>
	<td><select name="theme">
	{section name=theme loop=$themes}
	<option value="{$themes[theme].link}" {if $themes[theme].link eq $theme} selected {/if}
	>{$themes[theme].name}</option>
	{/section}
	</select></td>
  </tr>
	<tr>
    <td><label for="language">Language (not yet available):</label></td>
  	<td><select name="language">
	{section name=lang loop=$languages}
	<option value="{$languages[lang].value}">{$languages[lang].name}</option>
	{/section}
	</select></td>
  </tr>
  <tr>
    <td><label for="notification">Send Notifications to:</label></td>
  	<td><input type="radio" name="notification" value="phone" {if $notification eq "phone"} checked {/if}>Phone</td>
		<td><input type="radio" name="notification" value="pager" {if $notification eq "pager"} checked {/if}>Pager</td>
  </tr>
  <tr>
    <td><label for="per_page">Max Jobs/Clients to show per page:</label></td>
		<td><input type="radio" name="per_page" value="2" {if $per_page eq "2"} checked {/if}>25</td>
		<td><input type="radio" name="per_page" value="3" {if $per_page eq "3"} checked {/if}>50</td>
		<td><input type="radio" name="per_page" value="10" {if $per_page eq "10"} checked {/if}>100</td>
  </tr>
	<tr>
	  <td><label for="vacation">On Vacation, contact:</label>
  
  </td></tr>
  <tr><td><input type="submit" name="save_settings" value="Save Settings"></td>
  </tr>
  </table>

</form>
{include file="footer.tpl"}
