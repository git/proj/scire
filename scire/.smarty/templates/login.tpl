{include file="header.tpl" title="Login"}

<div style="display: table; width: 100%; height: 100%; _position: relative; overflow: hidden;">
<div style="_position: absolute; _top: 50%; display: table-cell; vertical-align: middle;">
<div style="_position: relative; _top: -50%;">
<table class="box" align="center" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td class="boxtitle">Please Login to use Scire</td>
  </tr>
	<tr>
	  <td>
		<form id="login" name="login" method="post" action="">
		  <table border="0" cellspacing="5" cellpadding="5">
			<tr>

			  <td><label for="username">Username:</label></td>
			  <td><input type="text" id="username" name="username" value="" /></td>
			</tr>
			<tr>
			  <td><label for="passwd">Password:</label></td>
			  <td><input type="password" id="passwd" name="passwd" /></td>
			</tr>
			<tr>

			  <td colspan="2" align="center">
				<input type="submit" name="submit" value="Login" />
			  </td>
			</tr>
		  </table>
		</form>
	  </td>
	</tr>
  </table>

</div>
</div>
</div>

{include file="footer.tpl"}
