{include file="header.tpl" title="Scire User Interface Permissions"}
<form action="" method="post" name="ui_permform">
{if $Action eq "delete"}
<b>Are you absolutely SURE you want to delete this ACL?</b>
<input type="hidden" name="aclid" value="{$aclid}">
<input type="submit" name="delete_confirm" value="Yes, that's why I clicked 'Delete', duh!"><input type="submit" name="delete_cancel" value="No wait!">
{elseif $Action eq "add" or $Action eq "edit"}
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td align="center" valign="top">
	  <table class="box" width="50%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">User Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="usergroups">Usergroups:</label></td>
				<td><label for="users">Users:</label></td>
			  </tr>
			  <tr>
				  {if isset($get.username)}
				    <td>Selected User:</td>
					<td><input type="hidden" name="users[]" value="{$get.username}">{$get.username}</td>
				  {elseif isset($get.usergroup)}
				  {else}
				    <td>
					  <select name="usergroups[]" size="18" multiple="multiple">
					  {foreach from=$ugroups item=ugroup key=id}
						<option value="{$id}"
						{if isset($acl)}
						  {foreach from=$acl.aro_groups item=group key=groupid}
			  				{if $id eq $group} selected {/if}
						  {/foreach}
						{/if}
						>{$ugroup}</option>
					  {/foreach}
					  </select>
					</td>
					<td>
					  <select name="users[]" size="18" multiple="multiple">
					  {section loop=$users name=users}
						<option value="{$users[users].username}"
						{if isset($acl)}
						  {foreach from=$acl.aro.users item=user key=key2}
			  				{if $users[users].username eq $user} selected {/if}
						  {/foreach}
						{/if}
						>{$users[users].username}</option>
					  {/section}
					  </select>
					</td>
				  {/if}
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
	<td align="center" valign="top">
	  <table class="box" width="50%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Permission Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="perm">Permissions (choose an existing or create a new one):</label></td>
			  </tr>
			  <tr>
				<td>
				  <select name="perm" size="7">
				  {foreach from=$perms item=perm key=id}
					<option value="{$perm}"
					{if isset($acl)}
					  {foreach from=$acl.aco item=ignore key=perm_category}
					    {if $perm eq $acl.aco.$perm_category[0]} selected {/if}
					  {/foreach}
					{/if}
					>{$perm}</option>
				  {/foreach}
				  </select>
				</td>
			  </tr>
			  <tr>
			    <td><label for="newpermname">New Permission Name:</label><input name="newpermname" type="text" size="20"></td>
			  </tr>
			  <tr>
			    <td><label for="newpermdesc">New Permission Description:</label></td>
			  </tr>
			  <tr>
			    <td><textarea name="newpermdesc" rows="3" cols="20"></textarea></td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table
	</td>
  </tr>
  <tr>
    <td align="center"><input type="radio" name="allow" value="1" 
	{if isset($acl)}
		{if $acl.allow} checked {/if}
	{else}
	  checked
	{/if}
	>Allow 
	<input type="radio" name="allow" value="0"
	{if isset($acl)}
		{if (!$acl.allow)} checked {/if}
	{/if}
	>Deny</td>
	<td align="center"><input type="checkbox" name="active" value="1" 
	{if isset($acl)}
		{if $acl.enabled} checked {/if}
	{else}
	  checked
	{/if}
	>Active (vs. hidden)</td>
    <td align="right" valign="bottom">
	  <label for="note">Note:</label><input type="text" name="note"
	  {if isset($acl)}
		value="{$acl.note}"
	  {/if}
	  > 
	  {if isset($acl)}
	  <input type="hidden" name="editing" value="{$acl.acl_id}">
	  <input type="submit" name="editperm_submit" value="Update Permission">
	  {else}
      <input type="submit" name="addperm_submit" value="Assign Permission">
	  {/if}
    </td>
  </tr>
</table>
{else}
<table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle">Scire User Interface Permissions</td></tr>
  <tr><td>
  
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
    <tr>
      <th>User(s)</th>
	  <th>Perm. Name</th>
	  <th width="7%">Allow/Deny</th>
	  <th>Active</th>
	  <th>Actions</th>
	</tr>
	{foreach from=$acls item=acl key=k} 
	<tr bgcolor="{cycle values="#eeeeee,#dddddd"}">
	  <td>
	    {if $acl.aro}  <!-- we have individual users -->
			{foreach from=$acl.aro.users item=user key=key2}
			  <font style="background-color:#A0F8FF">User: {$user}</font><br>
			{/foreach}
		{/if}
		{foreach from=$acl.aro_groups item=group key=groupid}
		  <font style="background-color:#FFFACD">UserGroup: {$ugroups[$group]} </font><br>
		{/foreach}
	  </td>
	  <td>
	  	{foreach from=$acl.aco item=ignore key=perm_category}
		  {$acl.aco.$perm_category[0]}
		{/foreach}
	  </td>
	  {if $acl.allow}
	  	<td style="background-color: #008000">Allow</td>
	  {else}
	    <td style="background-color: #DC143C">Deny</td>
	  {/if}
	  <td>
	    {if $acl.enabled}Yes{else}No{/if}
	  </td>
	  <td>
	    <a href="ui_permissions.php?Action=edit&aclid={$acl.acl_id}">Edit</a> 
		<a href="ui_permissions.php?Action=delete&aclid={$acl.acl_id}">Delete</a>
	  </td>
	</tr>
	{/foreach}
	
  </table><hr>
  </tr>
  <tr>
  <table><tr>
  <td>
  	Blue = Single User/Client<br>
  	Tan = UserGroup/ClientGroup
  </td>
  <td align="right"><a href="ui_permissions.php?Action=add">Assign a Permission</a></td>
  </tr>
  </table>
  </tr>
{/if}
</form>
{include file="footer.tpl"}
