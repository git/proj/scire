{include file="header.tpl" title="Clients"}
<form action="" method="post" name="edit_clientform">
{if $Action eq "addgroup"}
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr><td class="boxtitle">Add client to group</td></tr>
  <tr>
	<td colspan="2" align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
	  <tr>
		<td class="boxtitle" width="15">Add</td><td class="boxtitle">Group Name</td>
	  </tr>
	  {foreach from=$groups name=ugroup item=item key=id}
	  <tr>
	    <td><input type="checkbox" name="addgroupid[]" value="{$id}"
		{if isset($ingroups)} {if in_array($id, $ingroups)} checked {/if} {/if}
		></td>
	    <td>{$item}</td>
	  </tr>
      {/foreach}
	  <tr><td>
	    <input type="hidden" name="clientid" value="{$clientid}">
	    <input type="submit" name="addgroup_confirm" value="Save Changes">
	  </td></tr>
	  </table>
	</td>
  </tr>
  </table>
{elseif $Action eq "create"}
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td colspan="2" align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Client</td><td class="boxtitle">Groups</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="addhostname">*Hostname:</label></td>
				<td><input type="text" id="addhostname" name="addhostname" /></td>
				<td><label for="ip">*IP Address:</label></td>
				<td><input type="text" id="ip" name="ip" /></td>
				
			  </tr>
			  <tr>
				<td><label for="assetid">Asset ID:</label></td>
				<td><input type="text" id="assetid" name="assetid" /></td>
				<td><label for="mac">*MAC:</label></td>
				<td><input type="text" id="mac" name="mac" /></td>

			  </tr>
			  <tr>
				<td><label for="digest">Digest (DO NOT EDIT):</label></td>
				<td colspan="3"><input type="text" id="digest" name="digest" size="58"></td>
				</tr>
				<tr>
				<td><label for="gli_profile">GLI Profile:</label></td>
				<td><select name="gli_profile" size="1">
				{section name=p loop=$gli_profiles}
				<option value="{$gli_profiles[p].profileid}">{$gli_profiles[p].profile_name}</option>
				{/section}
				</select>
				</td> 
				<td><label for="status">Status:</label></td>
				<td><select id="status" name="status" size="1">
					{section name=s loop=$client_statuses}
				<option value="{$client_statuses[s].statusid}">{$client_statuses[s].statusname}</option>
				{/section}
				</select>
				</td>
			  </tr>
			  <tr>
				  <td><label for="osid">OS:</label></td>
				<td><select name="osid" size="1">
				{section name=o loop=$oss}
				<option value="{$oss[o].osid}">{$oss[o].osname}</option>
				{/section}
				</select>
				</td>
				<td><label for="contact">Contact:</label></td>
				<td><select name="contact" size="1">
				{section name=con loop=$users}
				<option value="{$users[con].userid}">{$users[con].username}</option>
				{/section}
				</select></td>
			  </tr>
			  <tr>
			    <td><label for="installtime">Install Time <br>(leave blank for NOW):</label></td>
				<td colspan="3"><input type="text" id="installtime" name="installtime" size="25"/></td>
			  </tr>
			</table>
			<p><input type="submit" name="AddClientSubmit" value="Add Client" /></p>
		  </td>
		  <td>
		  <table>
		    <tr><td>Assign client to the following groups:</td></tr>
		    <tr><td>
			<select name="groups[]" size="10" multiple="multiple" id="groups">
		  	{foreach from=$groups item=ugroup key=id}
			<option value="{$id}">{$ugroup}</option>
			{/foreach}
        	</select>
			</td>
		  </tr></table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
{elseif $Action eq "edit"}
<h1>EDITING</h1>
 <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td colspan="2" align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Client</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="addhostname">*Hostname:</label></td>
				<td><input type="text" id="edithostname" name="edithostname" value="{$client.hostname}" /></td>
				<td><label for="ip">*IP Address:</label></td>
				<td><input type="text" id="ip" name="ip" value="{$client.ip}" /></td>
				
			  </tr>
			  <tr>
				<td><label for="assetid">Asset ID:</label></td>
				<td><input type="text" id="assetid" name="assetid" value="{$client.assetid}" /></td>
				<td><label for="mac">*MAC:</label></td>
				<td><input type="text" id="mac" name="mac" value="{$client.mac}" /></td>

			  </tr>
			  <tr>
				<td><label for="digest">Digest (DO NOT EDIT):</label></td>
				<td colspan="3"><input type="text" id="digest" name="digest" size="55" value="{$client.digest}" /></td>
				</tr>
				<tr>
				<td><label for="gli_profile">GLI Profile:</label></td>
				<td><select name="gli_profile" size="1">
				{section name=p loop=$gli_profiles}
				<option value="{$gli_profiles[p].profileid}" {if $gli_profiles[p].profileid eq $client.gli_profile} selected {/if}
				>{$gli_profiles[p].profile_name}</option>
				{/section}
				</select>
				</td> 
				<td><label for="status">Status:</label></td>
				<td><select id="status" name="status" size="1">
					{section name=s loop=$client_statuses}
				<option value="{$client_statuses[s].statusid}" {if $client_statuses[s].statusid eq $client.status} selected {/if}>{$client_statuses[s].statusname}</option>
				{/section}
				</select>
				</td>
			  </tr>
			  <tr>
				  <td><label for="osid">OS:</label></td>
				<td><select name="osid" size="1">
				{section name=o loop=$oss}
				<option value="{$oss[o].osid}" {if $oss[o].osid eq $client.osid} selected {/if}
				>{$oss[o].osname}</option>
				{/section}
				</select>
				</td>
				<td><label for="contact">Contact:</label></td>
				<td><select name="contact" size="1">
				{section name=con loop=$users}
				<option value="{$users[con].userid}" {if $users[con].userid eq $client.contact} selected {/if}
				>{$users[con].username}</option>
				{/section}
				</select></td>
			  </tr>
			  <tr>
			    <td><label for="installtime">Install Time <br>(leave blank for NOW):</label></td>
				<td colspan="3"><input type="text" id="installtime" name="installtime" size="25" value="{$client.installtime}" /></td>
			  </tr>
			</table><input type="hidden" name="clientid" value="{$client.clientid}">
			<p><input type="submit" name="EditClientSubmit" value="Save Changes" /></form></p>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
  <tr>
	<td align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Groups</td>
		</tr>
		<tr>
		  <td>
			<table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
			  <tr>
				<th>Groupname</th>
				<th>Actions</th>
			  </tr>
			  {section name=group loop=$groups}
			  <tr>
				<td>{$groups[group].name}{if $groups[group].parent} (subgroup of {$groups[group].parent}){/if}</td>
				<td><form id="form1" name="form1" method="post" action=""><input type="hidden" name="group_id" value="{$groups[group].id}"><input type="submit" name="remove_ug" value="Remove"></form></td>
			  </tr>
			  {/section}
			  <tr><td><a href="client.php?clientid={$client.clientid}&Action=addgroup">Add client to a group</a></td></tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
	<td align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Permissions</td>
		</tr>
		<tr>
		  <td>
			<table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
			  <tr>
				<th>Category -> Permission</th>
				<th>From User(s)</th>
				<th>Actions</th>
			  </tr>
			  {foreach from=$acls item=acl key=k}
			  <tr>
				<td>{foreach from=$acl.aco item=perm key=category}{$category} -> {$perm[0]}{/foreach}</td>
				<td>
					{if $acl.aro}  <!-- we have individual users -->
					  {foreach from=$acl.aro.users item=user key=key2}
			  		<font style="background-color:#A0F8FF">User: {$user}</font><br>
					  {/foreach}
		  			{/if}
					{foreach from=$acl.aro_groups item=group key=groupid}
			  		  <font style="background-color:#FFFACD">UserGroup: {$ugroups[$group]} </font><br>
					{/foreach}
				</td>
				<td><form id="form1" name="form1" method="post" action=""><input type="hidden" name="perm_id" value="{$acl.acl_id}" /><input type="submit" name="submit" value="Remove" /></form></td>
			  </tr>
			  {/foreach}
			  <tr><td><a href="assign_permission.php?hostname={$client.hostname}">Assign a permission for this client</a></td></tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
</table>
{elseif $Action eq "delete"}
<h1>DELETING</h1>
<b>Are you absolutely SURE you want to delete client {$client.hostname}?</b>
<input type="hidden" name="clientid" value="{$client.clientid}">
<input type="submit" name="delete_confirm" value="Yes, get rid of the bastard!"><input type="submit" name="delete_cancel" value="No wait!">
{/if}

{include file="footer.tpl"}
