{include file="header.tpl" title="$View Page Settings"}
<form action="" method="post" name="settingsform">

  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle" width="3%">Sort On</td><td class="boxtitle" width="10%">View Order (blank to not show)</td><td class="boxtitle">Field Name</td></tr>
  {foreach from=$fields item=item key=key}
  <tr class="table">
    <td>{if $item.sortable}
	  <input type="radio" name="sorton" value="{$key}" 
	  {if $sorton eq $key} checked {/if}
	  >{/if}
	</td>
	<td><input type="text" name="{$View}_{$key}" 
	value="{if $curr_list[$key]}{$curr_list[$key]}{/if}"
	size="5"></td>
	<td>{$item.name}</td>
  </tr>
  {/foreach}
  <tr>
    <td><label for="sortdir">Sort Direction:</label></td>
	<td>
	  <select name="sortdir">
	    <option value="asc" {if $sortdir eq "asc"} selected {/if}>Ascending</option>
		<option value="desc" {if $sortdir eq "desc"} selected {/if}>Descending</option>
	  </select>
	</td>
  </tr>
  <tr><td><input type="submit" name="save_{$View}_settings" value="Save Settings"></td></tr>
  </table>

</form>
{include file="footer.tpl"}
