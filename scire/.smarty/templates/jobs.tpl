{include file="header.tpl" title="Jobs"}
<form action="" method="post" name="jobsform">
<p>
  <strong>Search filter: 
    <input type="text" name="searchfield">
  </strong>
</p>
{if $jobs}
  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle">Jobs</td></tr>
  <tr><td>
  
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
  {foreach from=$fields item=i key=k}
    <th>
	  <a href="jobs.php{if $f_attr[$i].sortable}?sorton={$i}&sortdir={if $sorton eq $i and $sortdir eq "desc"}asc{else}desc{/if}{/if}">{$f_attr[$i].name}</a> {if $f_attr[$i].sortable}<img src="images/sort_{if $sorton eq $i and $sortdir eq "desc"}desc{else}asc{/if}.png">{/if}
	</th>
  {/foreach}

	<th>Actions</th>
  </tr>
  {section name=jobs loop=$jobs}
  <tr>
    {foreach from=$fields item=item key=key}
	  {if $item eq "hostname"}
	  <td><a href="job.php?jobid={$jobs[jobs].jobid}&Action=edit">{$jobs[jobs].hostname}</a></td>
	  {else}
	  <td>{$jobs[jobs].$item}</td>
	  {/if}
	{/foreach}
    <td>
	  <a href="job.php?jobid={$jobs[jobs].jobid}&Action=view">View Details</a>
	  <a href="job.php?jobid={$jobs[jobs].jobid}&Action=edit">Edit</a>
	  <a href="job.php?jobid={$jobs[jobs].jobid}&Action=delete">Delete</a>
	  
	</td>
  </tr>
  {/section}

  </td></tr>
  
  </table>
{/if}
</form>
{include file="footer.tpl"}
