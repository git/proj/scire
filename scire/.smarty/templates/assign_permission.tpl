{include file="header.tpl" title="Assign A Permission"}
<form action="" method="post" name="add_permform">
{if $Action eq "delete"}
<b>Are you absolutely SURE you want to delete this ACL?</b>
<input type="hidden" name="aclid" value="{$aclid}">
<input type="submit" name="delete_confirm" value="Yes, that's why I clicked 'Delete', duh!"><input type="submit" name="delete_cancel" value="No wait!">
{else}
Instructions:<br>
1. Select a set of users and/or usergroups (any combination, they are exclusive)<br>
2. Select a permission category and then the permission to use, or choose to create a new one<br>
3. Select a set of clients and/or clientgroups (again, groups are exclusive from individual clients)<br>
4. Select options below and give a note for the permission.
 <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td align="left" valign="top">
	  <table class="box" width="50%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">User Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="usergroups">Usergroups:</label></td>
				<td><label for="users">Users:</label></td>
			  </tr>
			  <tr>
				  {if isset($get.username)}
				    <td>Selected User:</td>
					<td><input type="hidden" name="users[]" value="{$get.username}">{$get.username}</td>
				  {elseif isset($get.usergroup)}
				  {else}
				    <td>
					  <select name="usergroups[]" size="18" multiple="multiple">
					  {foreach from=$ugroups item=ugroup key=id}
						<option value="{$id}"
						{if isset($acl)}
						  {foreach from=$acl.aro_groups item=group key=groupid}
			  				{if $id eq $group} selected {/if}
						  {/foreach}
						{/if}
						>{$ugroup}</option>
					  {/foreach}
					  </select>
					</td>
					<td>
					  <select name="users[]" size="18" multiple="multiple">
					  {section loop=$users name=users}
						<option value="{$users[users].username}"
						{if isset($acl)}
						  {foreach from=$acl.aro.users item=user key=key2}
			  				{if $users[users].username eq $user} selected {/if}
						  {/foreach}
						{/if}
						>{$users[users].username}</option>
					  {/section}
					  </select>
					</td>
				  {/if}
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
	<td align="center" valign="top">
	  <table class="box" width="50%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Permission Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
			    <td><label for="perm_category">Category:</label>	</td>
			  </tr>
			  <tr>
			    <td>
				<select name="perm_category">
				{foreach from=$perm_categories item=cat }
				  <option value="{$cat}"
				  {if isset($acl)}
				    {foreach from=$acl.aco item=ignore key=perm_category}
					  {if $cat eq $perm_category} selected {/if}
				    {/foreach}
				  {/if}
				  >{$cat}</option>
				{/foreach}
				<option value="new">New (specify below)</option>
				</select>
				</td>
			  </tr>
			  <tr>
				<td><label for="perm">Permissions (choose an existing or create a new one):</label></td>
			  </tr>
			  <tr>
				<td>
				  <select name="perm" size="7">
				  {foreach from=$perms item=perm key=id}
					<option value="{$perm}"
					{if isset($acl)}
					  {foreach from=$acl.aco item=ignore key=perm_category}
					    {if $perm eq $acl.aco.$perm_category[0]} selected {/if}
					  {/foreach}
					{/if}
					>{$perm}</option>
				  {/foreach}
				  </select>
				</td>
			  </tr>
			  <tr>
			    <td><label for="newpermname">New Category (optional):</label><input name="newpermcat" type="text" size="20"></td>
			  </tr>
			  <tr>
			    <td><label for="newpermname">New Permission Name:</label><input name="newpermname" type="text" size="20"></td>
			  </tr>
			  <tr>
			    <td><label for="newpermdesc">New Permission Description:</label></td>
			  </tr>
			  <tr>
			    <td><textarea name="newpermdesc" rows="3" cols="20"></textarea></td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table
	</td>
	<td align="right" valign="top">
	  <table class="box" width="50%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Client Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="clientgroups">Clientgroups:</label></td>
				<td><label for="clients">Clients:</label></td>
			  </tr>
			  <tr>
			    {if isset($get.hostname)}
					<td>Selected Client:</td>
					<td><input type="hidden" name="clients[]" value="{$get.hostname}">{$get.hostname}</td>
				{elseif isset($get.clientgroup)}
				{else}
					<td>
					  <select name="clientgroups[]" size="18" multiple="multiple">
					  {foreach from=$cgroups item=cgroup key=id}
						<option value="{$id}"
						{if isset($acl)}
						  {foreach from=$acl.axo_groups item=group key=groupid}
			  				{if $id eq $group} selected {/if}
						  {/foreach}
						{/if}
						>{$cgroup}</option>
					  {/foreach}
					  </select>
					</td>
					<td>
					  <select name="clients[]" size="18" multiple="multiple">
					  {section loop=$clients name=clients}
						<option value="{$clients[clients].hostname}"
						{if isset($acl)}
						  {foreach from=$acl.axo.clients item=client key=key2}
			  				{if $clients[clients].hostname eq $client} selected {/if}
						  {/foreach}
						{/if}
						>{$clients[clients].hostname}</option>
					  {/section}
					  </select>
					</td>
				{/if}
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
  <tr>
    <td align="center"><input type="radio" name="allow" value="1" 
	{if isset($acl)}
		{if $acl.allow} checked {/if}
	{else}
	  checked
	{/if}
	>Allow 
	<input type="radio" name="allow" value="0"
	{if isset($acl)}
		{if (!$acl.allow)} checked {/if}
	{/if}
	>Deny</td>
	<td align="center"><input type="checkbox" name="active" value="1" 
	{if isset($acl)}
		{if $acl.enabled} checked {/if}
	{else}
	  checked
	{/if}
	>Active (vs. hidden)</td>
    <td align="right" valign="bottom">
	  <label for="note">Note:</label><input type="text" name="note"
	  {if isset($acl)}
		value="{$acl.note}"
	  {/if}
	  > 
	  {if isset($acl)}
	  <input type="hidden" name="editing" value="{$acl.acl_id}">
	  <input type="submit" name="editperm_submit" value="Update Permission">
	  {else}
      <input type="submit" name="addperm_submit" value="Assign Permission">
	  {/if}
    </td>
  </tr>
</table>
{/if}
</form>
{include file="footer.tpl"}
