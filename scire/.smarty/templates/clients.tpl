{include file="header.tpl" title="Clients"}
<form action="" method="post" name="clientsform">
<p>
  <strong>Search filter: 
    <input type="text" name="searchfield">
  </strong>
  {if check_access("View Pending Clients")}
  <strong>Status Filter:
    <select name="statusfilter" size="1">
	  {html_options values=$statusfilters output=$statusfilters selected=$View}
	</select>
	<input type="submit" name="submit" value="Update">
  </strong>
  {/if}
</p>
{if $clients}
  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle">Clients</td></tr>
  <tr><td>
  
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
  {foreach from=$fields item=i key=k}
    <th>
	  <a href="clients.php{if $f_attr[$i].sortable}?sorton={$i}&sortdir={if $sorton eq $i and $sortdir eq "desc"}asc{else}desc{/if}{/if}">{$f_attr[$i].name}</a> {if $f_attr[$i].sortable}<img src="images/sort_{if $sorton eq $i and $sortdir eq "desc"}desc{else}asc{/if}.png">{/if}
	</th>
  {/foreach}

	<th>Actions</th>
	{if $View eq "Pending"}
	<th>Approve/Deny</th>
	{/if}
  </tr>
  {section name=clients loop=$clients}
  <tr>
    {foreach from=$fields item=item key=key}
	  {if $item eq "hostname"}
	  <td><a href="client.php?clientid={$clients[clients].clientid}&Action=edit">{$clients[clients].hostname}</a></td>
	  {else}
	  <td>{$clients[clients].$item}</td>
	  {/if}
	{/foreach}
    <td>
	  {if check_access("Run Job") }
	  <a href="job.php?clientid={$clients[clients].clientid}&Action=create">Create Job</a>
	  {/if}
	  {if check_access("Edit Client") }
	  <a href="client.php?clientid={$clients[clients].clientid}&Action=edit">Edit</a>
	  {/if}
	  {if ($View eq "Pending") and check_access("Approve/Reject Pending Clients")}
	    <a href="client.php?clientid={$clients[clients].clientid}&Action=assign">Assign</a>
	  {else}
	    <a href="client.php?clientid={$clients[clients].clientid}&Action=delete">Delete</a>
	  {/if}
	</td>
	{if $View eq "Pending" and check_access("Approve/Reject Pending Clients")}
	<td>
	  <input type="checkbox" name="pending_ids[]" value="{$clients[clients].clientid}">
	</td>
	{/if}
  </tr>
  {/section}

  </td></tr>
  
  </table>
  {if $View eq "Pending" and check_access("Approve/Reject Pending Clients")}
  <tr>
    <td align="right"><input type="submit" name="pend_approve" value="Approve">
	<input type="submit" name="pend_reject" value="Reject"></td>
  </tr>
  {/if}
{/if}
</form>
{include file="footer.tpl"}
