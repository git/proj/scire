{include file="header.tpl" title="Users"}
<form action="" method="post" name="usersgroupsform">
{if $Action eq "create"}
    <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr><td class="boxtitle">Create a User Group</td></tr>
      <tr><td>
	
	<table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
	  <td class="boxtitle">Add User Group</td><td class="boxtitle">Parent Group</td>
	</tr>
	<tr>
	  <td>
	    <label for="addgroupname">Name:</label>
	    <input type="text" name="addgroupname" id="addgroupname">
	  </td>
	  <td>
	    <select name="parentgroup" id="parentgroup">
		{foreach item=cgroup key=ckey from=$groups}
		  <option value="{$ckey}">{$cgroup}</option>
		{/foreach}
		</select>
	  </td>
	</tr>
	<tr><td>as a subgroup of  ---></td></tr>
	<tr><td></td><td><input type="submit" name="addgroup" value="Add User Group"></td></tr>
	</table>
    </table>
  {elseif $Action eq "edit"}
  <input type="hidden" name="groupid" value="{$groupid}">
  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr><td class="boxtitle">Edit User Group {$groupdata[3]}</td></tr>
      <tr><td>
	
	<table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr>
	  <td class="boxtitle">Edit User Group</td><td class="boxtitle">Parent Group</td>
	</tr>
	<tr>
	  <td>
	    <label for="editgroupname">Name:</label>
	    <input type="text" name="editgroupname" id="editgroupname" value="{$groupdata[3]}">
	  </td>
	  <td>
	    <select name="parentgroup" id="parentgroup">
		{foreach item=cgroup key=ckey from=$groups}
		  <option value="{$ckey}" {if $groupdata[1] == $ckey} selected {/if}>{$cgroup}</option>
		{/foreach}
		</select>
	  </td>
	</tr>
	<tr><td>as a subgroup of  ---></td></tr>
	<tr><td></td><td><input type="submit" name="editgroup" value="Save Changes"></td></tr>
	</table>
    </table>
  {elseif $Action eq "delete"}
    <h1>Delete</h1>
    <b>Are you absolutely SURE you want to delete this group?</b>
	<input type="hidden" name="delgroup" value="{$groupid}">
<input type="submit" name="delgroup_confirm" value="Yes, remove the group"><input type="submit" name="delete_cancel" value="No wait! Cancel!">
  {else}
    <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
      <tr><td class="boxtitle">User Groups</td></tr>
      <tr><td>

    <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
	<tr><td class="boxtitle">Group Name</td><td class="boxtitle">Edit</td><td class="boxtitle">Delete</td></tr>
    {foreach item=group key=key from=$groups}
    <tr><td>{$group}</td><td><a href="users.php?View=groups&Action=edit&groupid={$key}">Edit</a></td><td><a href="users.php?View=groups&Action=delete&groupid={$key}">Delete</a></td></tr>
    {/foreach}
  {/if} <!-- action -->

</form>
{include file="footer.tpl"}
