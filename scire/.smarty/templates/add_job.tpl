{include file="header.tpl" title="Add A Job"}
<form action="" method="post" name="add_jobform">
	Step 1: select a script.<br>
	Step 2: select a client or multiple clients and/or clientgroups.<br>
	Step 3: set 
<hr>
 <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td align="left" valign="top">
	  <table class="box" width="90%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Script Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="scripts">Scripts:</label></td>
			  </tr>
			  <tr>
				  {if isset($get.scriptid)}
				    
				    <td>Selected script:</td>
					<td><input type="hidden" name="script" value="{$script.scriptid}">{$script.name}</td>
				  {else}
				    <td>Categories will go here</td>
				    <td>
					  <select name="script" size="10" >
					  {foreach from=$scripts item=script key=key}
						<option value="{$script.scriptid}">
						{$script.name}</option>
					  {/foreach}
					  </select>
					</td>
				  {/if}
			  </tr>
			  {if !isset($get.scriptid)}
			  <tr>
			  <td colspan="2">Or just type in the ID of the script: <input type="text" name="scriptID" ></td>
			  </tr>
			  {/if}
			  <tr>
			  <td colspan="2"><hr><h4>Override Script default values:</h4><br>
			  Permission: <select size="1" name="permission">
			  		{foreach from=$perms item=perm key=id}
					<option value="{$perm}"
					{if isset($script.permission)}
					    {if $perm eq $script.permission} selected {/if}
					{/if}
					>{$perm}</option>
				    {/foreach}
			  			  </select><br>
			  
			  Set Priority: <select size="1" name="priority">
			  		{foreach from=$priorities item=priority key=id}
					<option value="{$priority}"
					{if isset($script.priority)}
					    {if $priority eq $script.priority} selected {/if}
					{/if}
					>{$priority}</option>
				    {/foreach}
			  			  </select><br>
			  Log Location: <input type="text" name="log_location" value="{$script.log_location}"><br>
			  Success Code: (not worrying bout this now)<br>
			  Set dynamic content: <br>
			  </td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
	
	<td align="right" valign="top">
	  <table class="box" width="50%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Client Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
			    {if isset($get.clientid)}
				    
				    <td>Selected client:</td>
					<td><input type="hidden" name="clients[]" value="{$client.hostname}">{$client.hostname}</td>
				{elseif isset($get.cgroupid)}
					<td>Selected clientgroup:</td>
					<td><input type="hidden" name="cgroups[]" value="{$get.cgroupid}">NAME HERE</td>
				{else}
				  <td><label for="clientgroups">Clientgroups:</label></td>
				  <td><label for="clients">Clients:</label></td>
			  </tr>
			  <tr>
					<td>
					  <select name="clientgroups[]" size="18" multiple="multiple">
					  {foreach from=$cgroups item=cgroup key=id}
						<option value="{$id}">{$cgroup}</option>
					  {/foreach}
					  </select>
					</td>
					<td>
					  <select name="clients[]" size="18" multiple="multiple">
					  {section loop=$clients name=clients}
						<option value="{$clients[clients].clientid}">{$clients[clients].hostname}</option>
					  {/section}
					  </select>
					</td>
				{/if}
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	  Input staging client hostname (optional): <input type="text" name="staging"><br>
	  Staging success: <input type="radio" name="staging_success" value="result">Positive Result Code <input type="radio" name="staging_success" value="user">Manually approved
	</td>
  </tr>
  </table>
  <hr>
  <table>
  {if $dyn_tags}
  <tr>
    <th>Dynamic Tag</th><th>Value</th>
  </tr>
  {foreach from=$dyn_tags item=tag key=tagnum}
  <tr>
    <td>Tag: <b>{$tag}</b></td><td><input type="text" name="tag_{$tag}" value="{$tag_values[$tag]}" /></td>
  </tr>
  {/foreach}
  {/if}
</table>
<hr>
Job Description: <br><textarea name="description" rows="3" cols="40"></textarea><br>
<hr>
Recurring stuff here. <b>You need to select all of them to get it to save the schedule!</b><br>
<table>
<tr><td>Minute(s):<br>
<select multiple name=minute1 size=10>
<!-- <option value=*> Every Minute
<option value=*/2> Every Other Minute -->
<option value=*/5> Every Five Minutes
<option value=*/10> Every Ten Minutes
<option value=*/15> Every Fifteen Minutes
<option value=0> 0
<option value=1> 1

<option value=2> 2
<option value=3> 3
<option value=4> 4
<option value=5> 5
<option value=6> 6
<option value=7> 7
<option value=8> 8
<option value=9> 9
<option value=10> 10

<option value=11> 11
<option value=12> 12
<option value=13> 13
<option value=14> 14
<option value=15> 15
<option value=16> 16
<option value=17> 17
<option value=18> 18
<option value=19> 19

<option value=20> 20
<option value=21> 21
<option value=22> 22
<option value=23> 23
<option value=24> 24
<option value=25> 25
<option value=26> 26
<option value=27> 27
<option value=28> 28

<option value=29> 29
<option value=30> 30
<option value=31> 31
<option value=32> 32
<option value=33> 33
<option value=34> 34
<option value=35> 35
<option value=36> 36
<option value=37> 37

<option value=38> 38
<option value=39> 39
<option value=40> 40
<option value=41> 41
<option value=42> 42
<option value=43> 43
<option value=44> 44
<option value=45> 45
<option value=46> 46

<option value=47> 47
<option value=48> 48
<option value=49> 49
<option value=50> 50
<option value=51> 51
<option value=52> 52
<option value=53> 53
<option value=54> 54
<option value=55> 55

<option value=56> 56
<option value=57> 57
<option value=58> 58
<option value=59> 59
</select><br>
</td>
<td>Hour(s):<br>
<select multiple name=hour1 size=5>
<option value=*> Every Hour
<option value=*/2> Every Other Hour

<option value=*/4> Every Four Hours
<option value=*/6> Every Six Hours
<option value=0> 0 = 12 AM/Midnight
<option value=1> 1 = 1 AM
<option value=2> 2 = 2 AM
<option value=3> 3 = 3 AM
<option value=4> 4 = 4 AM
<option value=5> 5 = 5 AM
<option value=6> 6 = 6 AM

<option value=7> 7 = 7 AM
<option value=8> 8 = 8 AM
<option value=9> 9 = 9 AM
<option value=10> 10 = 10 AM
<option value=11> 11 = 11 AM
<option value=12> 12 = 12 PM/Noon
<option value=13> 13 = 1 PM
<option value=14> 14 = 2 PM
<option value=15> 15 = 3 PM

<option value=16> 16 = 4 PM
<option value=17> 17 = 5 PM
<option value=18> 18 = 6 PM
<option value=19> 19 = 7 PM
<option value=20> 20 = 8 PM
<option value=21> 21 = 9 PM
<option value=22> 22 = 10 PM
<option value=23> 23 = 11 PM
</select>

<br><br>Day(s):<br>
<select multiple name=day1 size=5>
<option value=*> Every Day
<option value=1> 1
<option value=2> 2
<option value=3> 3
<option value=4> 4
<option value=5> 5
<option value=6> 6

<option value=7> 7
<option value=8> 8
<option value=9> 9
<option value=10> 10
<option value=11> 11
<option value=12> 12
<option value=13> 13
<option value=14> 14
<option value=15> 15

<option value=16> 16
<option value=17> 17
<option value=18> 18
<option value=19> 19
<option value=20> 20
<option value=21> 21
<option value=22> 22
<option value=23> 23
<option value=24> 24

<option value=25> 25
<option value=26> 26
<option value=27> 27
<option value=28> 28
<option value=29> 29
<option value=30> 30
<option value=31> 31
</select><br><br>
</td><td>Months(s):<br>

<select multiple name=month1 size=5>
<option value=*> Every Month
<option value=1> January
<option value=2> February
<option value=3> March
<option value=4> April
<option value=5> May
<option value=6> June
<option value=7> July

<option value=8> August
<option value=9> September
<option value=10> October
<option value=11> November
<option value=12> December
</select>
<br><br>Weekday(s):<br>
<select multiple name=weekday1 size=5>
<option value=*> Every Weekday
<option value=0> Sunday

<option value=1> Monday
<option value=2> Tuesday
<option value=3> Wednesday
<option value=4> Thursday
<option value=5> Friday
<option value=6> Saturday
</select>
</td></tr>
<tr><td>
<hr>
<label for="validity_period">Validity Period (in minutes?): </label><input type="text" name="validity_period" />
</td></tr>

<tr><td>
<script language="JavaScript" src="calendar/calendar3.js"></script><!-- Date only with year scrolling -->
<label for="deploy_time">Start time: <input type="Text" name="deploy_time" value="">
				<a href="javascript:cal1.popup();"><img src="calendar/img/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></label>
</td>
<td>
<label for="expiration_time">End time: <input type="Text" name="expiration_time" value="">
				<a href="javascript:cal2.popup();"><img src="calendar/img/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></label>
<script language="JavaScript">
			<!-- // create calendar object(s) just after form tag closed
				 // specify form element as the only parameter (document.forms['formname'].elements['inputname']);
				 // note: you can have as many calendar objects as you need for your application
				 var cal1 = new calendar3(document.forms['add_jobform'].elements['deploy_time']);
				cal1.year_scroll = false;
				cal1.time_comp = true;
				var cal2 = new calendar3(document.forms['add_jobform'].elements['expiration_time']);
				cal2.year_scroll = false;
				cal2.time_comp = true;
			//-->
			</script>
</td>
</tr>
</table>
<hr>
Job dependency stuff here.<B>CODE ME</B><br>
<!-- Set notification email address: <input type="radio" name="notify_addr" value="{$useremail}">{$useremail}</input><input type="radio" name="notify_addr" value="new"><input type="text" name="notify_new"></input> -->
<input type="submit" name="ADD" value="ADD JOB!!!">
</form>
{include file="footer.tpl"}
