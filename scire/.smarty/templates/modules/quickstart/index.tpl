{include file="header.tpl" title="Quickstart Server"}
<h3>Quickstart Server Status Overview</h3>
<form name="form1" method="post" action="#">
List of clients currently installing:<br>
<table class="table" border="2">
	<tr>
		<th>MAC</th>
		<th>IP</th>
		<th>PROFILE</th>
		<th>STATUS</th>
	</tr>
{foreach from=$clients item=client key=key}
	{if $client.status eq 'Installing'}
		<tr>
			<td>{$client.mac}</td>
			<td>{$client.ip}</td>
			<td>{$client.profile}</td>
			<td>{$client.status}</td>
		</tr>
	
	{/if}
{/foreach}
</table>
List of clients pending installation:<br>
<table class="table">
	<tr>
		<th>MAC</th>
		<th>IP</th>
		<th>PROFILE</th>
		<th>STATUS</th>
	</tr>
{foreach from=$clients item=client key=key}
	{if $client.status eq 'Scheduled for Install'}
		<tr>
			<td>{$client.mac}</td>
			<td>{$client.ip}</td>
			<td>{$client.profile}</td>
			<td>{$client.status}</td>
		</tr>
	
	{/if}
{/foreach}
</table>
List of completed installations:<br>
<table class="table">
	<tr>
		<th>MAC</th>
		<th>IP</th>
		<th>PROFILE</th>
		<th>STATUS</th>
	</tr>
{foreach from=$clients item=client key=key}
	{if $client.status eq 'Install Complete'}
		<tr>
			<td>{$client.mac}</td>
			<td>{$client.ip}</td>
			<td>{$client.profile}</td>
			<td>{$client.status}</td>
		</tr>
	
	{/if}
{/foreach}
</table>
List of available profiles:<br>
<table class="table">
	<tr>
		<th>Profile ID</th>
		<th>Profile Name</th>
		<th>Filename/URI</th>
		<th>Description</th>
	</tr>
{foreach from=$profiles item=profile key=key}
	<tr>
		<td>{$profile.profileid}</td>
		<td>{$profile.profile_name}</td>
		<td>{$profile.location}</td>
		<td>{$profile.description}</td>
	</tr>

{/foreach}
</table>


<input type="submit" name="Submit" value="Submit">
</form>
{include file="footer.tpl"}
