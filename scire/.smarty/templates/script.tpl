{include file="header.tpl" title="Script"}
<form action="" method="post" name="scriptform">
{if $Action eq "create"}
  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle">Create a Script</td></tr>
  <tr><td>
  
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
	<td><label for="addname">*Script Name:</label></td>
	<td><input type="text" id="addname" name="addname" value="{$addname}" /></td>
  </tr><tr>
	<td><label for="desc">*Description:</label></td>
	<td><input type="text" id="desc" name="desc" size="50" value="{$desc}" /></td>
	
  </tr>
  <tr>
	<td><label for="runas">System user to run as (can be overwritten by job):</label></td>
	<td colspan="3">Manual: <input type="text" id="runas" name="runas" value="{$runas}" /></td>
  </tr>
  <tr>
  	<td><input type="radio" name="runas_radio" value="root">root
	    <input type="radio" name="runas_radio" value="nobody">nobody</td>
	<td><input type="radio" name="runas_radio" value="httpd">httpd
	    <input type="radio" name="runas_radio" value="apache">apache</td>
  </tr>
  <tr>
	<td><label for="priority">Recommended Priority:</label></td>
	<td><select name="priority" size="1">
	{foreach from=$priorities item=pri key=key}
	<option value="{$pri}" {if $pri eq $priority}selected{/if}>{$pri}</option>
	{/foreach}
	</select>
	</td> 
	<td><label for="permission">Recommended Permission:</label></td>
	<td>
	  <select name="permission" size="7">
	  {foreach from=$perms item=perm key=id}
		<option value="{$perm.id}" {if $perm.id eq $permission}selected{/if}>{$perm.name}</option>
	  {/foreach}
	  </select>
	</td>
  </tr>
  <tr>
  <td><hr></td>
  </tr>
  <tr>
    <td>*Actual Script to run on client: </td>
    <td><input type="radio" name="script_data_type" value="load" disabled>Load from file <input type="upload" name="script_data_upload"></td>
  </tr>
  <tr>
    <td></td>
	<td><input type="radio" name="script_data_type" value="manual" checked>Input manually below</td>
  </tr>
  <tr>
    <td colspan="2"><textarea name="script_data_manual" rows="10" cols="80" >
{$script_data}
	</textarea> </td>
  </tr>
  
    <tr>
    <td><input type="submit" name="continue" value="Find Dynamic Tags">this button will take input above and parse and look for dynamic tags.  it will then generate the table below
	</td>
  </tr>

  </table>
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
  {if $dyn_tags}
  <tr>
    <th>Dynamic Tag</th><th>Default Value</th>
  </tr>
  {foreach from=$dyn_tags item=tag key=tagnum}
  <tr>
    <td>Tag: <b>{$tag}</b></td><td><input type="text" name="tag_{$tag}" value="" /></td>
  </tr>
  {/foreach}
    <tr>
  <td><hr></td>
  </tr>
  {/if}
  <tr>
    <td>Success return code (default 0): <input type="text" name="success_code" value="0">
	</td>
  </tr>
  <tr>
    <td>Post Processing Script:</td><td><input type="radio" name="pps_type" value="none" checked>No post-processing needed, this script just returns.</td>
  </tr>
  <tr>
    <td></td><td><input type="radio" name="pps_type" value="load" disabled >Load from file: <input type="upload" name="pps_load"></td>
  </tr>
  <tr>
    <td></td><td><input type="radio" name="pps_type" value="manual" >Input Manually below</td>
  </tr>
  <tr>
    <td colspan="2"><textarea name="pps_data" rows="6" cols="80"></textarea></td>
  </tr>
   
  </table>
  </td></tr>
  <input type="submit" name="add_script" value="Add Script">
{/if}


 

</form>
{include file="footer.tpl"}
