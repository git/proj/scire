{include file="header.tpl" title="Access Denied"}
<p>
An error has occurred.<br>
Unfortunatly, {$user} does not have sufficient permission to perform this action.<br>
The requried permission was: {$perm}<br>
Why don't you complain to your admin?
{$message|escape:"htmlall"}
</p>
{include file="footer.tpl"}
