{include file="header.tpl" title="Users"}
<form action="" method="post" name="usersform">
<p><strong>Search filter: 
    <input type="text" name="searchfield">
</strong></p>
{if $users}
  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle">Users</td></tr>
  <tr><td>
  
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
  <tr>
  {foreach from=$fields item=i key=k}
    <th>
	  <a href="users.php{if $f_attr[$i].sortable}?sorton={$i}&sort={if $sorton eq $i and $sortdir eq "desc"}asc{else}desc{/if}{/if}">{$f_attr[$i].name}</a> {if $f_attr[$i].sortable}<img src="images/sort_{if $sorton eq $i and $sortdir eq "desc"}desc{else}asc{/if}.png">{/if}
	</th>
  {/foreach}
  <th>Actions</th>
  </tr>
  {section name=users loop=$users}
  <tr>
    {foreach from=$fields item=item key=key}
	  {if $item eq "username"}
		<td><a href="user.php?userid={$users[users].userid}&Action=edit">{$users[users].username}</a></td>
	  {else}
	  <td>{$users[users].$item}</td>
	  {/if}
	{/foreach}
	<td><a href="user.php?userid={$users[users].userid}&Action=edit">Edit</a>  <a href="user.php?userid={$users[users].userid}&Action=delete">Delete</a></td>
  </tr>
  {/section}
  
  </td></tr>
  </table>
{/if}
</form>
{include file="footer.tpl"}
