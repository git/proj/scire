{include file="header.tpl" title="Jobs"}
{if $Action eq "create"}
<h1>Add a Job</h1>
<form action="" method="post" name="add_jobform">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td align="left" valign="top">
	  <table class="box" width="90%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Script Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="scripts">Scripts:</label></td>
			  </tr>
			  <tr>
				  {if isset($get.scriptid)}
				    
				    <td>Selected script:</td>
					<td><input type="hidden" name="script" value="{$script.scriptid}">{$script.name}</td>
				  {else}
				    <td>Categories will go here</td>
				    <td>
					  <select name="script" size="1" >
					  {foreach from=$scripts item=script key=key}
						<option value="{$script.scriptid}">
						{$script.name}</option>
					  {/foreach}
					  </select>
					</td>
				  {/if}
			  </tr>
			  {if !isset($get.scriptid)}
			  <tr>
			  <td colspan="2">Or just type in the ID of the script: <input type="text" name="scriptID" ></td>
			  </tr>
			  {/if}
			  <tr>
			  <td colspan="2"><hr><h4>Override Script default values:</h4><br>
			  Permission: <select size="1" name="permission">
			  		{foreach from=$perms item=perm key=id}
					<option value="{$perm}"
					{if isset($script.permission)}
					    {if $perm eq $script.permission} selected {/if}
					{/if}
					>{$perm}</option>
				    {/foreach}
			  			  </select><br>
			  
			  Set Priority: <select size="1" name="priority">
			  		{foreach from=$priorities item=priority key=id}
					<option value="{$priority}"
					{if isset($script.priority)}
					    {if $priority eq $script.priority} selected {/if}
					{/if}
					>{$priority}</option>
				    {/foreach}
			  			  </select><br>
			  Log Location: <input type="text" name="log_location" value="{$script.log_location}"><br>
			  Success Code: (not worrying bout this now)<br>
			  Set dynamic content: <br>
			  </td>
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
	
	<td align="right" valign="top">
	  <table class="box" width="50%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Client Selection</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
			    {if isset($get.clientid)}
				    
				    <td>Selected client:</td>
					<td><input type="hidden" name="clients[]" value="{$client.hostname}">{$client.hostname}</td>
				{elseif isset($get.cgroupid)}
					<td>Selected clientgroup:</td>
					<td><input type="hidden" name="cgroups[]" value="{$get.cgroupid}">NAME HERE</td>
				{else}
				  <td><label for="clientgroups">Clientgroups:</label></td>
				  <td><label for="clients">Clients:</label></td>
			  </tr>
			  <tr>
					<td>
					  <select name="clientgroups[]" size="18" multiple="multiple">
					  {foreach from=$cgroups item=cgroup key=id}
						<option value="{$id}">{$cgroup}</option>
					  {/foreach}
					  </select>
					</td>
					<td>
					  <select name="clients[]" size="18" multiple="multiple">
					  {section loop=$clients name=clients}
						<option value="{$clients[clients].clientid}">{$clients[clients].hostname}</option>
					  {/section}
					  </select>
					</td>
				{/if}
			  </tr>
			</table>
		  </td>
		</tr>
	  </table>
	  Input staging client hostname (optional): <input type="text" name="staging"><br>
	  Staging success: <input type="radio" name="staging_success" value="result">Positive Result Code <input type="radio" name="staging_success" value="user">Manually approved
	</td>
  </tr>
  </table>
  <hr>
  <table>
  {if $dyn_tags}
  <tr>
    <th>Dynamic Tag</th><th>Value</th>
  </tr>
  {foreach from=$dyn_tags item=tag key=tagnum}
  <tr>
    <td>Tag: <b>{$tag}</b></td><td><input type="text" name="tag_{$tag}" value="{$tag_values[$tag]}" /></td>
  </tr>
  {/foreach}
  {/if}
</table>
<hr>
Job Description: <br><textarea name="description" rows="3" cols="40"></textarea><br>
<table><tr>
			    <td><label for="runs_chedule">Run schedule:</label></td>
			    <td><input type="text" id="run_schedule" name="run_schedule" size="25" value="{$job.run_schedule}" /></td>
			    <td><label for="validity_period">Validity Period:<br>(in minutes)</label></td>
			    <td><input type="text" id="validity_period" name="validity_period" size="25" value="{$job.validity_period}" /></td>
</tr></table>
			<fieldset>
			  <legend>Run Schedule</legend>
			      <table>
				<tr>
				  <td>
				    <p>Minute(s):</p>
				    {html_options name=minutes options=$minutes selected=$selectedMinute size=6}				    
				  </td>
				  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				  <td>
				    <p>Hour(s):</p>
				    {html_options name=hours options=$hours selected=$selectedHour size=6}
				  </td>
				  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				  <td>
				    <p>Month(s):</p>
				    {html_options name=months options=$months selected=$selectedMonth size=6}
				  </td>
				</tr>

				<tr>
				  <td>
				    <p>Day(s):</p>
				    {html_options name=days options=$days selected=$selectedDay size=6}
				  </td>
				  <td></td>
				  <td></td>
				  <td></td>
				  <td>
				    <p>Weekday(s):</p>
				    {html_options name=weekdays options=$weekdays selected=$selectedWeekday size=6}
				  </td>
				</tr>
			      </table>

			</fieldset>
<table>
<tr><td>
<script language="JavaScript" src="calendar/calendar3.js"></script><!-- Date only with year scrolling -->
<label for="deploy_time">Start time: <input type="Text" name="deploy_time" value="">
				<a href="javascript:cal1.popup();"><img src="calendar/img/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></label>
</td>
<td>
<label for="expiration_time">End time: <input type="Text" name="expiration_time" value="">
				<a href="javascript:cal2.popup();"><img src="calendar/img/cal.gif" width="16" height="16" border="0" alt="Click Here to Pick up the date"></a></label>
<script language="JavaScript">
			<!-- // create calendar object(s) just after form tag closed
				 // specify form element as the only parameter (document.forms['formname'].elements['inputname']);
				 // note: you can have as many calendar objects as you need for your application
				 var cal1 = new calendar3(document.forms['add_jobform'].elements['deploy_time']);
				cal1.year_scroll = false;
				cal1.time_comp = true;
				var cal2 = new calendar3(document.forms['add_jobform'].elements['expiration_time']);
				cal2.year_scroll = false;
				cal2.time_comp = true;
			//-->
			</script>
</td>
</tr>
</table>
<hr>
Job dependency stuff here.<B>CODE ME</B><br>
<!-- Set notification email address: <input type="radio" name="notify_addr" value="{$useremail}">{$useremail}</input><input type="radio" name="notify_addr" value="new"><input type="text" name="notify_new"></input> -->
<input type="submit" name="ADD" value="ADD JOB!!!">
</form>
{elseif $Action eq "edit"}
<h1>Edit Job</h1>
<form action="" method="post" name="edit_jobform">
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td colspan="2" align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Job</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="addpriority">*Priority:</label></td>
				<td><input type="text" id="editpriority" name="editpriority" value="{$job.priority}" /></td>
				<td><label for="creator">Creator:</label></td>
				<td><input type="text" id="creator" disabled name="creator" value="{$job.creator}" /></td>
				
			  </tr>
			  <tr>
				<td><label for="permission:">Permission:</label></td>
				<td><input type="text" id="permission" name="permission" value="{$job.permission}" /></td>

			  </tr>
			  <tr>
				<td><label for="description">Description:</label></td>
				<td colspan="3"><textarea id="description" name="description" cols="55" rows="5" > {$job.description}</textarea></td>
				</tr>
				<tr>
				<td><label for="script">Script:</label></td>
				<td><select id="script" name="script" size="1">
				    {foreach item=sc from=$scripts }
				        {if $sc.scriptid eq $job.script}
					<option value="{$sc.scriptid}" selected="selected">{$sc.name}</option>
					{else}
					<option value="{$sc.scriptid}">{$sc.name}</option>
					{/if}
				    {/foreach}
				</select>
				</td>
			  </tr>
			  <tr>
			    <td><label for="runs_chedule">Run schedule:</label></td>
			    <td><input type="text" id="run_schedule" name="run_schedule" size="25" value="{$job.run_schedule}" /></td>
			    <td><label for="validity_period">Validity Period:<br>(in minutes)</label></td>
			    <td><input type="text" id="validity_period" name="validity_period" size="25" value="{$job.validity_period}" /></td>
			  </tr>
			</table>

			<fieldset>
			  <legend>Run Schedule</legend>
			      <table>
				<tr>
				  <td>
				    <p>Minute(s):</p>
				    {html_options name=minutes options=$minutes selected=$selectedMinute size=6}				    
				  </td>
				  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				  <td>
				    <p>Hour(s):</p>
				    {html_options name=hours options=$hours selected=$selectedHour size=6}
				  </td>
				  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
				  <td>
				    <p>Month(s):</p>
				    {html_options name=months options=$months selected=$selectedMonth size=6}
				  </td>
				</tr>

				<tr>
				  <td>
				    <p>Day(s):</p>
				    {html_options name=days options=$days selected=$selectedDay size=6}
				  </td>
				  <td></td>
				  <td></td>
				  <td></td>
				  <td>
				    <p>Weekday(s):</p>
				    {html_options name=weekdays options=$weekdays selected=$selectedWeekday size=6}
				  </td>
				</tr>
			      </table>

			</fieldset>


			<input type="hidden" name="jobid" value="{$job.jobid}">
			<p><input type="submit" name="EditJobSubmit" value="Save Changes" /></form></p>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
</table>
{elseif $Action eq "delete"}
<h1>DELETING</h1>
<b>Are you absolutely SURE you want to delete job {$jobid}:<br>Description:{$job.description}?</b>
<input type="hidden" name="jobid" value="{$jobid}">
<input type="submit" name="delete_confirm" value="Yes, get rid of it!"><input type="submit" name="delete_cancel" value="No wait!">
{/if}

{include file="footer.tpl"}
