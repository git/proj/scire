{include file="header.tpl" title="Script Library"}
<form action="" method="post" name="scriptsform">

  <table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle" colspan="6">Scripts</td></tr>
  <tr class="table">
    <th>Name</th><th>Description</th><th>Last Used</th><th>OS</th><th>Permissions</th><th>Actions</th>
  </tr>
  {foreach from=$scripts item=script key=key}
    <tr class="table">
	  <td>{$script.name}</td>
	  <td>{$script.description}</td>
	  <td>LAST USED HERE</td>
	  <td>ADD OS TO SCRIPT TABLE</td>
	  <td>{$script.permission}</td>
	  <td><a href="job.php?Action=create&scriptid={$script.scriptid}">Create Job</a>|<a href="script.php?Action=Edit&scriptid={$script.scriptid}">Edit</a>|<a href="script.php?Action=Delete&scriptid={$script.scriptid}">Delete</a><a href="script.php?Action=Copy&scriptid={$script.scriptid}">Copy as New</a></td>
	</tr>
  {/foreach}
  <tr class="table">
    <td>Install Package</td>
	<td>This will install a specified package to the client.</td>
	<td>Today</td>
	<td>Gentoo</td>
	<td>Install</td>
	<td><a href="add_job.php?scriptid=1">Create Job</a>|<a href="script.php?Action=Edit&scriptid=1">Edit</a>|<a href="script.php?Action=Delete&scriptid=1">Delete</a><a href="script.php?Action=Copy%scriptid=1">Copy as New</a></td>
	
  </tr>
  
  </table>

</form>
{include file="footer.tpl"}
