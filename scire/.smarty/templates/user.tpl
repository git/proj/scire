{include file="header.tpl" title="Users"}
<form action="" method="post" name="edit_userform">
{if $Action eq "addgroup"}
  <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr><td class="boxtitle">Add user to group</td></tr>
  <tr>
	<td colspan="2" align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
	  <tr>
		<td class="boxtitle" width="15">Add</td><td class="boxtitle">Group Name</td>
	  </tr>
	  {foreach from=$groups name=ugroup item=item key=id}
	  <tr>
	    <td><input type="checkbox" name="addgroupid[]" value="{$id}"
		{if isset($ingroups)} {if in_array($id, $ingroups)} checked {/if} {/if}
		></td>
	    <td>{$item}</td>
	  </tr>
      {/foreach}
	  <tr><td>
	    <input type="hidden" name="userid" value="{$userid}">
	    <input type="submit" name="addgroup_confirm" value="Save Changes">
	  </td></tr>
	  </table>
	</td>
  </tr>
  </table>
{elseif $Action eq "create"}
<table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td colspan="2" align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">User</td><td class="boxtitle">Groups</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				<td><label for="addusername">*Username:</label></td>
				<td><input type="text" id="addusername" name="addusername" /></td>
				<td><label for="passwd">*Password:</label></td>
				<td><input type="password" id="passwd" name="passwd" /></td>
				
			  </tr>
			  <tr>
				<td><label for="real_name">Real name:</label></td>
				<td><input type="text" id="real_name" name="real_name" /></td>
				<td><label for="passwd2">Confirm Password:</label></td>
				<td><input type="password" id="passwd2" name="passwd2" /></td>

			  </tr>
			  <tr>
				<td><label for="comment">Comment:</label></td>
				<td><input type="text" id="comment" name="comment" /></td>
				<td><label for="status">Status:</label></td>
				<td><select id="status" name="status" size="1">
					<option value="active" selected="selected">Active</option>
					<option value="disabled">Disabled</option>
				</select>
				</td>
			  </tr>
			  <tr>
			    <td><label for="email">*Email Address:</label></td>
				<td><input type="text" id="email" name="email" /></td>
				<td><label for="phone">Phone:</label></td>
				<td><input type="text" id="phone" name="phone" /></td>
			  </tr>
			  <tr>
			    <td><label for="pager">Pager:</label></td>
				<td><input type="text" id="pager" name="pager" /></td>
			  </tr>
			</table>
			<p><input type="submit" name="AddUserSubmit" value="Add User" /></p>
		  </td>
		  <td>
		  <table>
		    <tr><td>Assign user to the following groups:</td></tr>
		    <tr><td>
			<select name="groups[]" size="10" multiple="multiple" id="groups">
		  	{foreach from=$groups item=ugroup key=id}
			<option value="{$id}">{$ugroup}</option>
			{/foreach}
        	</select>
			</td>
		  </tr></table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
{elseif $Action eq "edit"}
 <table width="100%" border="0" cellspacing="0" cellpadding="5">
  <tr>
	<td colspan="2" align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">User</td>
		</tr>
		<tr>
		  <td>
			<table border="0" cellspacing="0" cellpadding="5">
			  <tr>
				{if check_access("Change User Pass/Name") }
				  <td><label for="editusername">*Username:</label></td>
				  <td><input type="text" id="editusername" name="editusername" value="{$user.username}"></td>
				{else}
				  <td><label for="editusername">*Username:</label></td>
				  <td><input type="hidden" id="editusername" name="editusername" value="{$user.username}">{$user.username}</td>
				{/if}
				<td><label for="passwd">*Password:</label></td>
				<td><input type="password" id="passwd" name="passwd" {if !check_access("Change User Pass/Name") }disabled {/if}/></td>
			  </tr>
			  <tr>
				<td><label for="real_name">Real name:</label></td>
				<td><input type="text" id="real_name" name="real_name" value="{$user.real_name}" /></td>
				<td><label for="passwd2">Confirm Password:</label></td>
				<td><input type="password" id="passwd2" name="passwd2" {if !check_access("Change User Pass/Name") }disabled {/if}/></td>
			  </tr>
			  <tr>
				<td><label for="comment">Comment:</label></td>
				<td><input type="text" id="comment" name="comment" value="{$user.comment}" /></td>
				<td><label for="status">Status:</label></td>
				<td>
				  <select id="status" name="status" size="1">
					<option value="active" selected="selected">Active</option>
					<option value="disabled">Disabled</option>
				  </select>
				</td>
			  </tr>
			  <tr>
			    <td><label for="email">*Email Address:</label></td>
				<td><input type="text" id="email" name="email" value="{$user.email}" /></td>
				<td><label for="phone">Phone:</label></td>
				<td><input type="text" id="phone" name="phone" value="{$user.phone}" /></td>
			  </tr>
			  <tr>
			    <td><label for="pager">Pager:</label></td>
				<td><input type="text" id="pager" name="pager" value="{$user.pager}" /></td>
			  </tr>
			</table><input type="hidden" name="userid" value="{$user.userid}">
			<p><input type="submit" name="EditUserSubmit" value="Update" /></p>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
  <tr>
	<td align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Groups</td>
		</tr>
		<tr>
		  <td>
			<table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
			  <tr>
				<th>Groupname</th>
				<th>Actions</th>
			  </tr>
			  {section name=group loop=$groups}
			  <tr>
				<td>{$groups[group].name}{if $groups[group].parent} (subgroup of {$groups[group].parent}){/if}</td>
				<td><input type="submit" name="rmug_{$groups[group].id}" value="Remove"></td>
			  </tr>
			  {/section}
			  <tr><td><a href="user.php?userid={$user.userid}&Action=addgroup">Add user to a group</a></td></tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
	<td align="center" valign="top">
	  <table class="box" width="100%" border="1" cellspacing="0" cellpadding="0">
		<tr>
		  <td class="boxtitle">Permissions</td>
		</tr>
		<tr>
		  <td>
			<table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
			  <tr>
				<th>Category -> Permission</th>
				<th>On Client(s)</th>
				<th>Actions</th>
			  </tr>
			  {foreach from=$acls item=acl key=k}
			  <tr>
				<td>{foreach from=$acl.aco item=perm key=category}{$category} -> {$perm[0]}{/foreach}</td>
				<td>
				  {if $acl.axo}  <!-- we have individual clients -->
	  			    {foreach from=$acl.axo.clients item=client key=key2}
			  		<font style="background-color:#A0F8FF">Client: {$client}</font><br>
					{/foreach}
		  		  {/if}
				  {foreach from=$acl.axo_groups item=group key=groupid}
			  		<font style="background-color:#FFFACD">ClientGroup: {$cgroups[$group]} </font><br>
				  {/foreach}
				</td>
				<td><form id="form1" name="form1" method="post" action=""><input type="hidden" name="perm_id" value="{$acl.acl_id}" /><input type="submit" name="submit" value="Remove" /></form></td>
			  </tr>
			  {/foreach}
			  <tr><td><a href="assign_permission.php?username={$user.username}">Assign a permission for this user</a></td></tr>
			</table>
		  </td>
		</tr>
	  </table>
	</td>
  </tr>
</table>
{elseif $Action eq "delete"}
<b>Are you absolutely SURE you want to delete user {$user.username}?</b>
<input type="hidden" name="userid" value="{$user.userid}">
<input type="submit" name="delete_confirm" value="Yes, get rid of the bastard!"><input type="submit" name="delete_cancel" value="No wait!">
{/if}
</form>
{include file="footer.tpl"}
