{assign var="baseurl" value=$baseurl|default:"/"}
{assign var="leftbar" value=$leftbar|default:"off"}
{assign var="title" value=$title|default:""}
{assign var="subtitle" value=$subtitle|default:""}
{assign var="desc" value=$desc|default:""}
{assign var="theme" value=$theme|default:"std_grey"}
<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=ISO-8859-1"/>
    <title>Scire{if ($title ne "")} - {$title}{if ($subtitle ne "")} - {$subtitle}{/if}{/if}</title>
    <link rel="icon" href="{$baseurl}favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="{$baseurl}favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="{$baseurl}{$theme}.css" type="text/css" />

{if isset($head)}{$head}{/if}
  </head>
  <body link="#000000" alink="#ffffff">
    <table width="100%" bgcolor="#ffffff" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td colspan="2">
          <table id="logobar" width="100%" border="0" cellspacing="0" cellpadding="10">
            <tr>
              <td width="80">
                <a href="{$baseurl}index.php"><img src="{$baseurl}images/scire_trans.png" alt="Scire" border="0" width="80" height="80" /></a>
              </td>
              <td align="right" valign="top">
                <form id="search" name="search" method="post" action="{$baseurl}search.php">
                  <label for="s">Search:</label>
                  <input type="text" id="s" name="s" size="16" />
                  <input type="submit" name="g" value="go" />
                </form>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td id="topbar" colspan="2" height="22">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td><font color="#ffffff">&nbsp;{section name=topbar_left loop=$topbar_left}<a href="{$baseurl}{$topbar_left[topbar_left].link}"><font color="#ffffff">{$topbar_left[topbar_left].name}</font></a>{if !$smarty.section.topbar_left.last} | {/if}{/section}</font></td>
              <td align="right"><font color="#ffffff">{if isset($username)}<a href="{$baseurl}login.php?logout=True"><font color="#ffffff">Logout [{$username}]</font></a> | {/if}{section name=topbar_right loop=$topbar_right}<a href="{$baseurl}{$topbar_right[topbar_right].link}"><font color="#ffffff">{$topbar_right[topbar_right].name}</font></a>{if !$smarty.section.topbar_right.last} | {/if}{/section}&nbsp;</font></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
{if $leftbar eq "on"}
        <td id="leftbar" valign="top" bgcolor="#a6afc2">
{if isset($leftbar_menu)}
          <ul id="menu">
{section name=leftbar_menu loop=$leftbar_menu}
            <li><a class="menuitem" href="{$baseurl}{$leftbar_menu[leftbar_menu].link}">{$leftbar_menu[leftbar_menu].name}</a></li>
{/section}
          </ul>
{/if}
{if isset($leftbar_submenu)}
          <p></p>
          <ul id="submenu">
{if isset($leftbar_submenu_title) && $leftbar_submenu_title ne ""}
            <li>{$leftbar_submenu_title}</li>
{/if}
{section name=leftbar_submenu loop=$leftbar_submenu}
            <li><a class="menuitem" href="{$baseurl}{$leftbar_submenu[leftbar_submenu].link}">{$leftbar_submenu[leftbar_submenu].name}</a></li>
{/section}
          </ul>
{/if}
        </td>
{/if}
        <td width="100%" valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td id="header" bgcolor="#d8d4e2">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
{if $title ne ""}                      <h1>{$title}{if $subtitle ne ""} - {$subtitle}{/if}</h1>{/if}
{if $desc ne ""}                      <h2>{$desc}</h2>{/if}
                    </td>
                    <td align="right" valign="top">
{if isset($status)}                      <p>{$status}</p>{/if}
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td id="content" height="100%" valign="top" bgcolor="#ffffff">
                <!-- begin content -->
