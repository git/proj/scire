{include file="header.tpl" title="Permissions"}
<form action="" method="post" name="userspermsform">
<table class="box" width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr><td class="boxtitle">Permissions</td></tr>
  <tr><td>
  
  <table class="table" width="100%" border="1" cellspacing="0" cellpadding="0">
    <tr>
      <th>{if $type eq "clients"}Client(s){else}User(s){/if}</th>
	  <th>Perm. Name</th>
	  <th>{if $type eq "clients"}from User(s){else}on Client(s){/if}</th>
	  <th width="7%">Allow/Deny</th>
	  <th>Active</th>
	  <th>Actions</th>
	</tr>
	{foreach from=$acls item=acl key=k} 
	<tr bgcolor="{cycle values="#eeeeee,#dddddd"}">
	  <td>
	    {if $type eq "clients"}
		  {if $acl.axo}  <!-- we have individual clients -->
	  		{foreach from=$acl.axo.clients item=client key=key2}
			  <font style="background-color:#A0F8FF">Client: {$client}</font><br>
			{/foreach}
		  {/if}
			{foreach from=$acl.axo_groups item=group key=groupid}
			  <font style="background-color:#FFFACD">ClientGroup: {$cgroups[$group]} </font><br>
			{/foreach}
		{else}	
			
	      {if $acl.aro}  <!-- we have individual users -->
			{foreach from=$acl.aro.users item=user key=key2}
			  <font style="background-color:#A0F8FF">User: {$user}</font><br>
			{/foreach}
		  {/if}
			{foreach from=$acl.aro_groups item=group key=groupid}
			  <font style="background-color:#FFFACD">UserGroup: {$ugroups[$group]} </font><br>
			{/foreach}
		{/if}
	  </td>
	  <td>
	  	{foreach from=$acl.aco item=ignore key=perm_category}
		  {$acl.aco.$perm_category[0]}
		{/foreach}
	  </td>
	  <td>
	    {if $type eq "clients"}
		  {if $acl.aro}  <!-- we have individual users -->
			{foreach from=$acl.aro.users item=user key=key2}
			  <font style="background-color:#A0F8FF">User: {$user}</font><br>
			{/foreach}
		  {/if}
			{foreach from=$acl.aro_groups item=group key=groupid}
			  <font style="background-color:#FFFACD">UserGroup: {$ugroups[$group]} </font><br>
			{/foreach}
		{else}
	      {if $acl.axo}  <!-- we have individual clients -->
	  		{foreach from=$acl.axo.clients item=client key=key2}
			  <font style="background-color:#A0F8FF">Client: {$client}</font><br>
			{/foreach}
		  {/if}
			{foreach from=$acl.axo_groups item=group key=groupid}
			  <font style="background-color:#FFFACD">ClientGroup: {$cgroups[$group]} </font><br>
			{/foreach}
		{/if}
	  </td>
	  {if $acl.allow}
	  	<td style="background-color: #008000">Allow</td>
	  {else}
	    <td style="background-color: #DC143C">Deny</td>
	  {/if}
	  <td>
	    {if $acl.enabled}Yes{else}No{/if}
	  </td>
	  <td>
	    <a href="assign_permission.php?Action=edit&aclid={$acl.acl_id}">Edit</a> 
		<a href="assign_permission.php?Action=delete&aclid={$acl.acl_id}">Delete</a>
	  </td>
	</tr>
	{/foreach}
	
  </table><hr>
  </tr>
  <tr>
  <table><tr>
  <td>
  	Blue = Single User/Client<br>
  	Tan = UserGroup/ClientGroup
  </td>
  <td align="right"><a href="assign_permission.php">Assign a Permission</a></td>
  </tr>
  </table>
  </tr>
</form>
{include file="footer.tpl"}
