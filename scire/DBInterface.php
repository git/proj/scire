<?php

function get_scire_modules() {
  global $db;
  $result = $db->select('*', 'modules');
  if ($result && count($result) > 0) {
    return $result;
  } else {
    return array();
  }
}

function get_scire_module_list() {
  global $db;
  $result = $db->select(array('main_content', 'name'), 'modules');
  if ($result && count($result) > 0) {
    return $result;
  } else {
    return array();
  }
}

function get_profile_list() {
  global $db;
  $result = $db->select('profile_name', 'profiles');
  if ($result && count($result) > 0) {
    return $result;
  } else {
    return array();
  }
}

function get_group_clients($group) {
  global $db;
  $result = $db->select('*', 'clients', '`profile` = \'' . $group . '\'');
  if ($result && count($result) > 0) {
    return $result;
  } else {
    return array();
  }
}

function get_all_clients() {
  global $db;
  $result = $db->select('*', 'clients');
  if ($result && count($result) > 0) {
    return $result;
  } else {
    return array();
  }
}

function get_scire_client($client_id) {
  global $db;
  $result = $db->select('*', 'clients', '`client_id` = \'' . $client_id . '\'');
  if ($result && count($result) > 0) {
    return $result[0];
  } else {
    return false;
  }
}

function scire_add_client($hostname, $mac, $ip, $profile, $distribution, $admin, $status) {
  global $db;
  $result = $db->insert('clients', array('hostname' => $hostname, 'mac' => $mac, 'ip' => $ip, 'profile' => $profile, 'distribution' => $distribution, 'administrator' => $admin, 'status' => $status));
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function scire_edit_client($client_id, $fields) {
  global $db;
  $result = $db->update('clients', $fields, '`client_id` = \'' . $client_id . '\'');
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function scire_del_client($client_id) {
  global $db;
  $result = $db->delete('clients', '`client_id` = \'' . $client_id . '\'');
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function get_scire_users() {
  #we don't want to store the password for security reasons. <- ???
  global $db;
  $result = $db->select(array('username', 'comment', 'real_name'), 'users');
  if ($result && count($result) > 0) {
    return $result;
  } else {
    return array();
  }
}

function get_scire_user($username) {
  global $db;
  $result = $db->select('*', 'users', '`username` = \'' . $username . '\'');
  if ($result && count($result) > 0) {
    return $result[0];
  } else {
    return false;
  }
}

function scire_add_user($username, $password, $real_name, $comment) {
  global $db;
  $result = $db->insert('users', array('username' => $username, 'password' => crypt($password, 447470567), 'comment' => $comment, 'real_name' => $real_name));
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function scire_del_user($username) {
  global $db;
  $result = $db->delete('users', '`username` = \'' . $username . '\'');
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function scire_add_permission($perm_name, $desc, $created_by) {
  global $db;
  $result = $db->insert('permissions', array('perm_name' => $perm_name, 'description' => $desc, 'creator' => $created_by));
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function scire_edit_permission($perm_name, $desc) {
  global $db;
  $result = $db->update('permissions', array('description' => $desc), '`perm_name` = \'' . $perm_name . '\'');
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function scire_del_permission($perm_name) {
  global $db;
  $result = $db->delete('permissions', '`perm_name` = \'' . $perm_name . '\'');
  if ($result) {
    return true;
  } else {
    return $db->error;
  }
}

function get_scire_permissions() {
  global $db;
  $result = $db->select('*', 'permissions');
  if ($result && count($result) > 0) {
    return $result;
  } else {
    return array();
  }
}

?>
