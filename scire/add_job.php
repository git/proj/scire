<?php
include('.lib/common.php');
include('cron2.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();

add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "jobs.php");

$status = "";

#####################################################
#Deal with submitted forms.
if ($_POST['ADD']) {
	if ($_POST['script']) {
		$script = get_scire_script($_POST['script']);
		if ($_POST['permission']) {
			$permission = $_POST['permission'];
			$permission = get_scire_permission_by_name($permission);
			$permission = $permission['permid'];
		#	print "Permission is: $permission";
		} else {
			$permission = $script['permission'];
		}
		if ($_POST['priority']) {
			$priority = $_POST['priority'];
		} else {
			$priority = $script['priority'];
		}
	} else { $status .= "ERROR: No script selected!"; }
	if ($_POST['description']) {
		$description = $_POST['description'];
	} else {
		$description = "No description provided.";
	}
	$pending = sizeof($_POST['clients']) + sizeof($_POST['clientgroups']);

	if (!$status and ($pending or $_POST['clientgroups'])) {  #We have a script and clients;
		# Get the schedule!  All fields are required or else you're SOL
		$scheduleComplete = $_POST["minute1"] and $_POST["hour1"] and
			$_POST["day1"] and $_POST["month1"] and $_POST["weekday1"];
		if ($scheduleComplete) {
			$str = implode(" ", array($_POST["minute1"], $_POST["hour1"],
				$_POST["day1"], $_POST["month1"], $_POST["weekday1"]));
		} else {
			$str = "";
		}
#		pre_var_dump($_POST);
#		pre_var_dump($scheduleComplete);
		pre_var_dump($str);

		try {
			$result = scire_add_job($_POST['script'], $priority, $_SESSION['userid'], $permission, $description, $pending, $_POST['clients'], $_POST['clientgroups'], $dependency, $str, $_POST['validity_period']);

			if (!$result) {
				$status .= "Job successfully added.";
			} else {
				$status .= "Error occurred during job addition. $result";
			}
		} catch (CronException $e) { print_r($e); }

	}
}


########################################################
if ($_GET['scriptid']) {
	$_GET['scriptid'] = (int) $_GET['scriptid'];
	$script = get_scire_script($_GET['scriptid']);
	$smarty->assign('script', $script);
#	print $script['script_data'];
	preg_match_all('/%%(.*?)%%/', $script['script_data'], $matches);
	$smarty->assign('dyn_tags', $matches[1]);
	foreach ($matches[1] as $tag) {
		$tag_values[$tag] = get_dyn_tag_value($_GET['scriptid'],$tag);
	}
	$smarty->assign('tag_values', $tag_values);
	
	pre_var_dump($tag_values);
}

if ($_GET['clientid']) {
	$_GET['clientid'] = (int) $_GET['clientid'];
	$client = get_scire_client($_GET['clientid']);
	#$id = $acl->get_object_id("clients",$client['hostname'],'AXO'
	
	$smarty->assign('client', $client);
}
########################################################

$permissions = array();
$all_permissions = $acl->get_objects(NULL, 1, 'ACO');
foreach ($all_permissions as $categoryname => $cat) {
	#print "Perm: $perm<br>Permname: $permname<br>";
	if ($categoryname != $scire_ui_section) {
		foreach ($cat as $perm) {
			array_push($permissions, $perm);
		}
	}
}
$smarty->assign('perms', $permissions);
#pre_var_dump($permissions);
$smarty->assign('priorities', get_priorities());


$scripts = get_scire_scripts();
$smarty->assign('scripts', $scripts);

$smarty->assign('cgroups', $acl->format_groups($acl->sort_groups('AXO'), TEXT));
$clients = get_scire_clients('hostname', 'asc');
$smarty->assign('clients', $clients);

$smarty->assign('get', $_GET);
$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display('add_job.tpl');
#var_dump($clients);
?>
