<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();
$leftbar_submenu = array();
$template = "users.tpl";
$status = "";

############################################
## Deal with submitted forms first.
if ($_POST['addgroupname']) {  #Adding a group.
	$newgroup_id = $acl->add_group($_POST['addgroupname'], $_POST['addgroupname'], $_POST['parentgroup'], 'ARO');
	$_GET['Action'] = "created";
	$_GET['View'] = "groups";
	$status = "User Group Created Successfully.";
}
if ($_POST['editgroupname']) {
	$group_id = $acl->edit_group($_POST['groupid'], $_POST['editgroupname'], $_POST['editgroupname'], $_POST['parentgroup'], 'ARO');
	$_GET['Action'] = "edited";
	$_GET['View'] = "groups";
	$status = "User Group Edited Successfully.";
}
if($_POST['delgroup_confirm']) {
	#Check if the group has children.
	$result = $acl->del_group($_POST['delgroup'],TRUE, 'ARO');
	if ($result) {
		$status = "Group deleted successfully.";
	} else {
		$status = "ERROR: Group could not be successfully deleted.";
	}
	$_GET['Action'] = "deleted";
	$_GET['View'] = "groups";
}





$smarty->assign('desc', "View, create, edit, and delete users");


switch($_GET['View']) {
	case "groups":
		check_action_access("View Users");
		$template = "users_groups.tpl";
		switch($_GET['Action']) {
			case "create":
				add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "users.php?View=groups");
				$smarty->assign('groups', $acl->format_groups($acl->sort_groups('aro'), TEXT));
				break;
			case "edit":
				add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "users.php?View=groups");
				$smarty->assign('groups', $acl->format_groups($acl->sort_groups('aro'), TEXT));
				$smarty->assign('groupid', $_GET['groupid']);
				$smarty->assign('groupdata', $acl->get_group_data($_GET['groupid'], 'ARO'));
				break;
			case "delete":
				add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "users.php?View=groups");
				$smarty->assign('groupid', $_GET['groupid']);
				break;
			default:  #Show usergroups.
				add_leftbar($leftbar_menu, "Users", "users.php");
				add_leftbar($leftbar_menu, "Permissions", "users.php?View=permissions");
				$smarty->assign('leftbar_submenu_title', "Actions");
				if ( check_access("Add UserGroup") ) {
					add_leftbar($leftbar_submenu, "Create User Group", "users.php?View=groups&Action=create");
				}	
		
				$groups = $acl->format_groups($acl->sort_groups('aro'), HTML);
				$smarty->assign('groups', $groups);
				
			break;
		}
		break;
	case "permissions":
		$smarty->assign('leftbar_submenu_title', "Actions");
		add_leftbar($leftbar_submenu, "Assign Permission", "assign_permission.php");
		$template = "permissions.tpl";
		$smarty->assign('type','users');
		$ugroups = $acl->format_groups($acl->sort_groups('aro'), 'ARRAY');
		$smarty->assign('ugroups', $ugroups);
		$cgroups = $acl->format_groups($acl->sort_groups('axo'), 'ARRAY');
		$smarty->assign('cgroups', $cgroups);

		#pre_var_dump($groups);
		
		$acls = array();
		#Format of search: aco_section, aco_value, aro_section, aro_value, aro_group_name, axo_section, axo_value, axo_group_name, return_value
		$result_acl_ids = $acl->search_acl(FALSE,FALSE, 
									FALSE,FALSE,FALSE,
									FALSE,FALSE,FALSE,FALSE);
		foreach ($result_acl_ids as $acl_id) {
			$this_acl = $acl->get_acl($acl_id);
			if ($this_acl['aco']['scire_ui']) {
			  next;
			} else {
				array_push($acls, $this_acl);
			}
		}
		#pre_var_dump($acls);
		$smarty->assign('acls',$acls);
		break;

	default:  #Show users.
		add_leftbar($leftbar_menu, "Groups", "users.php?View=groups");
		add_leftbar($leftbar_menu, "Permissions", "users.php?View=permissions");
		$smarty->assign('leftbar_submenu_title', "Actions");
		add_leftbar($leftbar_submenu, "Create User", "user.php?Action=create");
		
		############################################
		## Deal with sorting for the query.
		if (!$_GET['sorton']) {
			$_GET['sorton'] = $_SESSION['settings']['users_page_sorton'];
		}
		if (!$_GET['sortdir']) {
			$_GET['sortdir'] = $_SESSION['settings']['users_page_sortdir'];
		}
		$smarty->assign('sorton', $_GET['sorton']);
		$smarty->assign('sortdir', $_GET['sortdir']);
		
		$users = get_scire_users($_GET['sorton'], $_GET['sort']);
		foreach ($users as $key => $user) {
			#$id = $acl->get_object_id("users",$user['username'],'ARO');
			$groups = $acl->get_object_groups($user['userid'],'ARO');
			$group_string = "";
			if ($groups) {
				foreach ($groups as $group) {
					$group_data = $acl->get_group_data($group, 'ARO');
					$group_string .= $group_data[3]. " | ";
				}
			}
			$users[$key]['groups'] = $group_string;
		}
		
		#Set information about the fields (display name, sortable)
		$f_attr = get_users_fields();
		$smarty->assign('f_attr', $f_attr);
		
		#Grab the field list.
		if ($_SESSION['settings']['users_page_fields']) {
			$fields = explode(',', $_SESSION['settings']['users_page_fields']);
		} else {
			$fields = explode(',', get_default_users_fields());
		}
		$smarty->assign('fields',$fields);
		
		$smarty->assign('users', $users);
		break;
}

$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('leftbar_submenu', $leftbar_submenu);
$smarty->assign('View', $_GET['View']);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display($template);
#var_dump($_POST);
?>
