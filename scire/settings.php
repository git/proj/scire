<?php
include('.lib/common.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();
$leftbar_submenu = array();
$template = "settings.tpl";
$status = "";
$smarty->assign('desc', "Edit user settings.");

$themes = array();
array_push($themes, array('name' => 'Standard Grey', 'link' => 'std_grey'));
array_push($themes, array('name' => 'Light Blue', 'link' => 'light_blue'));
$smarty->assign('themes',$themes);

############################################
## Deal with submitted forms first.
if ($_POST['save_settings']) {
  #$settings = $_SESSION['settings'];
  foreach ($_POST as $settingname => $value) {
    if ($settingname != "save_settings") {  #SKIP
		if (isset($_SESSION['settings'][$settingname])) {
	    	if ($value != $_SESSION['settings'][$settingname]) {   #The value doesn't match, we need to update.
				#update the SESSION value.
				$_SESSION['settings'][$settingname] = $value;
				#writeback to the DB as well to keep in sync.
				scire_set_setting($_SESSION['userid'], $settingname, $value);
			}
		} else {  #New setting.  store it.
			$_SESSION['settings'][$settingname] = $value;
			#writeback to the DB as well to keep in sync.
			scire_add_setting($_SESSION['userid'], $settingname, $value);
		}
    }
  }
}
########################################  POST
if ($_POST['save_Jobs_settings']) {
	pre_var_dump($_POST);
	foreach ($_POST as $name => $value) {
		if ($name == "sorton") {
			if ($value != $_SESSION['settings']['jobs_page_sorton']) {   #The value doesn't match, we need to update.
				scire_set_setting($_SESSION['userid'], 'jobs_page_sorton', $value);
			} else {
				scire_add_setting($_SESSION['userid'], 'jobs_page_sorton', $value);
			}
			$_SESSION['settings']['jobs_page_sorton'] = $value;
		}
		elseif ($name == "sortdir") {
			if ($value != $_SESSION['settings']['jobs_page_sortdir']) {   #The value doesn't match, we need to update.
				scire_set_setting($_SESSION['userid'], 'jobs_page_sortdir', $value);
			} else {
				scire_add_setting($_SESSION['userid'], 'jobs_page_sortdir', $value);
			}
			$_SESSION['settings']['jobs_page_sortdir'] = $value;
		}
		elseif (substr($name,0, 4) == "Jobs" and $value) {
			$temp = substr($name,5);
			$jobs_page_fields[$temp] = $value;
			print "Adding field $name ... $value";
		}
	}
	#Done with looping
	pre_var_dump($jobs_page_fields);
	asort($jobs_page_fields);
	$keys = array_keys($jobs_page_fields);
	$fields_string = join(',', $keys);
	
	#print $fields_string;
	if ($fields_string != $_SESSION['settings']['jobs_page_fields']) {
		scire_set_setting($_SESSION['userid'], 'jobs_page_fields', $fields_string);
	} else {
		scire_add_setting($_SESSION['userid'], 'jobs_page_fields', $fields_string);
	}
	$_SESSION['settings']['jobs_page_fields'] = $fields_string;
}

########################################
if ($_POST['save_Clients_settings']) {
	foreach ($_POST as $name => $value) {
		if ($name == "sorton") {
			if ($value != $_SESSION['settings']['clients_page_sorton']) {   #The value doesn't match, we need to update.
				scire_set_setting($_SESSION['userid'], 'clients_page_sorton', $value);
			} else {
				scire_add_setting($_SESSION['userid'], 'clients_page_sorton', $value);
			}
			$_SESSION['settings']['clients_page_sorton'] = $value;
		}
		elseif ($name == "sortdir") {
			if ($value != $_SESSION['settings']['clients_page_sortdir']) {   #The value doesn't match, we need to update.
				scire_set_setting($_SESSION['userid'], 'clients_page_sortdir', $value);
			} else {
				scire_add_setting($_SESSION['userid'], 'clients_page_sortdir', $value);
			}
			$_SESSION['settings']['clients_page_sortdir'] = $value;
		}
		elseif (substr($name,0, 7) == "Clients" and $value) {
			$temp = substr($name,8);
			$clients_page_fields[$temp] = $value;
		}
	}
	#Done with looping
	asort($clients_page_fields);
	$keys = array_keys($clients_page_fields);
	$fields_string = join(',', $keys);
	
	#print $fields_string;
	if ($fields_string != $_SESSION['settings']['clients_page_fields']) {
		scire_set_setting($_SESSION['userid'], 'clients_page_fields', $fields_string);
	} else {
		scire_add_setting($_SESSION['userid'], 'clients_page_fields', $fields_string);
	}
	$_SESSION['settings']['clients_page_fields'] = $fields_string;
}
########################################
if ($_POST['save_Users_settings']) {
	foreach ($_POST as $name => $value) {
		if ($name == "sorton") {
			if ($value != $_SESSION['settings']['users_page_sorton']) {   #The value doesn't match, we need to update.
				scire_set_setting($_SESSION['userid'], 'users_page_sorton', $value);
			} else {
				scire_add_setting($_SESSION['userid'], 'users_page_sorton', $value);
			}
			$_SESSION['settings']['users_page_sorton'] = $value;
		}
		elseif ($name == "sortdir") {
			if ($value != $_SESSION['settings']['users_page_sortdir']) {   #The value doesn't match, we need to update.
				scire_set_setting($_SESSION['userid'], 'users_page_sortdir', $value);
			} else {
				scire_add_setting($_SESSION['userid'], 'users_page_sortdir', $value);
			}
			$_SESSION['settings']['users_page_sortdir'] = $value;
		}
		elseif (substr($name,0, 5) == "Users" and $value) {
			$temp = substr($name,6);
			$users_page_fields[$temp] = $value;
		}
	}
	#Done with looping
	asort($users_page_fields);
	$keys = array_keys($users_page_fields);
	$fields_string = join(',', $keys);
	
	#print $fields_string;
	if ($fields_string != $_SESSION['settings']['users_page_fields']) {
		scire_set_setting($_SESSION['userid'], 'users_page_fields', $fields_string);
	} else {
		scire_add_setting($_SESSION['userid'], 'users_page_fields', $fields_string);
	}
	$_SESSION['settings']['users_page_fields'] = $fields_string;
}



switch($_GET['View']) {
	case "Clients":
		$template = "settings_page.tpl";

		$settings = $_SESSION['settings'];
		$clients_setting_list = array('clients_page_fields', 'clients_page_sorton', 'clients_page_sortdir');
		foreach ($clients_setting_list as $clients_set) {
		    if ($settings[$clients_set]) {
			  $smarty->assign($clients_set, $settings[$clients_set]);
			}
		}
		$fields = get_clients_fields();
		$smarty->assign('fields', $fields);
		
		
		foreach (explode(',',$_SESSION['settings']['clients_page_fields']) as $key=> $curr_fieldname) {
			$curr_list[$curr_fieldname] = ($key+1);
		}
		var_dump($curr_list);
		$smarty->assign('curr_list', $curr_list);
		$smarty->assign('sorton', $_SESSION['settings']['clients_page_sorton']);
		$smarty->assign('sortdir', $_SESSION['settings']['clients_page_sortdir']);
		break;
		
	case "Users":
		$template = "settings_page.tpl";

		$settings = $_SESSION['settings'];
		$users_setting_list = array('users_page_fields', 'users_page_sorton', 'users_page_sortdir');
		foreach ($users_setting_list as $users_set) {
		    if ($settings[$users_set]) {
			  $smarty->assign($users_set, $settings[$users_set]);
			}
		}
		$fields = get_users_fields();
		$smarty->assign('fields', $fields);
		
		foreach (explode(',',$_SESSION['settings']['users_page_fields']) as $key=> $curr_fieldname) {
			$curr_list[$curr_fieldname] = ($key+1);
		}
		var_dump($curr_list);
		$smarty->assign('curr_list', $curr_list);
		$smarty->assign('sorton', $_SESSION['settings']['users_page_sorton']);
		$smarty->assign('sortdir', $_SESSION['settings']['users_page_sortdir']);
		break;
	
	case "Jobs":
		$template = "settings_page.tpl";
		$settings = $_SESSION['settings'];
		$jobs_setting_list = array('jobs_page_fields', 'jobs_page_sorton', 'jobs_page_sortdir');
		foreach ($jobs_setting_list as $jobs_set) {
		    if ($settings[$jobs_set]) {
			  $smarty->assign($jobs_set, $settings[$jobs_set]);
			}
		}
		$fields = get_jobs_fields();
		$smarty->assign('fields', $fields);
		
		foreach (explode(',',$_SESSION['settings']['jobs_page_fields']) as $key=> $curr_fieldname) {
			$curr_list[$curr_fieldname] = ($key+1);
		}
		var_dump($curr_list);
		$smarty->assign('curr_list', $curr_list);
		$smarty->assign('sorton', $_SESSION['settings']['jobs_page_sorton']);
		$smarty->assign('sortdir', $_SESSION['settings']['jobs_page_sortdir']);
		break;

	default:  #Show users.

		
		#$smarty->assign('leftbar_submenu_title', "Actions");
		#add_leftbar($leftbar_submenu, "Create User", "user.php?Action=create");
		
		############################################
		## Deal with sorting for the query.
		$settings = $_SESSION['settings'];
		$main_setting_list = array('start_page', 'theme', 'language', 'notification', 'per_page', 'vacation');
		foreach ($main_setting_list as $main_set) {
		    if ($settings[$main_set]) {
			  $smarty->assign($main_set, $settings[$main_set]);
			}
		}
		
		dumper($settings);
		break;
}

add_leftbar($leftbar_menu, "Main Settings", "settings.php");
add_leftbar($leftbar_menu, "Jobs Page", "settings.php?View=Jobs");
add_leftbar($leftbar_menu, "Clients Page", "settings.php?View=Clients");
add_leftbar($leftbar_menu, "Users Page", "settings.php?View=Users");
add_leftbar($leftbar_menu, "UI Permissions", "ui_permissions.php");
add_leftbar($leftbar_menu, "Pending Clients", "clients.php?View=Pending");

$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('leftbar_submenu', $leftbar_submenu);
$smarty->assign('View', $_GET['View']);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display($template);
#var_dump($_POST);
?>
