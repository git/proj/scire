<?php

require_once('simpletest/unit_tester.php');
require_once('simpletest/reporter.php');

require_once 'cron2.php';

//     May 2007    
// Mo Tu We Th Fr Sa Su
//     1  2  3  4  5  6
//  7  8  9 10 11 12 13
// 14 15 16 17 18 19 20
// 21 22 23 24 25 26 27
// 28 29 30 31

class CronTest extends UnitTestCase {
     var $now;

     function CronTest(){
	  $this->UnitTestCase();
     }

     function setUp() {
	  $this->now = strftime("%M,%H,%d,%m,%w,%Y", mktime(10,0,0,5,25,2007));
     }

     function tearDown() {
	  unset($this->now);
     }

     function testCorrectInputValidation() {
	  $test_cases = array("* * * * *", 
			      "  *  *  *                   *  *" );
	  $fail = true;
	  foreach ($test_cases as $case) {
	       try {
		    $t = new CronParser($case);
	       } catch (CronException $ce) {
		    $this->assertFalse($fail);
		    $this->fail('Wrong formated input: ' . $case);
	       }
	  }
	  $this->assertNoErrors();
     }

     function testIncorrectInputValidation() {
	  $test_cases = array("*-*-*-*",
			      "5-10    2  -2          5",
			      "  *  *    *        *");
	  $fail = $true;
	  foreach ($test_cases as $case) {
	       try {
		    $t = new CronParser($case);
	       } catch (CronException $ce) {
		    continue;
	       }
	       $this->assertFalse($fail);
	  }
	  $this->assertNoErrors();
     }

     function testCorrectInputRange() {
	  $t = new CronParser("0 0 1 1 0");
	  $this->assertNoErrors();
     }

     function test_IncorrectInputRange() {
	  $test_cases = array("-1 -4 -6 90 80",
 			      "0 0 0 0 0",
 			      "3/5 22 1 12 8",
 			      "1/4 * 20-11 1 1",
 			      "12/2 2 2 * *");
	  $fail = true;
	  foreach ($test_cases as $case) {
	       try {
		    $t = new CronParser($case);
	       } catch (CronException $ce) {
		    continue;
	       }
	       $this->assertFalse($fail, "error: ".$case);
	  }
	  $this->assertNoErrors();
     }

     function test_EventCalculation() {
	  $test_cases = array ( array ( 
				     "test" => "*/15 * * * *" ,
				     "next" => array(0,10,25,5,2007),
				     "prev" => array(45,9,25,5,2007)
				     ),
				array (
				     "test" => "2 * * * *" ,
				     "next" => array(2,10,25,5,2007),
				     "prev" => array(2,9,25,5,2007)
				     ),
				array (
				     "test" => "1-30/15 * * * *" ,
				     "next" => array(1,10,25,5,2007),
				     "prev" => array(16,9,25,5,2007)
				     ),
				array (
				     "test" => "0 2,7 * * 1-5" ,
				     "next" => array(0,2,28,5,2007),
				     "prev" => array(0,7,25,5,2007)
				     ),
				array (
				     "test" => "9 2,9,22 */4 */3 *",
				     "next" => array(9,2,1,7,2007),
				     "prev" => array(9,22,29,4,2007)
				     ),
	       );

	  foreach ($test_cases as $case) { 
	       $t = new CronParser($case["test"]);
	       $t->setNow($this->now);
//	       echo $case["test"];
//	       $this->assertSame(explode(",",$case["prev"]), $t->getLastRan());
	       $this->assertEqual($case["next"], $t->calculateNextRun());
	       }
     
     }

}


$test = &new CronTest();
$test->run(new HtmlReporter());

?>