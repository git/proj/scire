<?
include('../.lib/module_common.php');
$mod_path = "modules/quickstart";

$smarty->assign('leftbar', "on");
$leftbar_menu = array();

add_leftbar($leftbar_menu, "Edit Profiles", "$mod_path/profiles.php");
add_leftbar($leftbar_menu, "Install Client", "$mod_path/install_client.php");

$clients = array();
#The first thing we need to do is parse the quickstart server data file.
#Find the file.
$fh = fopen("sample_data.csv", "r");
#Open and loot.
if ($fh) {
	while(!feof($fh)) {
		$line = trim(fgets($fh));
		#File is a CSV with MAC,profile,status
		list($client['mac'], $client['ip'], $client['profile'], $client['status']) = explode(",",$line);
		array_push($clients,$client);
	}
	fclose($fh);
}



#Get a list of the groups to put in the select box.
$profile_list = get_profile_list();

$smarty->assign('clients',$clients);
$smarty->assign('profiles',$profile_list);
$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->display($mod_path."/index.tpl");
#pre_var_dump($clients);
#var_dump($profile_list);
?>
