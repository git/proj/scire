<?php
include('.lib/common.php');
include('cron2.php');

$smarty->assign('leftbar', "on");
$leftbar_menu = array();

add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "jobs.php");


$jobid = "";

list($minutes,$hours,$days,$weekdays,$months) = get_cron_times();

  
function scheduleUIHelper($cronString) {
    $bits = explode(" ", $cronString);
    if ($bits[0] != $_POST["minutes"] and $_POST["minutes"] != "other") {
        $bits[0] = $_POST["minutes"];
    }
    if ($bits[1] != $_POST["hours"] and $_POST["hours"] != "other") {
        $bits[1] = $_POST["hours"];
    }
    if ($bits[2] != $_POST["days"] and $_POST["days"] != "other") {
        $bits[2] = $_POST["days"];
    }
    if ($bits[3] != $_POST["months"] and $_POST["months"] != "other") {
        $bits[3] = $_POST["months"];
    }
    if ($bits[4] != $_POST["weekdays"] and $_POST["weekdays"] != "other") {
        $bits[4] = $_POST["weekdays"];
    }
    print_r($bits);
    
    return implode(" ",$bits);
}

#####################################################
#Deal with submitted forms.

if ($_POST['ADD']) {
	check_action_access("Add Job");
	if ($_POST['script']) {
		$script = get_scire_script($_POST['script']);
		if ($_POST['permission']) {
			$permission = $_POST['permission'];
			$permission = get_scire_permission_by_name($permission);
			$permission = $permission['permid'];
		#	print "Permission is: $permission";
		} else {
			$permission = $script['permission'];
		}
		if ($_POST['priority']) {
			$priority = $_POST['priority'];
		} else {
			$priority = $script['priority'];
		}
	} else { $status .= "ERROR: No script selected!"; }
	if ($_POST['description']) {
		$description = $_POST['description'];
	} else {
		$description = "No description provided.";
	}
	$pending = sizeof($_POST['clients']) + sizeof($_POST['clientgroups']);

	if (!$status and ($pending or $_POST['clientgroups'])) {  #We have a script and clients;
		# Get the schedule!  All fields are required or else you're SOL
		$scheduleComplete = $_POST["minute1"] and $_POST["hour1"] and
			$_POST["day1"] and $_POST["month1"] and $_POST["weekday1"];
		if ($scheduleComplete) {
			$str = implode(" ", array($_POST["minute1"], $_POST["hour1"],
				$_POST["day1"], $_POST["month1"], $_POST["weekday1"]));
		} else {
			$str = "";
		}
#		pre_var_dump($_POST);
#		pre_var_dump($scheduleComplete);
		pre_var_dump($str);

		try {
			$result = scire_add_job($_POST['script'], $priority, $_SESSION['userid'], $permission, $description, $pending, $_POST['clients'], $_POST['clientgroups'], $dependency, $str, $_POST['validity_period']);

			if (!$result) {
				$status .= "Job successfully added.";
			} else {
				$status .= "Error occurred during job addition. $result";
			}
		} catch (CronException $e) { print_r($e); }

	}
}

###############################################


###############################################
# Editing a job's information.
if ($_POST['EditJobSubmit']) {
	check_action_access("Edit Job");
	$jobinfo = get_scire_job($_GET['jobid']);
	$smarty->assign('job', $jobinfo);
	
	if (isset($_POST['editpriority']) and ($_POST['editpriority'] != $jobinfo['priority'])) {
		$fields['priority'] = $_POST['editpriority'];
	}
	if ($_POST['permission'] and ($_POST['permission'] != $jobinfo['permission'])){
		$fields['permission'] = $_POST['permission'];
	}
	if ($_POST['description'] and ($_POST['description'] != $jobinfo['description'])) {
            $fields['description'] = trim($_POST['description']);
	}
	if ($_POST['script'] and ($_POST['script'] != $jobinfo['script'])) {
		$fields['script'] = $_POST['script'];
	}
	if ($_POST['run_schedule'] and ($_POST['run_schedule'] != $jobinfo['run_schedule'])) {
            try{
                $c = new CronParser($_POST['run_schedule']);
                $fields['run_schedule'] = $_POST['run_schedule'];
            } catch (CronException $e) {}
	} else {
            $isScheduleUI = $_POST["minutes"] and $_POST["hours"] and $_POST["days"] and $_POST["months"] and $_POST["weekdays"];
            if ($isScheduleUI)
            try{
                $tmp = scheduleUIHelper($jobinfo["run_schedule"]);
                $c = new CronParser($tmp);
                $fields['run_schedule'] = $tmp;
            } catch (CronException $e) {}
        }
	if ($_POST['validity_period'] and ($_POST['validity_period'] != $jobinfo['validity_period'])) {
		$fields['validity_period'] = $_POST['validity_period'];
	}
	$status = scire_edit_job($_POST['jobid'], $fields);
	$_GET['Action'] = "edit";
	$_GET['jobid'] = $_POST['jobid'];
}

###############################################
# Delete a job.
if ($_POST['delete_confirm']) {
 	check_action_access("Delete Job");
# 	$status = scire_del_job($_POST['jobid']);
 	header('Location: ' . $baseurl . 'jobs.php');
}
if ($_POST['delete_cancel']) {
	header('Location: ' . $baseurl . 'jobs.php');
}

########################################################
################## MAIN SWITCH #########################
switch($_GET['Action']) {
 	case "create":
		check_action_access("Add Job"); #Access check
		add_leftbar($leftbar_menu, "&#171;&#171;Cancel", "jobs.php");
		
		if ($_GET['scriptid']) {
			$_GET['scriptid'] = (int) $_GET['scriptid'];
			$script = get_scire_script($_GET['scriptid']);
			$smarty->assign('script', $script);
		#	print $script['script_data'];
			preg_match_all('/%%(.*?)%%/', $script['script_data'], $matches);
			$smarty->assign('dyn_tags', $matches[1]);
			foreach ($matches[1] as $tag) {
				$tag_values[$tag] = get_dyn_tag_value($_GET['scriptid'],$tag);
			}
			$smarty->assign('tag_values', $tag_values);
			
			pre_var_dump($tag_values);
		}
		
		if ($_GET['clientid']) {
			$_GET['clientid'] = (int) $_GET['clientid'];
			$client = get_scire_client($_GET['clientid']);
			#$id = $acl->get_object_id("clients",$client['hostname'],'AXO'
			
			$smarty->assign('client', $client);
		}
		########################################################
		
		$permissions = array();
		$all_permissions = $acl->get_objects(NULL, 1, 'ACO');
		foreach ($all_permissions as $categoryname => $cat) {
			#print "Perm: $perm<br>Permname: $permname<br>";
			if ($categoryname != $scire_ui_section) {
				foreach ($cat as $perm) {
					array_push($permissions, $perm);
				}
			}
		}
		$smarty->assign('perms', $permissions);
		#pre_var_dump($permissions);
		$smarty->assign('priorities', get_priorities());
		
		
		$scripts = get_scire_scripts();
		$smarty->assign('scripts', $scripts);
		
		$smarty->assign('cgroups', $acl->format_groups($acl->sort_groups('AXO'), TEXT));
		$clients = get_scire_clients('hostname', 'asc');
		$smarty->assign('clients', $clients);
        $smarty->assign('minutes', $minutes);
        $smarty->assign('hours', $hours);
        $smarty->assign('days', $days);
        $smarty->assign('months', $months);
        $smarty->assign('weekdays', $weekdays);

		break;
	case "edit":
		check_action_access("Edit Job");#Access check
		$acls = array();
		#make the topbar look good
		$smarty->assign('desc', "Edit client information.");
		$jobinfo = get_scire_job($_GET['jobid']);
		$smarty->assign('job', $jobinfo);
		$smarty->assign('jobid', $_GET['jobid']);
                $smarty->assign('scripts', get_scire_scripts());
//                $cron = new CronParser("*/2 7-18/3 * * *");
//                $smarty->assign('cron', $cron);
                $smarty->assign('minutes', $minutes);
                $smarty->assign('hours', $hours);
                $smarty->assign('days', $days);
                $smarty->assign('months', $months);
                $smarty->assign('weekdays', $weekdays);

                $bits = explode(" ", $jobinfo["run_schedule"]);
                $smarty->assign('selectedMinute', array_key_exists($bits[0], $minutes) ? $bits[0] : "other" );
                $smarty->assign('selectedHour', array_key_exists($bits[1], $hours) ? $bits[1] : "other" );
                $smarty->assign('selectedDay', array_key_exists($bits[2], $days) ? $bits[2] : "other" );
                $smarty->assign('selectedMonth', array_key_exists($bits[3], $months) ? $bits[3] : "other" );
                $smarty->assign('selectedWeekday', array_key_exists($bits[4], $minutes) ? $bits[4] : "other" );

// 		$smarty->assign('gli_profiles', get_scire_gli_profiles());
// 		$smarty->assign('oss', get_scire_oss());
// 		$smarty->assign('users', get_scire_users('username', 'desc'));
		
// 		$clientgroups = array();
 		$groups = $acl->get_object_groups($_GET['jobid'],'AXO');
		if ($groups) {
			foreach ($groups as $group) {
				$group_data = $acl->get_group_data($group, 'AXO');
				$clientgroups_entry['name'] = $group_data[3];
				$clientgroups_entry['id'] = $group_data[0];
				if ($group_data[1]) {
				  $parent_group_data = $acl->get_group_data($group_data[1],'AXO');
				  $clientgroups_entry['parent'] = $parent_group_data[3];
				} else {
				  $clientgroups_entry['parent'] = "";
				}
				array_push($clientgroups, $clientgroups_entry);
				$result_acl_ids = $acl->search_acl(FALSE,FALSE, 
											       FALSE,FALSE,FALSE,
												   FALSE,FALSE,$group_data[3],FALSE);
				foreach ($result_acl_ids as $acl_id) {
					array_push($acls, $acl->get_acl($acl_id));
				}
			}
		}
		$smarty->assign('groups', $clientgroups);
		
	case "delete":
		check_action_access("Delete Job");#Access check
		$jobinfo = get_scire_job($_GET['jobid']);
		$smarty->assign('job', $jobinfo);
		#pre_var_dump($jobinfo);
		$smarty->assign('jobid', $_GET['jobid']);
		break;
}

$smarty->assign('get', $_GET);
$smarty->assign('leftbar_menu', $leftbar_menu);
$smarty->assign('Action', $_GET['Action']);
$smarty->assign('status', $status);
$smarty->display('job.tpl');
#pre_var_dump($_POST);
?>
