<?php
include('webgliIP.php');
include('webgliCF.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');
$cf = new ClientConfiguration();
#$cf->parse('testcc.xml');
$etc_files = $ip->get("etc_files");
$smarty->assign('dynamic', $ip->get("dynamic_stage3"));

$error_msg = "";
if ($_POST['savemakeconf']) {
	$cflags = "";
	$features = " ";
	if (!array_key_exists("make.conf", $etc_files)) {
		$etc_files['make.conf'] = array();
	}
	
	if ($_POST['proc'] and $_POST['m_thing'] and ($_POST['m_thing'] != "-O0") ) {
		$cflags .= " ".$_POST['m_thing']."=".$_POST['proc'];
	}
	if ($_POST['optim1']) {
		$cflags .= " ".$_POST['optim1'];
	}
	if ($_POST['optim2']) {
		foreach ($_POST['optim2'] as $param) {
			$cflags .= " ".$param;
		}
	}
	if ($_POST['optim3']) {
		$cflags .= " ".$_POST['optim3'];
	}
	if ($cflags) {
		$etc_files['make.conf']['CFLAGS'] = $cflags;
	}
	
	if ($_POST['chost']) {
		$etc_files['make.conf']['CHOST'] = $_POST['chost'];
	}
	if ($_POST['unstable']) {
		$etc_files['make.conf']['ACCEPT_KEYWORDS'] = "~" . $cf->get("architecture_template");
	}
	if ($_POST['features']) {
		$features .= join(" ",$_POST['features']);
	}
	if ($_POST['manfeatures']) {
		$features .= " ".$_POST['manfeatures'];
	}
	if ($features) {
		$etc_files['make.conf']['FEATURES'] = $features;
	}
	if (strpos($features,"distcc")) {
		#Add distcc to the services list.
		$services = $ip->get("services");
		if (!in_array("distccd",$services)) {
			array_push($services, "distccd");
		}
		$ip->set("services", $services) or 
			$error_msg .= "ERROR! Could not set the services list.";
		#Set the distcc flag to emerge earlier than other packages.
		$ip->set("install_distcc", 1) or 
			$error_msg .= "ERROR! Could not set the install distcc flag!";
		if ($_POST['distcc']) {
			$etc_files['make.conf']['DISTCC_HOSTS'] = $_POST['distcc'];
		}
	}
	if ($_POST['makeopts']) {
		$etc_files['make.conf']['MAKEOPTS'] = $_POST['makeopts'];
	}
	$ip->set("etc_files",$etc_files) or 
		$error_msg .= "ERROR! Could not set the make_conf correctly!";
	file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
	if (!$error_msg) $error_msg = "Values saved successfully";
}

$arch_procs['x86'] = array("i386", "i486", "i586", "pentium", "pentium-mmx", "i686", "pentiumpro", "pentium2", "pentium3", "pentium3m", "pentium-m", "pentium4", "pentium4m", "prescott", "nocona", "k6", "k6-2", "k6-3", "athlon", "athlon-tbird", "athlon-4", "athlon-xp", "athlon-mp", "k8", "opteron", "athlon64", "athlon-fx", "winchip-c6", "winchip2", "c3", "c3-2");

$smarty->assign('procs',$arch_procs[$cf->get("architecture_template")]);

$smarty->assign('bigO', array(" ","-O1","-O2 (Recommended)", "-O3", "-O4", "-O5"));

$arch_chost_options["x86"] = array('i386-pc-linux-gnu', 'i486-pc-linux-gnu', 'i586-pc-linux-gnu', 'i686-pc-linux-gnu');
$arch_chost_options["amd64"] = array('x86_64-pc-linux-gnu');
$arch_chost_options["alpha"] = array('alpha-unknown-linux-gnu');
$arch_chost_options["ppc"] = array('powerpc-unknown-linux-gnu');
$arch_chost_options["ppc64"] = array('powerpc64-unknown-linux-gnu');
$arch_chost_options["sparc"] = array('sparc-unknown-linux-gnu');
$arch_chost_options["sparc64"] = array('sparc-unknown-linux-gnu');
$arch_chost_options["hppa"] = array('hppa1.1-unknown-linux-gnu', 'hppa2.0-unknown-linux-gnu');
$arch_chost_options["mips"] = array('mips-unknown-linux-gnu');
$smarty->assign('arch_chost_options', $arch_chost_options[$cf->get("architecture_template")]);

$smarty->display('makedotconf.tpl');
