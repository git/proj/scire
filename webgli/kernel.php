<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

#CHANGE THIS TO A SESSION VAR.
$smarty->assign('advanced',True);

$error_msg = "";
print_r($_POST);

if ($_POST['setkernel']) {
	if ($_POST['manualsources']) {
		$ip->set("kernel_source_pkg", $_POST['manualsources']);
		#$error_msg .= "ERROR: Could not set the kernel sources!" . $_POST['manualsources'];
	}
	elseif ($_POST['sources']) {
		$ip->set("kernel_source_pkg", $_POST['sources']);
		#$error_msg .= "ERROR: Could not set the kernel sources!" . $_POST['sources'];
	}
	
	if ($_POST['build_method']) {
		$ip->set("kernel_build_method", $_POST['build_method']);
		#$error_msg .= "ERROR: Could not set the kernel build method!";
		if ($_POST['configuri']) {
			$ip->set("kernel_config_uri", $_POST['configuri']);
		#	$error_msg .= "ERROR: Could not set the kernel config URI!";
		}
	}
	
	if ($_POST['bootsplash']) {
		$ip->set("kernel_bootsplash", $_POST['bootsplash']);
		#$error_msg .= "ERROR: Could not set the kernel bootsplash!" . $_POST['bootsplash'];
	}
	#SAVE VALUES
	file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
	if (!$error_msg) {
		$error_msg = "Values saved successfully";
	}
}

$sources = $ip->get("kernel_source_pkg");
$build_method = $ip->get("kernel_build_method");
$bootsplash = $ip->get("kernel_bootsplash");
$configuri = $ip->get("kernel_config_uri");

$kernels["livecd-kernel"] = array("Livecd Kernel","Use the running kernel (fastest)");
$kernels["vanilla-sources"] = array("Vanilla (normal)","Normal. The Unaltered Linux Kernel ver 2.6+ (safest) (recommended)");
$kernels["gentoo-sources"] = array("Gentoo","Gentoo's optimized 2.6+ kernel. (less safe)");
$kernels["hardened-sources"] = array("Hardened","Hardened sources for the 2.6 kernel tree");
$kernels["grsec-sources"] = array("grsec","Vanilla sources with grsecurity patches");
if ($sources == "livecd-kernel" or $sources == "vanilla-sources" or $sources == "gentoo-sources" or $sources == "hardened-sources" or $sources == "grsec-sources") {
	$cust_source = False;
} else {
	$cust_source = True;
}
$smarty->assign("error_msg", $error_msg);
$smarty->assign("sources", $sources);
$smarty->assign("build_method",$build_method);
$smarty->assign("bootsplash",$bootsplash);
$smarty->assign("configuri",$configuri);
$smarty->assign("kernels",$kernels);
$smarty->assign("cust_source",$cust_source);

$smarty->display("kernel.tpl");
?>
