<?php
include('webgliCF.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;
#phpinfo();
$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';
$error_msg = "";


$cf = new ClientConfiguration();
$cf->parse('testcc.xml') or $error_msg .= "No configuration could be loaded.";




if ($_POST['SaveCC']) {
	if ($_POST['ArchType']) {
		$error_msg .= "Found an architecture:  you submitted " . $_POST['ArchType']. "<BR>\n";
		$cf->set("architecture_template", $_POST['ArchType']);
		#$error_msg .= "ERROR: Could not set the Architecture Template<br>\n";
	}
	if ($_POST['Logfile']) {
		$error_msg .= "Found a logfile: you submitted " . $_POST['Logfile'] . "<BR>\n";
		$cf->set("log_file",$_POST['Logfile']);
		#$error_msg .= "ERROR: Could not set the Logfile <BR>\n";
	}
	if ($_POST['RootMountPoint']) {
		$error_msg .= "Found a root mount point: you submitted " . $_POST['RootMountPoint'] . "<BR>\n";
		$cf->set("root_mount_point",$_POST['RootMountPoint']);
		#$error_msg .= "ERROR: Could not set the Root Mount Point<BR>\n";
	}
	if ($_POST['Network_Iface']) {
		$error_msg .= "Found a network interface: you submitted " . $_POST['Network_Iface'] . "<BR>\n";
		$cf->set("network_interface", $_POST['Network_Iface']);
		#$error_msg .= "ERROR: Could not set the Network Interface<BR>\n";
	}
	if ($_POST['Network_Type']) {
		$error_msg .= "Found a Network Type: you submitted " . $_POST['Network_Type'] . "<BR>\n";
		$cf->set("network_type", $_POST['Network_Type']);
		#$error_msg .= "ERROR: Could not set the Network Type<BR>\n";
	}
	if ($_POST['dhcp_options']) {
		$error_msg .= "Found DHCP Options.  You submitted " . $_POST['dhcp_options'] . "<BR>\n";
		$cf->set("network_dhcp_options", $_POST['dhcp_options']);
		#$error_msg .= "ERROR: Could not set the dhcp options<br>\n";
	}
	if ($_POST['ip']) {
		$error_msg .= "Found an IP: you submitted " . $_POST['ip'] . "<BR>\n";
		$cf->set("network_ip",$_POST['ip']);
		#$error_msg .= "ERROR: Could not set the IP<BR>\n";
	}
	if ($_POST['broadcast']) {
		$error_msg .= "Found an broadcast IP: you submitted " . $_POST['broadcast'] . "<BR>\n";
		$cf->set("network_broadcast", $_POST['broadcast']);
		#$error_msg .= "ERROR: Could not set the broadcast IP<BR>\n";
	}
	if ($_POST['netmask']) {
		$error_msg .= "Found an netmask IP: you submitted " . $_POST['netmask'] . "<BR>\n";
		$cf->set("network_netmask", $_POST['netmask']);
		#$error_msg .= "ERROR: Could not set the netmask IP<BR>\n";
	}
	if ($_POST['gateway']) {
		$error_msg .= "Found an gateway IP: you submitted " . $_POST['gateway'] . "<BR>\n";
		$cf->set("network_gateway",$_POST['gateway']);
		#$error_msg .= "ERROR: Could not set the gateway IP<BR>\n";
	}
	if ($_POST['http_proxy']) {
		$error_msg .= "Found an HTTP Proxy IP: you submitted " . $_POST['http_proxy'] . "<BR>\n";
		$cf->set("http_proxy", $_POST['http_proxy']);
		#$error_msg .= "ERROR: Could not set the HTTP Proxy IP<BR>\n";
	}
	if ($_POST['ftp_proxy']) {
		$error_msg .= "Found an FTP Proxy IP: you submitted " . $_POST['ftp_proxy'] . "<BR>\n";
		$cf->set("ftp_proxy", $_POST['ftp_proxy']);
		#$error_msg .= "ERROR: Could not set the FTP Proxy IP<BR>\n";
	}
	if ($_POST['rsync_proxy']) {
		$error_msg .= "Found an RSYNC Proxy IP: you submitted " . $_POST['rsync_proxy'] . "<BR>\n";
		$cf->set("rsync_proxy",$_POST['rsync_proxy']);
		#$error_msg .= "ERROR: Could not set the RSYNC Proxy IP<BR>\n";
	}
	$dns_servers = array();
	if ($_POST['dnsserver']) {
		$error_msg .= "Found an DNS server: you submitted " . $_POST['dnsserver'] . "<BR>\n";
		array_push($dns_servers, $_POST['dnsserver']);
	}
	if ($_POST['dnsserver2']) {
		$error_msg .= "Found an alternate DNS server: you submitted " . $_POST['dnsserver2'] . "<BR>\n";
		array_push($dns_servers, $_POST['dnsserver2']);
	}
	if ($dns_servers) {
		$cf->set("dns_servers", $dns_servers);
		$error_msg .= "ERROR: Could not set the DNS Server<BR>\n";
	}
	if ($_POST['EnableSSH']) {
		$error_msg .= "Found an Enable SSH Flag: you set it to " . $_POST['EnableSSH'] . "<BR>\n";
		$cf->set("enable_ssh", $_POST['EnableSSH']);
		#$error_msg .= "ERROR: Could not set the SSH flag<BR>\n";
	}
	if ($_POST['Verbose']) {
		$error_msg .= "Found an Verbose Mode Flag: you set it to " . $_POST['Verbose'] . "<BR>\n";
		$cf->set("verbose", $_POST['Verbose']);
		#$error_msg .= "ERROR: Could not set the Verbose mode flag<BR>\n";
	}
	if ($_POST['RootPass1'] and $_POST['RootPass2']) {
		$error_msg .= "Found a root password1: you submitted " . $_POST['RootPass1'] . "<BR>\n";
		$error_msg .= "Found a root password2: you submitted " . $_POST['RootPass2'] . "<BR>\n";
		if ($_POST['RootPass1'] == $_POST['RootPass2']) {
			$error_msg .= "Trying to set the root password. <br>";
			$cf->set("root_passwd",hash_password($_POST['RootPass1']));
			$error_msg .= "ERROR: Could not set the root password<BR>\n";
		} else {
			$error_msg .= "ERROR: Passwords DO NOT MATCH!<BR>\n";
		}
	}
	if ($_POST['Modules']) {
		$error_msg .= "Found an Additional Module: you submitted " . $_POST['Modules'] . "<BR>\n";
		$cf->set("kernel_modules", $_POST['Modules']);
		#$error_msg .= "ERROR: Could not set the Kernel Modules<BR>\n";
	}
	if ($_POST['SaveCC']) {
		$error_msg .= "Trying to save the profile. <br>\n";
		#SAVE VALUES
		file_put_contents('testcc.xml', $cf->serialize());  #PHP 5 only
		#$error_msg .= "ERROR: Could not save the profile!<BR>\n";
	}
	if ($error_msg == "") {
		$error_msg = "Values saved successfully";
	}
}
$smarty->assign('error_msg',$error_msg);
#########################################
$smarty->assign('advanced', True);

$subarches['i386'] = 'x86';
$subarches['i486'] = 'x86';
$subarches['i586'] = 'x86';
$subarches['i686'] = 'x86';
$subarches['x86_64'] = 'amd64';
$subarches['parisc'] = 'hppa';
$smarty->assign('subarches', $subarches);

#Choose the architecture for the Install.
#import platform
#arch = platform.machine()
#if arch in subarches: 
#	arch = subarches[arch]
#Arch selection string here.<br>

$arches["x86"] = "x86 (Pentium and Athlon Series)";
$arches["amd64"] = "AMD Athlon 64 and Opteron";
$arches["ppc"] = "PPC (New World) Macs";
$arches["sparc"] = "Sparc (don't know how to be more specific yet";
$arches["alpha"] = "Alpha";
$arches["hppa"] = "HPPA";
$smarty->assign('arches', $arches);

$modules = list_modules();
$smarty->assign('modules', $modules);

$smarty->display('clientconfig.tpl');

?>
