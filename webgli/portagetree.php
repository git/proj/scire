<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$error_msg = "";
if ($_POST['saveportage']) {
	if ($_POST['portagetype']) {
		$ip->set("portage_tree_sync_type", $_POST['portagetype']) or
			$error_msg .= "ERROR: Could not set the portage tree sync type";
	}
	if ($_POST['snapshoturi']) {
		$ip->set("portage_tree_snapshot_uri", $_POST['snapshoturi']) or
			$error_msg .= "ERROR: Could not set the portage snapshot URI";
	}
	file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
}

$smarty->assign('synctype',$ip->get("portage_tree_sync_type"));
$smarty->assign('snapshoturi',$ip->get("portage_tree_snapshot_uri"));
$smarty->assign('dynamic', $ip->get("dynamic_stage3"));

$smarty->display('portagetree.tpl');
?>
