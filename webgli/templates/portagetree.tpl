{include file="header.tpl" title="WebGLI - Portage Tree", advanced=$advanced}
<p>Portage Tree Sync Type:</p>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
{if $dynamic}
<p><b>Since you are doing a dynamic stage3 install, it requires the use of the portage snapshot contained on the livecd.  This has been auto-set.</b></p>
{else}
<form name="portage" action="" method="POST" enctype="multipart/form-data">
<p>Which method do you want to use to sync the portage tree for the installation? If choosing a snapshot you will need to provide the URI for the snapshot if it is not on the livecd.</p>
<table width="100%"  border="1">
  <tr>
    <td><input name="portagetype" type="radio" value="sync" {if $synctype == "sync"} checked {/if}>Sync</td>
    <td>Normal. Use emerge sync RECOMMENDED!</td>
  </tr>
  <tr>
    <td><input name="portagetype" type="radio" value="webrsync" {if $synctype == "webrsync"} checked {/if}>Webrsync</td>
    <td>HTTP daily snapshot. Use when rsync is firewalled.</td>
  </tr>
  <tr>
    <td><input name="portagetype" type="radio" value="snapshot" {if $synctype == "snapshot"} checked {/if}>Snapshot</td>
    <td>Use a portage snapshot, either a local file or a URL</td>
  </tr>
  <tr>
    <td><input name="portagetype" type="radio" value="none" {if $synctype == "none"} checked {/if}>None</td>
    <td>Extra cases such as if /usr/portage is an NFS mount</td>
  </tr>
</table>
<p>Snapshot URI (if doing shapshot): <input name="snapshoturi" type="text" id="snapshoturi" size="90" value="{$snapshoturi}"> or </p>
<input type="button" value="Browse the mirrors for the URL" onClick="window.open('/webgli/URIBrowser?screen=portage&baseuri=' + document.portage.snapshoturi.value, 'uribrowser', 'width=500,height=500,toolbars=no,statusbar=no,menubar=no,scrollbars=yes')">
<p><input type="submit" name="saveportage" value="Save Portage Settings">
</form>
{/if}
{include file="bottom.tpl"}
