{include file="header.tpl" title="WebGLI - Make.conf Settings", advanced=$advanced}
<b>Make.conf Settings:</b><br>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
{if $dynamic}
<b>You have selected a dynamic stage3 installation.  This setting forbids you from changing some make.conf settings until after the installation to prevent breaking the stage3 creation process.</b>
{else}
<b>NOTE: Your old values are NOT loaded here.  Do not save these settings without making sure all values are what you want.  To keep your old values just skip this step.</b>
<form action="" method="POST" enctype="multipart/form-data">
<h3>CFLAGS Settings: </h3>
<table width="100%" border="1">
  <tr>
    <td scope="col"><div align="left">Processor:<select name="proc" id="proc">
	
{section name=proc loop=$procs}
      <option value="{$procs[proc]}">{$procs[proc]}</option>
{/section}
      </select><br><input type="radio" name="m_thing" value="-march">-march <input type="radio" name="m_thing" value="-mcpu">-mcpu <input type="radio" name="m_thing" value="-mtune">-mtune </div></td>
    <td scope="col">Optimizations: <select name="optim1" id="optim1">
{section name=bigo loop=$bigO}
      <option value="-O{$smarty.section.bigo.index }">{$bigO[bigo]}</option>
{/section}
      </select></td>
  </tr>
  <tr>
    <td>Common CFLAGS:<br><input name="optim2[]" type="checkbox" value="-pipe">-pipe<br><input name="optim2[]" type="checkbox" value="-fomit-frame-pointer">-fomit-frame-pointer</td>
    <td>Additional CFLAGS: <input name="optim3" type="text" id="optim3" size="60"></td>
  </tr>
</table>
<hr>
<h3>CHOST Setting:</h3>
<select name="chost" size="4" id="chost" {if $dynamic} disabled {/if}>
{section name=key loop=$arch_chost_options}
<option value="{$arch_chost_options[key]}">{$arch_chost_options[key]}</option>
{/section}
</select>
<hr>
<table width="100%"  border="1">
  <tr>
    <td scope="col"><input name="unstable" type="checkbox" id="unstable" value="unstable" {if $dynamic} disabled {/if}>	Use unstable (~arch) </td>
    <td scope="col"><input name="features[]" type="checkbox" id="binary" value="buildpkg"> Build binary packages (buildpkg)</td>
    <td scope="col"><input name="features[]" type="checkbox" id="ccache" value="ccache"> ccache</td>
    <td scope="col"><input name="features[]" type="checkbox" id="sandbox" value="sandbox"> sandbox</td>
  </tr>
  <tr>
    <td scope="col"><input name="features[]" type="checkbox" id="distlocks" value="distlocks"> distlocks</td>
    <td scope="col"><input name="features[]" type="checkbox" id="prelink" value="prelink"> prelink</td>
    <td scope="col">More Features: <input name="manfeatures" type="text" id="manfeatures" size="10"></td>
    <td scope="col">MAKEOPTS: <input name="makeopts" type="text" id="makeopts" value="-j2" size="10" maxlength="5"></td>
  </tr>
  <tr>
    <td scope="col"><input name="features[]" type="checkbox" id="distcc" value="distcc"> Distcc</td>
    <td>Distcc Config Line:<input name="distcc" type="text" size="80"></td>
  </tr>
</table>
<p>
<input name="savemakeconf" type="submit" id="savemakeconf" value="Save Make.Conf Settings">
</p>
</form>
{/if}
{include file="bottom.tpl"}
