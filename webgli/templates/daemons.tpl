{include file="header.tpl" title="WebGLI - Daemons", advanced=$advanced}
<h2>Cron and Logging Daemons:</h2>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
<form name="daemons" method="post" action="" enctype="multipart/form-data">
<p>Choose which cron daemon to use for your system. While it is possible to not choose a cron daemon and still have a working system, this is NOT recommended and is considered a VERY BAD THING.<br>
Choose your cron daemon:</p>
<table width="100%"  border="1">
{foreach item=thiscron from=$cron_daemons key=key}
  <tr>
    <td><input name="cron" type="radio" value="{$key}" {if $cron eq $key} checked {/if}>{$key}</td>
    <td>{$thiscron}</td>
  </tr>
{/foreach}
</table>
<br>
<hr>
<p>Choose which logging daemon to use for your system.  A logger is required by the Gentoo Manual.<br>
Choose your logging daemon:</p>
<table width="100%" border="1">
{foreach item=thislog from=$log_daemons key=key}
  <tr>
    <td><input name="logger" type="radio" value="{$key}" {if $logger eq $key} checked {/if}>{$key}</td>
    <td>{$thislog}</td>
  </tr>
{/foreach}
</table>
<input type="submit" name="savedaemons" value="Save Daemons">
</form>
{include file="bottom.tpl"}
