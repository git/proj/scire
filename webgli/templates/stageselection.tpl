{include file="header.tpl" title="WebGLI - Stage Selection", advanced=$advanced}
<h4>Stage selection:</h4>
{if $error_msg }
<br><br>{$error_msg}<br><br>
{/if}
<form name="stage" action="" method="POST" enctype="multipart/form-data">
<p>Which stage do you want to start at?</p>
<table width="100%"  border="1">
  <tr>
    <td><input name="stage" type="radio" value="1" {if $stage == 1} checked {/if}>1</td>
    <td>Stage1 is used when you want to bootstrap&amp;build from scratch.</td>
  </tr>
  <tr>
    <td><input name="stage" type="radio" value="2" {if $stage == 2} checked {/if}>2</td>
    <td>Stage2 is used for building from a bootstrapped semi-compiled state.</td>
  </tr>
  <tr>
    <td><input name="stage" type="radio" value="3" {if ($stage == 3) and (!$grp_install)} checked {/if}>3</td>
    <td>Stage3 is a basic system that has been built for you (no compiling).</td>
  </tr>
  <tr>
    <td><input name="stage" type="radio" value="3+GRP" {if ($stage == 3) and $grp_install} checked {/if}>3 + GRP </td>
    <td>A Stage3 install but using binaries from the LiveCD when able.</td>
  </tr>
</table>
<p>  
<input name="dynamic" type="checkbox" id="dynamic" value="true" {if $dynamic} checked {/if}>
Generate a dynamic stage3 on the fly using the files on the LiveCD? (faster for slow Internet connections, slower for fast connections and slow drives) </p>
<p>Stage Tarball URI: <input name="tarballuri" type="text" id="tarballuri" size="90" value="{$tarball}"> or 
<input type="button" value="Browse the mirrors for the URL" onClick="window.open('/webgli/URIBrowser?screen=stage&baseuri=' + document.stage.tarballuri.value, 'uribrowser', 'width=500,height=500,toolbars=no,statusbar=no,menubar=no,scrollbars=yes')">
(requires net connectivity)</p>
<p><input type="submit" name="savestage" value="Save Stage Selection">
</form>
{include file="bottom.tpl"}
