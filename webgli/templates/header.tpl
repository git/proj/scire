{assign var=advanced value=1}
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>{$title|default:"WebGLI"}</title>
<link rel="stylesheet" href="webgli.css" type="text/css" />
  <script language="JavaScript" SRC="webgli.js">
  </script>
</head>
<body>

<table id="layout" width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
  <tr id="header">
    <td colspan="2">
      <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="50%">
            <img src="images/header_bg.png" border="0" width="100%" height="64">
          </td>
          <td>
            <img src="images/header.png" alt="Gentoo Linux Installer" border="0" width="400" height="64">
          </td>
          <td width="50%">
            <img src="images/header_bg.png" border="0" width="100%" height="64">
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td valign="top">
      <ul id="steps">
        <li><a class="step" href="/webgli/index.php">Welcome</a></li>
        <li><a class="step" href="/webgli/clientconfig.php">Client Config</a></li>
        <li><a class="step" href="/webgli/partitioning.php">Partitioning</a></li>
        <li><a class="step" href="/webgli/networkmounts.php">Network Mounts</a></li>
        <li><a class="step" href="/webgli/stageselection.php">Stage Selection</a></li>
        <li><a class="step" href="/webgli/portagetree.php">Portage Tree</a></li>
        <li><a class="step" href="/webgli/globaluse.php">Global USE Flags</a></li>
        <li><a class="step" href="/webgli/localuse.php">Local USE Flags</a></li>
{if $advanced}
        <li><a class="step" href="/webgli/makedotconf.php">Make.conf</a></li>
        <li><a class="step" href="/webgli/configfiles.php">Config Files</a></li>
        <li><a class="step" href="/webgli/etc_portage.php">etc/portage</a></li>
{/if}
        <li><a class="step" href="/webgli/kernel.php">Kernel</a></li>
        <li><a class="step" href="/webgli/bootloader.php">Bootloader</a></li>
        <li><a class="step" href="/webgli/timezone.php">Timezone</a></li>
        <li><a class="step" href="/webgli/networking.php">Networking</a></li>
        <li><a class="step" href="/webgli/daemons.php">Daemons</a></li>
        <li><a class="step" href="/webgli/extrapackages.php">Extra Packages</a></li>
        <li><a class="step" href="/webgli/services.php">Services</a></li>
        <li><a class="step" href="/webgli/users.php">Users</a></li>
        <li><a class="step" href="/webgli/review.php">Review</a></li>
      </ul>
      <ul id="menu">
        <li><a href="/webgli/saveprofile.php">Save Profile</a></li>
        <li><a href="/webgli/loadprofile.php">Load Profile</a></li>
      </ul>
    </td>
    <td id="content" width="100%" valign="top">
