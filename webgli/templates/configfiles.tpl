{include file="header.tpl" title="WebGLI - Configuration Files"}
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
<form action="" method="POST" enctype="multipart/form-data">
<table cellspacing="0" cellpadding="0" width="790" height="600" border="1">
  <tr height="33%">
    <td>
      <table width="100%" height="100%" border="1">
        <tr>
          <td width="50%">
            <table width="100%" height="100%" border="1">
              <tr>
                <td><b>Clock:</b><br>Should CLOCK be set to UTC or local?<br><input type="radio" name="clock" value="UTC" {if $clock == "UTC"} checked {/if}>UTC<br>
                  <input type="radio" name="clock" value="local" {if $clock == "local"} checked {/if}>Local
                </td>
              </tr>
              <tr>
                <td><b>Default Editor:</b><br>Choose your default editor:<br>
                  <input type="radio" name="editor" value="/bin/nano" {if $editor == "/bin/nano"} checked {/if}>/bin/nano (Default editor)<br>
                  <input type="radio" name="editor" value="/usr/bin/vim" {if $editor == "/usr/bin/vim"} checked {/if}>/usr/bin/vim (vi improved editor)<br>
                  <input type="radio" name="editor" value="/usr/bin/emacs" {if $editor == "/usr/bin/emacs"} checked {/if}>/usr/bin/emacs (The emacs editor)
                </td>
              </tr>
            </table>
          </td>
          <td width="50%"><b>Display Manager:</b><br>Choose your display manager for Xorg-x11 (note you must make sure that package also gets installed for it to work):<br>
            <input type="radio" name="disp_manager" value="xdm" {if $disp_manager == "xdm"} checked {/if}>xdm (X Display Manager (NOT recommended))<br>
            <input type="radio" name="disp_manager" value="gdm" {if $disp_manager == "gdm"} checked {/if}>gdm (Gnome Display Manager)<br>
            <input type="radio" name="disp_manager" value="kdm" {if $disp_manager == "kdm"} checked {/if}>kdm (KDE Display Manager)
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr height="66%">
    <td>
      <table width="100%" height="100%" border="1">
        <tr>
          <td width="50%"><b>Keymap</b><br>Choose your desired keymap:<br>
            <select name="keymap">
		      <option value=""> </option>
{section name=key loop=$keymap_list}
              <option value="{$keymap_list[key]}" {if $keymap_list[key] == $keymap} selected {/if}>{$keymap_list[key]}</option>
{/section}
            </select><br>
            <br><b>Windowkeys</b><br>Should we first load the 'windowkeys' console keymap?
            <input type="radio" name="windowkeys" value="yes" {if $windowkeys == "yes"} checked {/if}> Yes  <input type="radio" name="windowkeys" value="no" {if $windowkeys == "no"} checked {/if}> No <br>
            <br><b>Extended Keymaps</b><br>This sets the maps to load for extended keyboards.  Most users will leave this as is.<br>
            <input type="text" name="ext_keymap" value="{$ext_keymap}"><br>
		  </td>
          <td width="50%">
            <table width="100%" height="100%" border="1">
              <tr>
                <td><b>Console Font</b><br>Choose your desired console font:<br>
                  <select name="font">
                    <option value=""> </option>
					{section name=key loop=$font_list}
                    <option value="{$font_list[key]}" {if $font == $font_list[key]} selected {/if} >{$font_list[key]}</option>
					{/section}
                  </select>
                </td>
              </tr>
              <tr>
                <td><b>Xsession</b><br>Choose what window manager you want to start default with X if run with xdm, startx, or xinit. (common options are Gnome or Xsession: <input type="text" name="xsession" value="{$xsession}"></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<input type="submit" name="SaveConfigFiles" value="Save Values">
</form>
{include file="bottom.tpl"}
