{include file="header.tpl" title="WebGLI - Bootloader", advanced=$advanced}
<p>Bootloader Settings:</p>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
<form name="Bloader" method="post" action="" enctype="multipart/form-data">
<p>To boot successfully into your new Linux system, a bootloader will be needed. If you already have a bootloader you want to use you can select None here. The bootloader choices available are dependent on what GLI supports and what architecture your system is. Choose a bootloader:</p>
<table width="100%"  border="1">
{section name=bl loop=$bootloaders}
<tr><td><input name="bootloader" type="radio" value="{$bootloaders[bl][0]}" {if $bootloaders[bl][0] == $bootloader} checked {/if} > {$bootloaders[bl][0]}</td><td>{$bootloaders[bl][1]}</td></tr>
{/section}
</table>
{if $advanced}
	<hr>
	Most bootloaders have the ability to install to either the Master Boot Record (MBR) or some other partition. Most people will want their bootloader installed on the MBR for successful boots, but if you have special circumstances, you can have the bootloader installed to the /boot partition instead. Do you want the boot loader installed in the MBR? (YES is RECOMMENDED)
	<p>

<input name="bootmbr" type="checkbox" id="bootmbr" value="True" {if $bootmbr} checked {/if}> Install to MBR </p>
{/if}

{if (!$boot_device)}
<br>	You need to partition before you can select the boot device.<br>
{/if}
{if ($boot_device and (substr($boot_device,-1) != "a")) }
	Your boot device may not be correct.  It is currently set to {$boot_device}, but this device may not be the first to boot.  Usually boot devices end in 'a' such as hda or sda.
	<br>Please confirm your boot device.<br>
  {section name=bootdevices loop=bootdevices}
		<input type="radio" name="boot_drive_choice" value="{$bootdevices}">{$bootdevices}<br>
	{/section}
{/if}

<br>
{if $advanced}
	<p>If you have any additional optional arguments you want to pass to the kernel at boot, type them here: 
	<input name="bootargs" type="text" id="bootargs" value="{$bootargs}">
  </p>
{/if}
<p>
<input name="setbootloader" type="submit" id="setbootloader" value="Save Bootloader Settings">
</p>
</form>
{include file="bottom.tpl"}
