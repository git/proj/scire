{include file="header.tpl" title="WebGLI - Daemons", advanced=$advanced}
<h2>Kernel Settings:</h2>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
<form name="kernel" method="post" action="" enctype="multipart/form-data">
<p>Choose which kernel sources to use for your system. If using a previously-made kernel configuration, make sure the sources match the kernel used to create the configuration.</p>
<table width="100%"  border="1">
{foreach item=thissource from=$kernels key=key}
  <tr>
    <td><input name="sources" type="radio" value="{$key}" {if $sources eq $key} checked {/if}>{$key}</td>
    <td>{$thissource[0]}</td>
	<td>{$thissource[1]}</td>
  </tr>
{/foreach}
{if $advanced}
  <tr>
    <td><input name="sources" type="radio" value="Other">Other</td>
    <td>Specify your own here: <input name="manualsources" type="text" id="manualsources" value="{if $cust_source} {$sources}{/if}"></td>
  </tr>
{/if}
</table>
<hr>
{if $advanced}
<table width="507"	border="1">
  <tr>
    <td colspan="2" scope="col"><p>There are currently two ways the installer can compile a kernel for your new system. You can either provide a previously-made kernel configuration file and use the traditional kernel-compiling procedure (no initrd) or have genkernel automatically create your kernel for you (with initrd).</p>
      <p>If you do not have a previously-made kernel configuration, YOU MUST CHOOSE Genkernel. Choose which method you want to use:</p></td>
  </tr>
  <tr>
    <td width="143" scope="col"><input name="build_method" type="radio" value="genkernel" {if $build_method == "genkernel"} checked {/if}>Genkernel</td>
    <td width="348" scope="col"><input name="build_method" type="radio" value="custom" {if $build_method == "custom"} checked {/if}>Traditional (requires a config!)</td>
  </tr>
</table>
<p><input name="bootsplash" type="checkbox" id="bootsplash" value="True" {if $bootsplash} checked {/if}>Display the bootsplash screen on startup </p><p>If you have a custom kernel configuration, enter its location (otherwise just leave blank):
<input name="configuri" type="text" id="configuri" value="{$configuri}">
<input name="browseuri" type="button" id="browseuri" value="Browse" onClick="window.open('/webgli/URIBrowser?screen=kernel&baseuri=' + document.kernel.configuri.value, 'uribrowser', 'width=500,height=500,toolbars=no,statusbar=no,menubar=no,scrollbars=yes')">
</p>
{/if}
<p><input name="setkernel" type="submit" id="setkernel" value="Save Kernel Settings"></p>
</form>
{include file="bottom.tpl"}
