{include file="header.tpl" title="WebGLI - Client Configuration", advanced=$advanced}
<h2>Client Config</h2>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
<form name="CConfig" action="" method="POST" enctype="multipart/form-data">
{if $advanced}
	<table border="2">
	<tr>
		<td>
	Please select the architecture of the computer that gentoo will be installed on.  For pentium and AMD 32-bit processors, choose x86.<br>
	{foreach item=thisarch from=$arches key=key}
		  <input type="radio" name="ArchType" value="{$key}">{$thisarch}<br>
	{/foreach}
		</td>
		<td width="15"></td>
		<td>The installer logs all important events during the install process to a logfile for debugging purposes.
	The file gets copied to the new system once the install is complete.
	Enter the desired filename and path for the install log (the default is recommended):<br><input name="Logfile" type="text" maxlength="80" value="/var/log/installer.log">
		  <hr>Enter the mount point to be used to mount the partition(s) where the new system will be installed.  The default is /mnt/gentoo and is greatly recommended, but any mount point will do.<br><input name="RootMountPoint" type="text" maxlength="80" value="/mnt/gentoo">
		  <hr>Debug Mode: <input name="Verbose" type="radio" value="True">Yes <input name="Verbose" type="radio" value="False" checked>No</td>
	  </tr>
	</table>
{/if}

<hr>
<table border="1">
  <tr>
	<td width="50%">In order to complete most installs, an active Internet connection is required.  Setup one network connection for GLI to use to connect to the Internet if doing a network install.  If your desired device does not show up in the list, you can select Other and input the device name manually. <br>
	  <select name="Network_Iface" size="4">
		<option value="device">NEED WAY OF ADDING DEVICES</option>
	  </select>
			
	  <select name="Network_Type" size="3"><option value="dhcp">DHCP</option><option value="static">Manual Config</option><option value="None">None (Networkless)</option></select>
	</td>
	<td width="50%">Networking Info for Manual Configurations:<br>
	  Enter your IP address: <input name="ip" type="text" maxlength="15" value="192.168."><br>
	  Enter your Broadcast address: <input name="broadcast" type="text" maxlength="15" value=".255"><br>
	  Enter your Netmask: <input name="netmask" type="text" maxlength="15" value="255.255.255.0"><br>
	  Enter your default gateway: <input name="gateway" type="text" maxlength="15" value=".1"><br>
	  <hr>Info for DHCP Configurations:<br>DHCP Options: <input type="text" name="dhcp_options" >
	  <hr>Enter a DNS server: <input name="dnsserver" type="text" maxlength="15" value="128.118.25.3"><br>
	  Alternate DNS server: <input name="dnsserver2" type="text" maxlength="15" value=""></td>
  </tr>
</table>
{if $advanced}
	Proxy Information (if necessary):<br>
	HTTP Proxy URI: <input name="http_proxy" type="text" value=""><br>
	FTP Proxy URI: <input name="ftp_proxy" type="text" value=""><br>
	RSYNC Proxy URI: <input name="rsync_proxy" type="text" value="">
{/if}

<hr>Do you want SSH enabled during the install?  This will allow you to login remotely during the installation process.  If choosing Yes, be sure you select a new LiveCD root password!<br>
Enable SSH?: <input name="EnableSSH" type="radio" value="True">Yes
<input name="EnableSSH" type="radio" value="False" checked="checked">No
<hr>If you want to be able to login to your machine from another console during the installation,
you will want to enter a new root password for the LiveCD.
Note that this can be different from your new system's root password.
Presss Enter twice to skip this step.<br>
Enter Password:<input name="RootPass1" type="password" maxlength="80" value=""><br>
Re-enter Password to verify:<input name="RootPass2" type="password" maxlength="80" value=""><br>

{if $advanced}
	<hr>List of loaded modules:. <br>
	{section name=mod loop=$modules}
	<br>{$modules[mod]}
	{/section}
<br>Additional Modules to Load (space-separated list): <input name="Modules" type="text" maxlength="80" value=""><br>
{/if}
<!-- #Save Client Configuration File.	THIS SHOULD BE A POPUP -->
<hr><br>Save Client Configuration File:<br>
<input name="SaveCC" type="submit" value="Save Client Configuration">

<!-- #Print buttons for Next/Previous/Help/Save -->
<hr>
<table>
  <tr>
    <td><input name="LoadCC" type="button" value="Load"></td>
    <td><input name="SaveCC" type="button" value="Save"></td>
    <td><input name="Help" type="button" value="Help"></td>
    <td><input name="Previous" type="button" value="Previous"></td>
    <td><input name="Next" type="button" value="Next"></td>
  </tr>
</table>
</form>
{include file="bottom.tpl"}
