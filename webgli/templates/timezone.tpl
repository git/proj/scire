{include file="header.tpl" title="WebGLI - Timezone Selection", advanced=$advanced}
<h2>Timezone Setup </h2>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
Your current timezone is: {$current}<br>
Timezones:<br>
<form name="Timezone" method="post" action="" enctype="multipart/form-data">
<input type="hidden" name="tzback" value="{$tzback}"><br>
{section name=tz loop=$tzlist}
<a href="timezone.php?zonepath={$zonepath}{$tzlist[tz]}">{$tzlist[tz]}</a><br>
{/section}
<br><input type="submit" name="back" value="Back">
</form>
{include file="bottom.tpl"}
