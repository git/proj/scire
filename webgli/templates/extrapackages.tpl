{include file="header.tpl" title="WebGLI - Extra Packages", advanced=$advanced}
<h3>Extra Packages:</h3>			
<form name="packages" action="" method="POST" enctype="multipart/form-data">

{foreach item=param from=$show_groups key=key}
	{if ($param == "Expand")}
		<input type="hidden" name="show_{$key}" value="{$param}">
	{/if}
{/foreach}

{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
Your current package list is: { $current_package_list }
<table width="100%" border="1">
{foreach from=$package_list item=highlevel key=key}
	<tr><th scope="col"><input type="submit" name="show_{$key}" value="{if $show_groups.$key == "Expand"}Collapse{else}Expand{/if}"></th><td>Package Name</td><th scope="col">{$key}: {$highlevel[0]}</th></tr>
	<!-- #now show the packages in the group -->
	{if $show_groups.$key == "Expand"} 
		{foreach item=pkg key=pkgkey from=$highlevel[1]}
			<tr><td></td>
			<td> <input type="checkbox" name="packages[]" value="{$pkgkey}"
			{section loop=$install_packages name=ipkg}
				{if $install_packages[ipkg] == $pkgkey} checked {/if}
			{/section}
			> {$pkgkey}</td><td>{$pkg}</td></tr>
		{/foreach}
	{else}
		{foreach item=pkg key=pkgkey from=$highlevel[1]}
			{section loop=$install_packages name=ipkg}
				{if $install_packages[ipkg] == $pkgkey}
					<tr><td></td><td> <input type="checkbox" name="packages[]" value="{$pkgkey}" checked>{$pkgkey}</td><td>{$pkg}</td></tr>
				{/if}
			{/section}
		{/foreach}	
	{/if}
{/foreach}
</table>
<br>Manually specify a comma-separated list of packages (overwrites checked list):<input type="text" name="manualpackages"><br>
<input type="submit" name="SavePackages" value="Save Packages">
</form>
{include file="bottom.tpl"}
