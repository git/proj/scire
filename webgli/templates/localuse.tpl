{include file="header.tpl" title="WebGLI - Local USE Flags", advanced=$advanced}
<h2>Configuration Files Settings</h2>
<p>Make.conf Settings:</p>
{if $dynamic}
<b>You have selected a dynamic stage3 installation.  This setting forbids you from changing your USE flags until after the installation to prevent breaking the stage3 creation process.</b>
{else}
The installer will now gather information regarding the contents of /etc/make.conf
One of the unique (and best) features of Gentoo is the ability to
define flags (called USE flags) that define what components are 
compiled into applications.  For example, you can enable the alsa
flag and programs that have alsa capability will use it.	
The result is a finely tuned OS with no unnecessary components to
slow you down.
The installer divides USE flag selection into two screens, one for
global USE flags and one for local flags specific to each program.
Please be patient while the screens load. It may take awhile.
<form action="" method="POST" enctype="multipart/form-data">
<input type="hidden" name="globalflags" value="{$global_use_flags}">
<h3>Local USE Flags:</h3>
<table width="100%" border="1">
  <tr>
    <th scope="col">Active</th>
    <th scope="col">Flag</th>
    <th scope="col">Description</th>
  </tr>
 {foreach item=desc key=flag from=$use_local_desc}
  <tr>
    <td><input name="flags[]" type="checkbox" value="{$flag}">
	<!-- if $flag in system_use_flags: print "checked" ?>> --></td>
    <td>{$flag}</td>
    <td>{$desc}</td>
  </tr>
{/foreach}
</table>
<br>
<input name="savelocaluse" type="submit" id="savelocaluse" value="Save Local USE Settings">
</form>
{/if}
{include file="bottom.tpl"}
