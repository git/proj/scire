{include file="header.tpl" title="WebGLI - etc/portage", advanced=$advanced}
<h3>etc/portage/* Settings:</h3>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
<form name="etcportage" method="post" action="" enctype="multipart/form-data">
<p>Pick a file to edit:</p>
<table width="100%"  border="1">
  <tr>
    <th scope="col">Edit</th>
    <th scope="col">Filename</th>
    <th scope="col">Description</th>
  </tr>
{foreach item=thisfile from=$file_list key=key}
  <tr>
    <td><input name="editfile" type="radio" value="{$key}"></td>
    <td>{$key}</td>
    <td>{$thisfile}</td>
  </tr>
{/foreach}
</table><br>
<input name="fileeditsubmit" type="submit" value="EDIT">
{if $file_to_edit}
	<hr> Currently editing file: {$file_to_edit}<br>
	<input type="hidden" name="currentfile" value="{$file_to_edit}">
	<textarea name="filecontents" rows=6 cols=80>{$previous_contents}</textarea><br>
	<input type="submit" name="save" value="Save Changes">
{/if}
</form>
{include file="bottom.tpl"}
