{include file="header.tpl" title="WebGLI - Network Mounts", advanced=$advanced}
</h3>Network Mounts</h3>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
If you have any network shares you would like to mount during the install and for your new system, define them here. Select a network mount to edit or add a new mount.	Currently GLI only supports NFS mounts.
<form name="netmount" action="" method="POST" enctype="multipart/form-data">
<p>If you have any network shares you would like to mount during the install and for your new system, define them here. Select a network mount to edit or add a new mount. Currently GLI only supports NFS mounts.</p>
<table width="511" border="1">
  <tr>
    <td width="31">Edit</td>
    <td width="31">Type</td>
    <td width="79">Hostname/IP</td>
    <td width="79">Export</td>
    <td width="70">Mountpoint</td>
    <td width="103">Mount Options </td>
  </tr>
{section name=i loop=$network_mounts} <!-- for i,netmount in enumerate(network_mounts): -->
  <tr>
    <td><input name="edit_nfs" type="radio" id="edit_nfs" value="{$i}">Edit</td>
    <td>{$network_mounts[i].type }</td>
    <td>{$network_mounts[i].host }</td>
    <td>{$network_mounts[i].export }</td>
    <td>{$network_mounts[i].mountpoint }</td>
    <td>{$network_mounts[i].mountopts }</td>
  </tr>
{/section}
</table>
<hr>
<p>&nbsp;  </p>
<table width="100%"  border="1">
  <tr>
    <td><p>Enter the IP/hostname: <input name="hostname" type="text" id="hostname"> <input type="Submit" name="Search" value="Search"></p>
        <p>Enter the export name: <input name="export" type="text" id="export"></p>
        <p>OR</p>
        <p>Choose the export from the list of found exports: <select name="exports" size="1" id="exports"></select></p>
    </td>
    <td><p>Enter the mountpoint: <input name="mountpoint" type="text" id="mountpoint"></p>
        <p>Enter any special mount options: <input name="mountopts" type="text" id="mountopts"></p>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input name="addnfs" type="submit" id="addnfs" value="Add New NFS Mount"></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</form>
{include file="bottom.tpl"}
