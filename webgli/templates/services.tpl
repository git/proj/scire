{include file="header.tpl" title="WebGLI - Services", advanced=$advanced}
<h3>Services</h3>
{if $error_msg}
<br>{$error_msg}<br><br>
{/if}
Choose the services you want started on bootup.  Note that depending on what packages are selected, some services listed will not exist.
<form name="services" method="post" action="" enctype="multipart/form-data">
<table border="1">
  <tr>
    <td>Activate</td>
    <td>Service</td>
    <td>Description</td>
  </tr>
{foreach from=$choices item=value key=key}
  <tr>
    <td><input type="checkbox" name="services[]" value="{$key}" {if in_array($key,$services) } checked {/if}></td>
    <td>{$key}</td>
    <td>{$value}</td>
  </tr>
{/foreach}
</table>
<hr>You can also opt to enter your services in a comma-separated list (NOTE Manual list overwrites checked selections!): 
<input type="text" name="servicesmanual" value="{$manualservices}"><br>
<input type="submit" name="saveservices" value="Save Services">
</form>
{include file="bottom.tpl"}
