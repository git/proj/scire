<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$error_msg = "";
$network_mounts = $ip->get("network_mounts");
# THE PYHTML WAY DID DEEP COPY HERE.
if ($_POST['addnfs']) {
	if (!$_POST['hostname']) $error_msg .= "ERROR: Hostname not found.";
	if (!$_POST['export']) $error_msg .= "ERROR: Export not found.";
	if (!$_POST['mountpoint']) $error_msg .= "ERROR: Mountpoint not found.";
	array_push($network_mounts, array('export'=> $_POST['export'], 'host'=> $_POST['hostname'], 'mountopts'=> $_POST['mountopts'], 'mountpoint'=> $_POST['mountpoint'], 'type'=> 'nfs'));
	$ip->set("network_mounts",$network_mounts) or
		$error_msg .= "ERROR: Could not add network mount.";
}
elseif ($_POST['Search']) {
	$error_msg .= "Function not yet implemented!  Very sorry about that!";
}
$smarty->assign('network_mounts',$network_mounts);
$smarty->display('networkmounts.tpl');
?>
