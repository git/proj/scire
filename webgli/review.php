<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$error_msg = "";
$data = "";
$data .= "Look carefully at the following settings to check for mistakes.\nThese are the installation settings you have chosen:\n\n";

#Partitioning
$data .= "Partitioning:	\n	Key: Minor, Pri/Ext, Filesystem, MkfsOpts, Mountpoint, MountOpts, Size.\n";
$devices = $ip->get("partition_tables"); #shared_info.install_profile.get_partition_tables()
#var_dump($devices);
$drives = array_keys($devices);
sort($drives);
foreach ($drives as $drive) {
	$data .= "  Drive: $drive\n";
	$partlist = $devices[$drive];
	sort($partlist);
				#tmpparts = devices[drive] #.get_partitions()
	foreach( $partlist as $part) {
		#tmppart = tmpparts[part]
		$entry = "		 ";
		if ($part["type"] == "free") { #tmppart.get_type() == "free":
			#partschoice = "New"
			$entry .= " - Unallocated space (";
			$entry .= $part["mb"] . "MB)";    	 #str(tmppart.get_mb()) . "MB)"
		} elseif ($part["type"] == "extended") {
			$entry .= $part["minor"];  #tmppart.get_minor()))
			$entry .= " - Extended Partition (" . $part["mb"] . "MB)"; # str(tmppart.get_mb())
		} else {
			$entry .= $part["minor"] . " - "; #str(int(tmppart.get_minor())) . " - "
			if ($part["minor"] > 4) { #tmppart.is_logical():
				$entry .= "Logical (";
			} else {
				$entry .= "Primary (";
			}
			$entry .= $part["type"] . ", "; 	#tmppart.get_type() . ", "
			$entry .= $part["mkfsopts"] . ", ";	#(tmppart.get_mkfsopts() or "none") . ", "
			if ($part["mountpoint"]) {
				$entry .= $part["mountpoint"];
			} else {
				$entry .= "none";
			}
			$entry .= ", "; #(tmppart.get_mountpoint() or "none") . ", "
			if ($part["mountopts"]) {
				$entry .= $part["mountopts"];
			} else {
				$entry .= "none";
			}
			$entry .= ", ";  #(tmppart.get_mountopts() or "none") . ", "
			$entry .= $part["mb"] . "MB)";	#str(tmppart.get_mb()) . "MB)"
		}
		$data .= $entry . "\n";
	}
}
/*
#Network Mounts:
network_mounts = copy.deepcopy(shared_info.install_profile.get_network_mounts())
$data .= "\nNetwork Mounts: \n"
for mount in network_mounts:
	$data .= "  ".mount['host'].":".mount['export']."\n"
*/			
#Install Stage:
$data .= "\nInstall Stage: " . $ip->get("install_stage") . "\n";
if ($ip->get("dynamic_stage3")) {
	$data .= "  Tarball will be generated on the fly from the CD.\n";
} else {
	$data .= "  Tarball URI: " . $ip->get("stage_tarball_uri") . "\n";
}

#Portage Tree Sync Type:
$data .= "\nPortage Tree Sync Type: " . $ip->get("portage_tree_sync_type") . "\n";
if ( $ip->get("portage_tree_sync_type") == "snapshot") {
	$data .= "  Portage snapshot URI: " . $ip->get("portage_tree_snapshot_uri") . "\n";
}

#Kernel Settings:
$data .= "\nKernel Settings:\n";
$data .= "  Kernel Sources: " . $ip->get("kernel_source_pkg") . "\n";
if ($ip->get("kernel_source_pkg") != "livecd-kernel") {
	$data .= "  Kernel Build Method: " . $ip->get("kernel_build_method") . "\n";
	if ( $ip->get("kernel_build_method") == "genkernel") {
		$data .= "  Kernel Bootsplash Option: " . $ip->get("kernel_bootsplash") . "\n";
	}
}
if ( $ip->get("kernel_config_uri")) {
	$data .= "  Kernel Configuration URI: " . $ip->get("kernel_config_uri") . "\n";
}

#Bootloader Settings:
$data .= "\nBootloader Settings:\n";
$data .= "  Bootloader package: " . $ip->get("boot_loader_pkg") . "\n";
if ( $ip->get("boot_loader_pkg") != "none") {
	$data .= "  Install bootloader to MBR: " . $ip->get("boot_loader_mbr") . "\n";
	$data .= "  Bootloader kernel arguments: " . $ip->get("bootloader_kernel_args") . "\n";
}
			
#Timezone:
$data .= "\nTimezone: " . $ip->get("time_zone") . "\n";
/*
#Networking Settings:
$data .= "\nNetworking Settings: \n"
interfaces = shared_info.install_profile.get_network_interfaces()
for iface in interfaces:
	if interfaces[iface][0] == 'dhcp':
		$data .= "  " . iface . _(u":	Settings: DHCP. Options: ") . interfaces[iface][1] . "\n"
	else:
		$data .= "  " . iface . _(u"IP: ") . interfaces[iface][0] . _(u" Broadcast: ") . interfaces[iface][1] . _(u" Netmask: ") . interfaces[iface][2] . "\n"
default_gateway = shared_info.install_profile.get_default_gateway()
if default_gateway:
	$data .= "  Default Gateway: " . default_gateway[0] . "/" . default_gateway[1] . "\n"
$data .= "  Hostname: " . shared_info.install_profile.get_hostname() . "\n"
if shared_info.install_profile.get_domainname():
	$data .= "  Domainname: " .shared_info.install_profile.get_domainname() . "\n"
if shared_info.install_profile.get_nisdomainname():
	$data .= "  NIS Domainname: " .shared_info.install_profile.get_nisdomainname() . "\n"
if shared_info.install_profile.get_dns_servers():
	for dns_server in shared_info.install_profile.get_dns_servers():
		$data .= "  DNS Server: " .dns_server . "\n"
if shared_info.install_profile.get_http_proxy():
	$data .= "  HTTP Proxy: " .shared_info.install_profile.get_http_proxy() . "\n"
if shared_info.install_profile.get_ftp_proxy():
	$data .= "  FTP Proxy: " .shared_info.install_profile.get_ftp_proxy() . "\n"
if shared_info.install_profile.get_rsync_proxy():
	$data .= "  RSYNC Proxy: " .shared_info.install_profile.get_rsync_proxy() . "\n"
*/		
#Cron Daemon:
$data .= "\nCron Daemon: " . $ip->get("cron_daemon_pkg") . "\n";
		
#Logger:
$data .= "\nLogging Daemon: " . $ip->get("logging_daemon_pkg") . "\n";
		
#Extra packages:
if ($ip->get("install_packages")) {
	$data .= "\nExtra Packages: ";
	$data .= explode(' ',$ip->get("install_packages"));
	$data .= "\n";
}


#Services:
if ( $ip->get("services")) {
	$data .= "\nAdditional Services: ";
	$data .= explode(' ',$ip->get("services"));
	$data .= "\n";
}
		
#Other Configuration Settings (rc.conf):
#Make.conf Settings:
$data .= "\nConfiguration Files Settings:\n";
$etc_files = $ip->get("etc_files");
foreach ($etc_files as $name => $file) {
	$data .= "  File: $name \n";
	if (is_array($file)) {
		foreach ($file as $var => $value) {# name in etc_files[etc_file]:
			$data .= "		 Variable: $var		Value: $value\n";# . etc_files[etc_file][name] . "\n"
		}
	} else {
		foreach( $file as $entry) {# entry in etc_files[etc_file]:
			$data .= "		 Value: $entry \n";
		}
	}
}

/*
#Additional Users:
$data .= "\nAdditional Users:\n"
users = {}
for user in shared_info.install_profile.get_users():
	users[user[0]] = (user[0], user[1], user[2], user[3], user[4], user[5], user[6])
for user in users:
	$data .= "  Username: " . user
	$data .= "\n		 Group Membership: " . ",".join(users[user][2])
	$data .= "\n		 Shell: " . users[user][3]
	$data .= "\n		 Home Directory: " . users[user][4]
	if users[user][5]:
		$data .= "\n		 User Id: " . users[user][5]
	if users[user][6]:
		$data .= "\n		 User Comment: " . users[user][6]
*/

$smarty->assign('data', $data);
$smarty->display('review.tpl');

?>
