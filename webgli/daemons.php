<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$error_msg = "";
if ($_POST['savedaemons']) {
	if ($_POST['logger']) {
		$ip->set("logging_daemon_pkg", $_POST['logger']);
		$error_msg .= "ERROR: Could not set logger!<br>\n";
	}
	if ($_POST['cron']) {
		$ip->set("cron_daemon_pkg", $_POST['cron']);
		$error_msg .= "ERROR: Could not set cron daemon!<br>\n";
	}
	if ($error_msg == "") {
		$error_msg = "Values saved successfully";
	}
	#SAVE VALUES
	file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
}

#Load profile
$cron = $ip->get("cron_daemon_pkg");
$logger = $ip->get("logging_daemon_pkg");

$smarty->assign('cron',$cron);
$smarty->assign('logger',$logger);

$cron_daemons['vixie-cron'] = "Paul Vixie's cron daemon, fully featured, RECOMMENDED.";
$cron_daemons['dcron'] = "A cute little cron from Matt Dillon.";
$cron_daemons['fcron'] = "A scheduler with extended capabilities over cron & anacron.";
$cron_daemons['none'] = "Don't use a cron daemon. (NOT Recommended!)";

$log_daemons['syslog-ng'] = "An advanced system logger. (default)";
$log_daemons['metalog'] = "A Highly-configurable system logger.";
$log_daemons['syslogkd'] = "The traditional set of system logging daemons.";

$smarty->assign('cron_daemons',$cron_daemons);
$smarty->assign('log_daemons',$log_daemons);

$smarty->display('daemons.tpl');
?>
