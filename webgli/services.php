<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

if ($_POST['saveservices']) {
	if ($_POST['servicesmanual']) {
		$services = explode(',',$_POST['servicesmanual']);
		
	} elseif ($_POST['services']) {
		$services = $_POST['services'];
	}
	if ($services) {
		$ip->set("services",$services);
		#SAVE VALUES
		file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
		#$error_msg .= "ERROR! Could not set the services list.";
	}
	if (!$error_msg) $error_msg = "Values saved successfully";
}
$smarty->assign('services', $ip->get("services"));
#  I don't want to do this anymore. $smarty->assign('manualservices', join(',',$ip->get("services")));

$choices['alsasound'] = "ALSA Sound Daemon";
$choices['apache'] = "Common web server (version 1.x)";
$choices['apache2'] = "Common web server (version 2.x)";
$choices['distccd'] = "Distributed Compiling System";
$choices['esound'] = "ESD Sound Daemon";
$choices['hdparm'] = "Hard Drive Tweaking Utility";
$choices['local'] = "Run scripts found in /etc/conf.d/local.start";
$choices['portmap'] = "Port Mapping Service";
$choices['proftpd'] = "Common FTP server";
$choices['sshd'] = "SSH Daemon (allows remote logins)";
$choices['xfs'] = "X Font Server";
$choices['xdm'] = "X Daemon";
$smarty->assign('choices',$choices);
$smarty->assign('error_msg',$error_msg);
$smarty->display('services.tpl');

?>
