<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');


$error_msg = "";
if ($_GET['zonepath']) {
	$zonepath = $_GET['zonepath'];
	if (is_dir($zonepath)) {
		$zonepath .= "/";
	}
	print "I got me a zonepath: $zonepath";

	if ( substr($zonepath,-1) != '/') {
		$ip->set("time_zone",substr($zonepath,20));
		$error_msg .= "Timezone saved.<br>";
		#$error_msg .= "ERROR: Could not set that timezone!";
			#SAVE VALUES
	file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
	}

} else {
	$zonepath = "/usr/share/zoneinfo/";
}
if ($_POST['back']) {
	$zonepath = $_POST['tzback'];
}

$smarty->assign('tzback',"/usr/share/zoneinfo/");
$smarty->assign('zonepath', $zonepath);

$skiplist = array("zone.tab","iso3166.tab","posixrules", '../', './' );
if (is_dir($zonepath)) {
	$temp_tzlist = scandir($zonepath);  #PHP 5 only.
	
} else {
	$temp_tzlist = array();
}
$tzlist = array();
foreach ($temp_tzlist as $tz) {
	if (!in_array($tz, $skiplist) ) {
		if (is_dir($tz)) {
			$tz .= "/";
		}
		array_push($tzlist, $tz);
	}
}
sort($tzlist);

#print "TZList: ";
#print_r($tzlist);
#print "Zonepath: ";
#print_r($zonepath);

$smarty->assign('current',$ip->get("time_zone"));
$smarty->assign('error_msg',$error_msg);
$smarty->assign('tzlist',$tzlist);
$smarty->display('timezone.tpl');
?>
