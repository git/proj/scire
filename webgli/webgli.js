   function browserType() {
      var type;
      if (document.layers) {
        type = 'nn4';
      }
      if (document.all) {
        type = 'ie';
      }
      if (window.navigator.userAgent.toLowerCase().match("gecko")) {
        type = 'gecko';
      }
      return type;
    }
    var browser_type = browserType();
    function findLayer(name) {
      var layer;
      if (browser_type == 'gecko') {
        layer = eval('document.getElementById(\'' + name + '\')');
      } else if (browser_type == 'ie') {
        layer = eval('document.all[\'' + name + '\']');
      } else {
        layer = eval('document.layers[\'' + name + '\']');
      }
      return layer;
    }
    var resizeOb = new Object();
    function resizeStart(event) {
      var x;
      if (browser_type == 'ie') {
        resizeOb.layer = window.event.srcElement;
        x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
      } else {
        resizeOb.layer = event.target;
        x = event.clientX + window.scrollX;
      }
      resizeOb.startX = x;
      resizeOb.startW = parseInt(resizeOb.layer.style.width, 10);
      if (isNaN(resizeOb.startW)) {
        resizeOb.startW = 0;
      }
      if (browser_type == 'ie') {
        document.attachEvent("onmousemove", resizeMove);
        document.attachEvent("onmouseup", resizeStop);
        window.event.cancelBubble = true;
        window.event.returnValue = false;
      } else {
        document.addEventListener("mousemove", resizeMove, true);
        document.addEventListener("mouseup", resizeStop, true);
        event.preventDefault();
      }
    }
    function resizeMove(event) {
      var x, w;
      if (browser_type == 'ie') {
        x = window.event.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
      } else {
        x = event.clientX + window.scrollX;
      }
      w = (resizeOb.startW + (x - resizeOb.startX));
      if (w <= (parseInt(resizeOb.layer.parentNode.style.width, 10) - (15 + 4))) {
        if (w >= (parseInt(findLayer('partition_used').style.width, 10) + 10)) {
          resizeOb.layer.style.width = w + 'px';
        }
      }
      if (browser_type == 'ie') {
        window.event.cancelBubble = true;
        window.event.returnValue = false;
      } else {
        event.preventDefault();
      }
    }
    function resizeStop(event) {
      if (browser_type == 'ie') {
        document.detachEvent("onmousemove", resizeMove);
        document.detachEvent("onmouseup", resizeStop);
      } else {
        document.removeEventListener("mousemove", resizeMove, true);
        document.removeEventListener("mouseup", resizeStop, true);
      }
    }
