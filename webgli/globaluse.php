<?php
include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;
#phpinfo();
$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');
$etc_files = $ip->get("etc_files");
$smarty->assign('dynamic',$ip->get("dynamic_stage3"));

$error_msg = "";
$global_use_flags = "";
$original_flags = "";

/* This is done in localuse.php
if ($_POST['saveglobaluse']) {
	if ($_POST['flags']) {
		$global_use_flags = join(' ',$_POST['flags']);
		$global_use_flags .= " "; #extra space for next screen.
	
		$ip->set("etc_files",$etc_files);
		file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
	}
}
*/

#First set the USE flags, this is a biggie.
if (array_key_exists("make.conf",$etc_files) and $etc_files['make.conf']['USE']) {
	$original_flags = $etc_files['make.conf']['USE'];
} else {
	exec("portageq envvar USE",$output);
	print $output;
	#system_use_flags = GLIUtility.spawn("portageq envvar USE", return_output=True)[1].strip().split()
}

	
$use_flags = array();
$use_desc = get_global_use_flags();
#sort($use_desc);
$smarty->assign('use_desc',$use_desc);
#populate the choices list
#sorted_use = use_desc.keys()
#sorted_use.sort()
#present the GLOBAL checkbox list

$smarty->display('globaluse.tpl');
?>
