<?php
include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;
#phpinfo();
$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');
$etc_files = $ip->get("etc_files");

#var_dump($etc_files);
$error_msg = "";

if ($_POST['SaveConfigFiles']) {
	if(!array_key_exists("conf.d/keymaps", $etc_files)) {
		if($_POST['keymap'] || $_POST['windowkeys'] || $_POST['ext_keymap']) {
			$etc_files['conf.d/keymaps'] = array();
		}
	}
	if(!array_key_exists("conf.d/consolefont", $etc_files)) {
		if($_POST['font']) {
			$etc_files['conf.d/consolefont'] = array();
		}
	}
	if(!array_key_exists("conf.d/clock", $etc_files)) {
		if($_POST['clock']) {
			$etc_files['conf.d/clock'] = array();
		}
	}
	if(!array_key_exists("rc.conf", $etc_files)) {
		if($_POST['editor'] || $_POST['disp_manager'] || $_POST['xsession']) {
			$etc_files['rc.conf'] = array();
		}
	}
	if($_POST['keymap']) $etc_files['conf.d/keymaps']['KEYMAP'] = $_POST['keymap'];
	if($_POST['windowkeys']) $etc_files['conf.d/keymaps']['SET_WINDOWSKEYS'] = $_POST['windowkeys'];
	if($_POST['ext_keymap']) $etc_files['conf.d/keymaps']['EXTENDED_KEYMAPS'] = $_POST['ext_keymap'];
	if($_POST['font']) $etc_files['conf.d/consolefont']['CONSOLEFONT'] = $_POST['font'];
	if($_POST['clock']) $etc_files['conf.d/clock']['CLOCK'] = $_POST['clock'];
	if($_POST['editor']) $etc_files['rc.conf']['EDITOR'] = $_POST['editor'];
	if($_POST['disp_manager']) $etc_files['rc.conf']['DISPLAYMANAGER'] = $_POST['disp_manager'];
	if($_POST['xsession']) $etc_files['rc.conf']['XSESSION'] = $_POST['xsession'];
#	var_dump($etc_files);
	$ip->set("etc_files", $etc_files);
	file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
	
	if(!$error_msg) {
		$error_msg = "Values saved successfully";
	}
}

if (array_key_exists("conf.d/clock", $etc_files) and $etc_files['conf.d/clock']['CLOCK']) {
	$smarty->assign('clock', $etc_files['conf.d/clock']['CLOCK']);
}
if (array_key_exists("conf.d/keymaps", $etc_files)) {
	if ($etc_files['conf.d/keymaps']['KEYMAP']) {
		$smarty->assign('keymap',$etc_files['conf.d/keymaps']['KEYMAP']);
	}
	if ($etc_files['conf.d/keymaps']['SET_WINDOWSKEYS']) {
		$smarty->assign('windowkeys',$etc_files['conf.d/keymaps']['SET_WINDOWSKEYS']);
	}
	if ($etc_files['conf.d/keymaps']['EXTENDED_KEYMAPS']) {
		$smarty->assign('ext_keymap',$etc_files['conf.d/keymaps']['EXTENDED_KEYMAPS']);	
	}
}
if (array_key_exists("conf.d/consolefont", $etc_files) and $etc_files['conf.d/consolefont']['CONSOLEFONT']) {
	$smarty->assign('font',$etc_files['conf.d/consolefont']['CONSOLEFONT']);
}
if (array_key_exists("rc.conf", $etc_files)) {
	if ($etc_files['rc.conf']['EDITOR']) {
		$smarty->assign('editor',$etc_files['rc.conf']['EDITOR']);
	}
	if ($etc_files['rc.conf']['DISPLAYMANAGER']) {
		$smarty->assign('disp_manager',$etc_files['rc.conf']['DISPLAYMANAGER']);
	}
	if ($etc_files['rc.conf']['XSESSION']) {
		$smarty->assign('xsession',$etc_files['rc.conf']['XSESSION']);
	}
}

$keymap_list = generate_keymap_list();
$font_list = generate_consolefont_list();
$smarty->assign('keymap_list', $keymap_list);
$smarty->assign('font_list',$font_list);

$smarty->display('configfiles.tpl');
?>
