<?
require_once('XMLParser.php');

class InstallProfile extends XMLParser {
	/*
	An object representation of a profile.
	InstallProfile is an object representation of a parsed installation
	profile file.
	*/

	##
	# Initializes all variables to default values and adds handlers.
	# Configuration information - profile data
	# All variables must be declared here with a default value to ensure
	# the XML will correctly serialize.

	function InstallProfile() {
		$this->_temp_etc_file = array();
		$this->_temp_partition_table = array();
		$this->xmldoc = "";

		$this->values = array(
			'boot_device' => array( 'type' => 'string', value => '' ),
			'bootloader_kernel_args' => array( 'type' => 'string', 'value' => "" ),
			'bootloader_mbr' => array( 'type' => 'bool', 'value' => 1 ),
			'bootloader_pkg' => array( 'type' => 'string', 'value' => 'grub' ),
			'cron_daemon_pkg' => array( 'type' => 'string', 'value' => 'vixie-cron' ),
			'default_gateway' => array( 'type' => 'array', 'value' => array(), 'serialize_func' => 'serialize_default_gateway' ),
			'dns_servers' => array( 'type' => 'array', 'value' => array() ),
			'domainname' => array( 'type' => 'string', 'value' => "localdomain" ),
			'dynamic_stage3' => array( 'type' => 'bool', 'value' => 0 ),
			'etc_files' => array( 'type' => 'array', 'value' => array(), 'serialize_func' => 'serialize_etc_files' ),
			'ftp_proxy' => array( 'type' => 'string', 'value' => "" ),
			'grp_install' => array( 'type' => 'bool', 'value' => 0 ),
			'hostname' => array( 'type' => 'string', 'value' => "localhost" ),
			'http_proxy' => array( 'type' => 'string', 'value' => "" ),
			'install_distcc' => array( 'type' => 'bool', 'value' => 0 ),
			'install_packages' => array( 'type' => 'array', 'value' => array() ),
			'install_pcmcia_cs' => array( 'type' => 'bool', 'value' => 0 ),
			'install_rp_pppoe' => array( 'type' => 'bool', 'value' => 0 ),
			'install_stage' => array( 'type' => 'int', 'value' => 3 ),
			'logging_daemon_pkg' => array( 'type' => 'string', 'value' => 'syslog-ng' ),
			'kernel_bootsplash' => array( 'type' => 'bool', 'value' => 0 ),
			'kernel_build_method' => array( 'type' => 'string', 'value' => 'genkernel' ),
			'kernel_config_uri' => array( 'type' => 'uri', 'value' => '' ),
			'kernel_modules' => array( 'type' => 'array', 'value' => array() ),
			'kernel_initrd' => array( 'type' => 'bool', 'value' => 1 ),
			'kernel_source_pkg' => array( 'type' => 'string', 'value' => "livecd-kernel" ),
			'mta_pkg' => array( 'type' => 'string', 'value' => "" ),
			'network_mounts' => array( 'type' => 'array', 'value' => array(), 'serialize_func' => 'serialize_network_mounts' ),
			'network_interfaces' => array( 'type' => 'array', 'value' => array(), 'serialize_func' => 'serialize_network_interfaces' ),
			'nisdomainname' => array( 'type' => 'string', 'value' => "" ),
			'partition_tables' => array( 'type' => 'array', 'value' => array(), 'serialize_func' => 'serialize_partition_tables' ),
			'portage_tree_snapshot_uri' => array( 'type' => 'uri', 'value' => "" ),
			'portage_tree_sync_type' => array( 'type' => 'string', 'value' => "sync" ),
			'post_install_script_uri' => array( 'type' => 'uri', 'value' => "" ),
			'root_pass_hash' => array( 'type' => 'string', 'value' => "" ),
			'rsync_proxy' => array( 'type' => 'string', 'value' => "" ),
			'services' => array( 'type' => 'array', 'value' => array() ),
			'stage_tarball_uri' => array( 'type' => 'uri', 'value' => "" ),
			'time_zone' => array( 'type' => 'string', 'value' => "UTC" ),
			'users' => array( 'type' => 'array', 'value' => array(), 'serialize_func' => 'serialize_users' ),
		);

		$this->xmlpaths = array(
			'gli-profile/bootloader' => 'bootloader_pkg',
			'gli-profile/boot-device' => 'boot_device',
			'gli-profile/bootloader-kernel-args' => 'bootloader_kernel_args',
			'gli-profile/bootloader-mbr' => 'bootloader_mbr',
			'gli-profile/cron-daemon' => 'cron_daemon_pkg',
			'gli-profile/default-gateway' => 'parse_default_gateway',
			'gli-profile/dns-servers' => 'dns_servers',
			'gli-profile/domainname' => 'domainname',
			'gli-profile/dynamic-stage3' => 'dynamic_stage3',
			'gli-profile/etc-files/file' => 'parse_etc_files_file',
			'gli-profile/etc-files/file/entry' => 'parse_etc_files_file_entry',
			'gli-profile/ftp-proxy' => 'ftp_proxy',
			'gli-profile/grp-install' => 'grp_install',
			'gli-profile/hostname' => 'hostname',
			'gli-profile/http-proxy' => 'http_proxy',
			'gli-profile/install-distcc' => 'install_distcc',
			'gli-profile/install-packages' => 'install_packages',
			'gli-profile/install-pcmcia-cs' => 'install_pcmcia_cs',
			'gli-profile/install-rp-pppoe' => 'install_rp_pppoe',
			'gli-profile/install-stage' => 'install_stage',
			'gli-profile/kernel-bootsplash' => 'kernel_bootsplash',
			'gli-profile/kernel-build-method' => 'kernel_build_method',
			'gli-profile/kernel-config' => 'kernel_config_uri',
			'gli-profile/kernel-initrd' => 'kernel_initrd',
			'gli-profile/kernel-modules' => 'kernel_modules',
			'gli-profile/kernel-source' => 'kernel_source_pkg',
			'gli-profile/logging-daemon' => 'logging_daemon_pkg',
			'gli-profile/mta' => 'mta_pkg',
			'gli-profile/network-interfaces/device' => 'parse_network_interface',
			'gli-profile/network-mounts/netmount' => 'parse_netmount',
			'gli-profile/nisdomainname' => 'nisdomainname',
			'gli-profile/partitions/device' =>  'parse_partitions_device',
			'gli-profile/partitions/device/partition' => 'parse_partitions_device_partition',
			'gli-profile/portage-snapshot' => 'portage_tree_snapshot_uri',
			'gli-profile/portage-tree-sync' => 'portage_tree_sync_type',
			'gli-profile/post-install-script-uri' => 'post_install_script_uri',
			'gli-profile/root-pass-hash' => 'root_pass_hash',
			'gli-profile/rsync-proxy' => 'rsync_proxy',
			'gli-profile/services' => 'services',
			'gli-profile/stage-tarball' => 'stage_tarball_uri',
			'gli-profile/time-zone' => 'time_zone',
			'gli-profile/users/user' => 'parse_user',
		);
	}

	function serialize() {
		return $this->_serialize("gli-profile");
	}

	function parse_default_gateway($gateway, $attr) {
		$this->set("default_gateway", array($attr['interface'], $gateway));
	}

	function serialize_default_gateway() {
		$gw = $this->get("default_gateway");
		return sprintf("\t<default-gateway interface=\"%s\">\n\t\t%s\n\t</default-gateway>\n", $gw[0], $gw[1]);
	}

	function parse_etc_files_file_entry($value, $attr) {
		if($attr['name']) {
			if(!$this->_temp_etc_file) $this->_temp_etc_file = array();
			$this->_temp_etc_file[$attr['name']] = $value;
		} else {
			if(!$this->_temp_etc_file) $this->_temp_etc_file = array();
			$this->_temp_etc_file[] = $value;
		}
	}

	function parse_etc_files_file($value, $attr) {
		$etc_files = $this->get("etc_files");
		$etc_files[$attr['name']] = $this->_temp_etc_file;
		$this->_temp_etc_file = array();
		$this->set("etc_files", $etc_files);
	}

	function serialize_etc_files() {
		$etc_files = $this->get("etc_files");
		if($etc_files) {
			$this->xmldoc .= "\t<etc-files>\n";
			foreach($etc_files as $etc_file => $values) {
				$this->xmldoc .= sprintf("\t\t<file name=\"%s\">\n", $etc_file);
				$isalist = (array_keys($values) === range(0, count($values) - 1)) ? true : false;
				foreach($values as $name => $value) {
					$this->xmldoc .= "\t\t\t<entry";
					if($isalist) {
						$this->xmldoc .= sprintf(">\n\t\t\t\t%s\n", $value);
					} else {
						$this->xmldoc .= sprintf(" name=\"%s\">\n\t\t\t\t%s\n", $name, $value);
					}
					$this->xmldoc .= "\t\t\t</entry>\n";
				}
				$this->xmldoc .= "\t\t</file>\n";
			}
			$this->xmldoc .= "\t</etc-files>\n";
		}
	}

	function parse_network_interface($device, $attr) {
		$options = null;
		$ip = $broadcast = $netmask = $dhcp_options = null;
		$dhcp = true;

		foreach($attr as $name => $value) {
			switch($name) {
				case 'ip':
					$ip = $value;
					break;
				case 'broadcast':
					$broadcast = $value;
					break;
				case 'netmask':
					$netmask = $value;
					break;
				case 'options':
					$dhcp_options = $value;
					break;
			}
		}

		if($ip != 'dhcp') {
			$dhcp = false;
			$options = array($ip, $broadcast, $netmask);
		} else {
			$options = array('dhcp', $dhcp_options, '');
		}

		$network_interfaces = $this->get("network_interfaces");
		$network_interfaces[$device] = $options;
		$this->set("network_interfaces", $network_interfaces);
	}

	function serialize_network_interfaces() {
		$interfaces = $this->get("network_interfaces");
		if($interfaces) {
			$this->xmldoc .= "\t<network-interfaces>\n";
			foreach($interfaces as $iname => $interface) {
				if($interface[0] == 'dhcp') {
					$attrs = "ip=\"dhcp\"";
					if($interface[1]) {
						$dhcp_options = sprintf("options=\"%s\"", $interface[1]);
						$attrs .= " " . $dhcp_options;
					}
					$this->xmldoc .= sprintf("\t\t<device %s>%s</device>\n", $attrs, $iname);
				} else {
					$this->xmldoc .= sprintf("\t\t<device ip=\"%s\" broadcast=\"%s\" netmask=\"%s\">%s</device>\n", $interface[0], $interface[1], $interface[2], $iname);
				}
			}
			$this->xmldoc .= "\t</network-interfaces>\n";
		}
	}

	function parse_netmount($unused, $attr) {
		$netmount_entry = array('export' => '', 'host' => '', 'mountopts' => '', 'mountpoint' => '', 'type' => '');
		foreach($attr as $name => $value) {
			$netmount_entry[$name] = $value;
		}
		$network_mounts = $this->get("network_mounts");
		$network_mounts[] = $netmount_entry;
		$this->set("network_mounts", $network_mounts);
	}

	function serialize_network_mounts() {
		$network_mounts = $this->get("network_mounts");
		if($network_mounts) {
			$this->xmldoc .= "\t<network-mounts>\n";
			foreach($network_mounts as $mount) {
				$this->xmldoc .= sprintf("\t\t<netmount host=\"%s\" export=\"%s\" type=\"%s\" mountpoint=\"%s\" mountopts=\"%s\"/>\n", $mount['host'], $mount['export'], $mount['type'], $mount['mountpoint'], $mount['mountopts']);
			}
			$this->xmldoc .= "\t</network-mounts>\n";
		}
	}

	function parse_partitions_device($unused, $attr) {
		$devnode = "";
		$disklable = "";
		foreach($attr as $name => $value) {
			if($name == "devnode") {
				$devnode = $value;
			} elseif($name == "disklabel") {
				$disklabel = $value;
			}
		}
		$partition_tables = $this->get("partition_tables");
		$partition_tables[$devnode] = $this->_temp_partition_table;
		$this->_temp_partition_table = array();
		$this->set("partition_tables", $partition_tables);
	}

	function parse_partitions_device_partition($unused, $attr) {
		$part_entry = array();
		foreach($attr as $name => $value) {
			$part_entry[$name] = $value;
		}
		$part_entry['format'] = $part_entry['format'] == "True" ? true : false;
		$part_entry['resized'] = $part_entry['resized'] == "True" ? true : false;
#		if GLIUtility.is_numeric(part_entry['mb']): part_entry['mb'] = long(part_entry['mb'])
#		if GLIUtility.is_numeric(part_entry['minor']):
#			part_entry['minor'] = float(part_entry['minor'])
#			if int(part_entry['minor']) == part_entry['minor']:
#				part_entry['minor'] = int(part_entry['minor'])
#		if GLIUtility.is_numeric(part_entry['origminor']): part_entry['origminor'] = int(part_entry['origminor'])
		$this->_temp_partition_table[] = $part_entry;
	}
	
	function serialize_partition_tables() {
		$partitions = $this->get("partition_tables");
		if($partitions) {
			$this->xmldoc .= "\t<partitions>\n";
			foreach($partitions as $device => $parts) {
				$this->xmldoc .= sprintf("\t\t<device devnode=\"%s\" disklabel=\"%s\">\n", $device, "msdos"); #partitions[device].get_disklabel())
				foreach($parts as $part) {
					$this->xmldoc .= sprintf("\t\t\t<partition minor=\"%s\" origminor=\"%s\" mb=\"%s\" type=\"%s\" mountpoint=\"%s\" mountopts=\"%s\" format=\"%s\" mkfsopts=\"%s\" resized=\"%s\"/>\n", $part['minor'], $part['origminor'], $part['mb'], $part['type'], $part['mountpoint'], $part['mountopts'], $part['format'], $part['mkfsopts'], $part['resized']);
				}
				$this->xmldoc .= "\t\t</device>\n";
			}
			$this->xmldoc .= "\t</partitions>\n";
		}
	}

	function parse_user($username, $attr) {
		$tmp_user = array();
		foreach($attr as $name => $value) {
			$tmp_user[$name] = $value;
		}
		$users = $this->get("users");
		$users[] = array($username, $tmp_user['hash'], $tmp_user['groups'], $tmp_user['shell'], $tmp_user['homedir'], $tmp_user['uid'], $tmp_user['comment']);
		$this->set("users", $users);
	}

	function serialize_users() {
		$users = $this->get("users");
		if($users) {
			$this->xmldoc .= "\t<users>\n";
			foreach($users as $user) {
				$attrstr = "";
				if($user[1]) $attrstr .= sprintf("hash=\"%s\" ", $user[1]);
				if($user[2]) $attrstr .= sprintf("groups=\"%s\" ", implode(",", $user[2]));
				if($user[3]) $attrstr .= sprintf("shell=\"%s\" ", $user[3]);
				if($user[4]) $attrstr .= sprintf("homedir=\"%s\" ", $user[4]);
				if($user[5]) $attrstr .= sprintf("uid=\"%s\" ", $user[5]);
				if($user[6]) $attrstr .= sprintf("comment=\"%s\" ", $user[6]);
				$this->xmldoc .= sprintf("\t\t<user %s>\n\t\t\t%s\n\t\t</user>\n", trim($attrstr), $user[0]);
			}
			$this->xmldoc .= "\t</users>\n";
		}
	}
}
?>
