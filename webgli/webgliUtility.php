<?php
#Utility functions for WebGLI

##
# Will take a password and return it hashed in md5 format
# @param password 		the password to be hashed
function hash_password($password)
{
	$salt = "$1$";
	$chars = "./abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	for($i=0;$i<=8;$i++) { # in range(0, 8):
		$salt .= $chars[rand(0,strlen($chars)-1)];            #chars[random.randint(0, len(chars)-1)]
	}
	$salt .= "$";
	$passwd_hash = crypt($password, $salt);

	return $passwd_hash;
}

function get_eth_devices()
{
	$eth_devices = system("/sbin/ifconfig -a | grep -e '^[A-Za-z]'| cut -d ' ' -f 1");
	return split(' ',$eth_devices);
}

function list_modules()
{
	exec("lsmod | grep -v ^Module | cut -d ' ' -f 1 ", $modules);
	return $modules;
}

function generate_keymap_list()
{
	$keymap_list = array();
	$path = "/usr/share/keymaps";
	
	# find /usr/share/keymaps -iname *.map.gz -printf "%f \n"
	exec("find ".$path." -iname *.map.gz -printf \"%f \n\"",$output);
	foreach ($output as $keymap) {
		# strip the last 9 chars ( .map.gz\n )
		$keymap = trim($keymap);
		$keymap = substr($keymap,0,-7);
		array_push($keymap_list, $keymap);
	}
	# sort the keymap list
	sort($keymap_list);
	
	return $keymap_list;
}

function generate_consolefont_list()
{
	$consolefont_list = array();
	$path = "/usr/share/consolefonts";
	
	# find /usr/share/consolefonts -iname *.gz -printf "%f \n"
	exec("find ".$path." -iname *.gz -printf \"%f \n\"", $output);
	foreach ($output as $consolefont) {
		# strip the last 5 chars ( .gz\n )
		$consolefont = trim($consolefont);
		$consolefont = substr($consolefont,0,-3);
		
		# test if its psfu or psf or fnt
		# and remove it if necessary
		if (substr($consolefont,-4) == "psfu") {
			$consolefont = substr($consolefont,0,-5);
		}
		if (substr($consolefont,-3) == "psf") {
			$consolefont = substr($consolefont,0,-4);
		}
		if (substr($consolefont,-3) =="fnt") {
			$consolefont = substr($consolefont,0,-4);
		}
		array_push($consolefont_list, $consolefont);
	}		
	# sort the keymap list
	sort($consolefont_list);
	
	return $consolefont_list;
}
/* We gave up on this feature I think
function generate_consoletranslation_list()
{
	$consoletranslation_list = array();
	$path = "/usr/share/consoletrans";
	
	# find /usr/share/keymaps -iname *.trans -printf "%f \n"
	exec("find ".$path." -iname *.trans -printf \"%f \n\"", $output);
	foreach ($output as $consoletran) {
		# strip the last 8 chars ( .trans\n )
		$consoletran = trim($consoletran);
		$consoletran = substr($consoletran,0,-8);
		array_push($consoletranslation_list, $consoletran);
	}
	sort($consoletranslation_list);
	
	return $consoletranslation_list;
}
*/

function get_install_package_list()
{
	$install_package_list = array( 
		'Desktop' => array("Popular Desktop Applications",
			array("bittorrent"=> "tool for distributing files via a distributed network of nodes",
			"evolution" => "A GNOME groupware application, a Microsoft Outlook workalike",
			"gaim" => "GTK Instant Messenger client",
			"gftp" => "Gnome based FTP Client",
			"gimp" => "GNU Image Manipulation Program",
			"inkscape" => "A SVG based generic vector-drawing program",
			"koffice" => "An integrated office suite for KDE, the K Desktop Environment",
			"mozilla" => "The Mozilla Web Browser",
			"mozilla-firefox" => "The Mozilla Firefox Web Browser",
			"mozilla-thunderbird" => "Thunderbird Mail Client",
			"mplayer" => "Media Player for Linux",
			"openoffice" => "OpenOffice.org, a full office productivity suite.",
			"openoffice-bin" => "Same as OpenOffice but a binary package (no compiling!)",
			"rhythmbox" => "Music management and playback software for GNOME",
			"vlc" => "VLC media player - Video player and streamer",
			"xchat" => "Graphical IRC Client",
			"xine-ui" => "Xine movie player",
			"xmms" => "X MultiMedia System" )),
		'Servers' => array("Applications often found on servers.",
			array("apache" => "Apache Web Server",
			"cups" => "The Common Unix Printing System",
			"exim" => "A highly configurable, drop-in replacement for sendmail",
			"iptables" => "Linux kernel (2.4+) firewall, NAT and packet mangling tools",
			"mod_php" => "Apache module for PHP",
			"mysql" => "A fast, multi-threaded, multi-user SQL database server",
			"postfix" => "A fast and secure drop-in replacement for sendmail",
			"postgresql" => "sophisticated Object-Relational DBMS",
			"proftpd" => "ProFTP Server",
			"samba" => "SAMBA client/server programs for UNIX",
			"sendmail" => "Widely-used Mail Transport Agent (MTA)",
			"traceroute" => "Utility to trace the route of IP packets" )),
		'X11' => array("Window managers and X selection.", 
			array("xorg-x11" => "An X11 implementation maintained by the X.Org Foundation",
			"gnome" => "The Gnome Desktop Environment",
			"kde" => "The K Desktop Environment",
			"blackbox" => "A small, fast, full-featured window manager for X",
			"enlightenment" => "Enlightenment Window Manager",
			"fluxbox" => "Fluxbox is an X11 window manager featuring tabs and an iconbar",
			"xfce4" => "XFCE Desktop Environment"  )),
		'Misc' => array("Miscellaneous Applications you may want.",
			array("emacs" => "An incredibly powerful, extensible text editor",
			"ethereal" => "A commercial-quality network protocol analyzer",
			"gkrellm" => "Single process stack of various system monitors",
			"gvim" => "GUI version of the Vim text editor",
			"keychain" => "ssh-agent manager",
			"logrotate" => "Rotates, compresses, and mails system logs",
			"ntp" => "Network Time Protocol suite/programs",
			"rdesktop" => "A Remote Desktop Protocol Client",
			"slocate" => "Secure way to index and quickly search for files on your system",
			"ufed" => "Gentoo Linux USE flags editor",
			"vim" => "Vim, an improved vi-style text editor" )),
		'Recommended' => array("Applications recommended by the GLI Team.",
			array("anjuta" => "A versatile IDE for GNOME",
			"chkrootkit" => "a tool to locally check for signs of a rootkit",
			"crack-attack" => "Addictive OpenGL-based block game",
			"gnupg" => "The GNU Privacy Guard, a GPL pgp replacement",
			"net-snmp" => "Software for generating and retrieving SNMP data",
			"netcat" => "the network swiss army knife",
			"nmap" => "A utility for network exploration or security auditing",
			"screen" => "full-screen window manager that multiplexes between several processes",
			"xpdf" => "An X Viewer for PDF Files" ))
	);
	return $install_package_list;
}

function get_grp_pkgs_from_cd() {
	if (!is_file("/usr/livecd/grppkgs.txt")) {
		return "";
	}
	exec('cat /usr/livecd/grppkgs.txt',$output);
	$output_arr = explode($output);
	#remove the first part before a / for comparision
	$results = array();
	foreach ($output_arr as $pkg) {
		array_push($results, substr($pkg, int(strpos($pkg,'/'))+1 )); #.append(pkg[(pkg.find('/')+1):])
	}
	return $results;
}

function get_global_use_flags() {
	$fh = fopen("/usr/portage/profiles/use.desc", "r");
	while (!feof($fh)) {
		$line = trim(fgets($fh));
		if ( (!$line) or $line[0] == '#') {
			continue;
		}
		$dash_pos = strpos($line, " - ");
		if ($dash_pos == -1) {
			continue;
		}
		$flagname = substr($line,0,$dash_pos);
		$desc = substr($line,$dash_pos+3);
		$use_desc[$flagname] = $desc;
	}
	fclose($fh);
	return $use_desc;
}
	
function get_local_use_flags() {
	$fh = fopen("/usr/portage/profiles/use.local.desc", "r");
	while (!feof($fh)) {
		$line = trim(fgets($fh));
		if ( (!$line) or $line[0] == '#') {
			continue;
		}
		$dash_pos = strpos($line, " - ");
		if ($dash_pos == -1) {
			continue;
		}
		$colon_pos = strpos($line,":");#, 0, dash_pos);
		$pkg = substr($line,0,$colon_pos);
		$flagname = substr($line,$colon_pos+1,$dash_pos); #line[colon_pos+1:dash_pos] or line[colon_pos+1]
		$flagname = substr($line,$colon_pos+1, ($dash_pos-$colon_pos));
		$desc = "(" . $pkg . ") " . substr($line,$dash_pos+3);
		$use_local_desc[$flagname] = $desc;
	}
	fclose($fh);
	return $use_local_desc;
}
?>
