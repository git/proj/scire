<?php
include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$error_msg = "";
$etc_files = $ip->get("etc_files");
$smarty->assign('dynamic', $ip->get("dynamic_stage3"));

if ($_POST['save']) {
	if ( !$_POST['currentfile'] ) {
		$error_msg .= "ERROR you must select a file to edit!<br>";
	}
	$file = $_POST['currentfile'];
	#if (array_key_exists($file,$etc_files)) {
		$etc_files[$file] = $_POST['filecontents'];
	#} else {
	#	$etc_files[$file] = 
	#etc_files[file].append($_POST['filecontents'])
	$ip->set("etc_files",$etc_files);
	#SAVE VALUES
	file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
	if ( !$error_msg ) { $error_msg = "Values saved successfully"; }
}
if ($_POST['fileeditsubmit']) {
	if ($_POST['editfile']) {
		$file_to_edit = $_POST['editfile'];
		$smarty->assign('file_to_edit',$file_to_edit);
		if (array_key_exists($file_to_edit,$etc_files)) {
			$previous_contents = join("\n",$etc_files[$file_to_edit]);
		} else {
			$previous_contents = "";
		}
	}
}

#get portage file list here
$file_list["portage/package.mask"] = "A list of DEPEND atoms to mask.";
$file_list["portage/package.unmask"] = "A list of packages to unmask.";
$file_list["portage/package.keywords"] = "Per-package KEYWORDS (like ACCEPT_KEYWORDS).";
$file_list["portage/package.use"] = "Per-package USE flags.";
$smarty->assign('file_list',$file_list);
$smarty->assign('error_msg',$error_msg);
$smarty->assign('previous_contents',$previous_contents);
$smarty->display('etc_portage.tpl');

?>
