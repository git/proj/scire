<?php
include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;
#phpinfo();
$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$error_msg = "";

if ($_POST['packages']) {
	$ip->set("install_packages", $_POST['packages']);
	#SERIALIZE
	$fh = fopen('test.xml','w');
	fwrite($fh, $ip->serialize());
	fclose($fh);
}
if ($_POST['manualpackages']) {
	$packages = explode(',',$_POST['manualpackages']);
	if ($packages) {
		$ip->set("install_packages", $packages);
		#SERIALIZE
		$fh = fopen('test.xml','w');
		fwrite($fh, $ip->serialize());
		fclose($fh);
	}
}
if (!$error_msg) {
	$error_msg = "Values saved successfully";
}
$smarty->assign('error_msg',$error_msg);

$install_packages = array();
$install_packages = $ip->get("install_packages");
print "Install Packages:";
var_dump($install_packages);
$smarty->assign('install_packages',$install_packages);
$smarty->assign('current_package_list', join(',',$install_packages));

$grp_list = get_grp_pkgs_from_cd();
$package_list = get_install_package_list();
$smarty->assign('grp_list',$grp_list);
$smarty->assign('package_list',$package_list);

$show_groups = array();
#figure out how to show groups
foreach ($_POST as $item => $param) {
	if( stristr($item,'show') and ($param == "Expand")) {
		$show_groups[substr($item, 5)] = "Expand";
	}
}
#print_r($show_groups);
$smarty->assign('show_groups',$show_groups);

$smarty->display('extrapackages.tpl');
?>
