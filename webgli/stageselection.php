<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$error_msg = "";

if ($_POST['savestage']) {
#	print "YES I CLICKED SAVE<br>"
	if ($_POST['stage']) {
#		print "YES THERE IS A STAGE<br>"
		if ($_POST['stage'] == "3+GRP") {
			$_POST['stage'] = "3";
			$ip->set("grp_install", True) or
				$error_msg .= "ERROR COULD NOT SET GRP INSTALL";
		} else {
			$ip->set("grp_install", False) or
				$error_msg .= "ERROR COULD NOT SET GRP INSTALL";
		}
		$ip->set("install_stage", $_POST['stage']) or
			$error_msg .= "ERROR: could not set the install stage";
	}
	if ($_POST['tarballuri']) {
		$ip->set("stage_tarball_uri", $_POST['tarballuri']) or
			$error_msg .= "ERROR: Could not set the tarball URI";
		if ($_POST['dynamic']) {
			$ip->set("dynamic_stage3",True);
			$ip->set("portage_tree_sync_type","snapshot");
			//	cd_snapshot_uri = GLIUtility.get_cd_snapshot_uri()
			$ip->set("portage_tree_snapshot_uri",$cd_snapshot_uri);
		} else {
			$ip->set("dynamic_stage3",False) or
				$error_msg .= "ERROR: Could not set dynamic stage 3.<br>";
		}
	}
	#SERIALIZE
	file_put_contents('test.xml', $ip->serialize()); #PHP 5 only
}
$smarty->assign('stage',$ip->get("install_stage"));
$smarty->assign('grp_install',$ip->get("grp_install"));
$smarty->assign('dynamic',$ip->get("dynamic_stage3"));
$smarty->assign('tarball', $ip->get("stage_tarball_uri"));
$smarty->display('stageselection.tpl');
?>
