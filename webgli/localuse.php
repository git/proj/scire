<?php

include('webgliIP.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');

$smarty->assign('dynamic',$ip->get("dynamic_stage3"));

$error_msg = "";
$global_use_flags = "";
$total_flags = "";
$original_flags = "";

if ($_POST['saveglobaluse']) {
	if ($_POST['flags']) {
		$global_use_flags = join(' ',$_POST['flags']);
		$global_use_flags .= " "; #extra space for next screen.
		$smarty->assign('global_use_flags',$global_use_flags);
#		$ip->set("etc_files",$etc_files);
#		file_put_contents('test.xml', $ip->serialize());  #PHP 5 only
	}
}

if ($_POST['savelocaluse']) {
	if ($_POST['flags']) {
		$local_use_flags = join(' ',$_POST['flags']);
	}
	if ($_POST['globalflags']) {
		$total_flags = $_POST['globalflags'] . $local_use_flags;
	} else {
		$total_flags = $local_use_flags;
	}
	#get the make.conf
	$etc_files = $ip->get("etc_files");
	if (array_key_exists("make.conf",$etc_files)) {
		$etc_files['make.conf']['USE'] = $total_flags;
	}
	else {
		$etc_files['make.conf'] = array();
		$etc_files['make.conf']['USE'] = $total_flags;
	}
	$ip->set("etc_files",$etc_files);
}
$use_local_desc = get_local_use_flags();
$smarty->assign('use_local_desc',$use_local_desc);

$smarty->display('localuse.tpl');
?>
