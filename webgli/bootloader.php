<?php

include('webgliIP.php');
include('webgliCF.php');
include('webgliUtility.php');

include('Smarty.class.php');
$smarty = new Smarty;

$smarty->template_dir = './templates';
$smarty->compile_dir = './templates_c';
$smarty->cache_dir = './cache';
$smarty->config_dir = './configs';

$ip = new InstallProfile();
$ip->parse('test.xml');
$cf = new ClientConfiguration();
$cf->parse('testcc.xml');

$error_msg = "";

if ($_POST['setbootloader']) {
	if ($_POST['bootloader']) {
		$ip->set("bootloader_pkg",$_POST['bootloader']); 
		$error_msg .= "ERROR: Could not set the bootloader pkg!";
	}
	if ($_POST['bootmbr']) {
		$ip->set("bootloader_mbr",True);
		$error_msg .= "ERROR: Could not set the bootloader MBR flag to TRUE";
		if ($_POST['boot_drive_choice']) {
			$ip->set("boot_device",$_POST['boot_drive_choice']);
			$error_msg .= "ERROR! Could not set the boot device! ".$_POST['boot_drive_choice'];
		}
	}
	else {
		$ip->set("bootloader_mbr",False);
		$error_msg .= "ERROR: Could not set the bootloader MBR flag to FALSE.";
	}
	if ($_POST['bootargs']) {
		$ip->set("bootloader_kernel_args",$_POST['bootargs']);
		$error_msg .= "ERROR: Could not set the bootloader kernel arguments!";
	}
	
	if (!$error_msg) {
		$error_msg = "Values saved successfully";
	}
}
$arch = $cf->get("architecture_template");

$boot_device = $ip->get("boot_device");
	#Bootloader code yanked from the x86ArchTemplate
	$devices = $ip->get("partition_tables");
	$foundboot = False;
	$drives = array_keys($devices);
	sort($drives);
	foreach ($drives as $drive) {
		$partlist = $devices[$drive];
		sort($partlist);
		foreach( $partlist as $part) {
			if ($part["mountpoint"] == "/boot") {
				print "found! <br>";
				$boot_device = $drive;
				$foundboot = True;
			}
			if ( ($part["mountpoint"] == "/") and (!$foundboot)) {
				$boot_device = $drive;
				print "set! $boot_device<br>";
			}
		}
	}
}
print "Boot device: $boot_device <br>";
print substr($boot_device,-1);
/*
bootloader = shared_info.install_profile.get_boot_loader_pkg()
$arch_loaders = array('x86': [
		("grub",(u"GRand Unified Bootloader, newer, RECOMMENDED")),
		("lilo",(u"LInux LOader, older, traditional.(detects windows partitions)"))],
	'amd64': [
		("grub",(u"GRand Unified Bootloader, newer, RECOMMENDED"))]} #FIXME ADD OTHER ARCHS
boot_loaders = arch_loaders[arch]
boot_loaders.append(("none", (u"Do not install a bootloader.	(System may be unbootable!)")))
bootargs = shared_info.install_profile.get_bootloader_kernel_args()
*/
  $bootloaders = array();
  array_push($bootloaders, array("grub", "GRand Unified Bootloader, newer, RECOMMENDED") );
  array_push($bootloaders, array("lilo", "LInux LOader, older, traditional.(detects windows partitions)") );

  $smarty->assign('boot_device', $boot_device);
  $smarty->assign('bootargs', $bootargs);
  $smarty->assign('bootloaders', $bootloaders);
  $smarty->assign('advanced', True); #FIXME
	
	$smarty->display('bootloader.tpl');
?>
