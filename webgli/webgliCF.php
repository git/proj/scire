<?
require_once('XMLParser.php');

class ClientConfiguration extends XMLParser {

	function ClientConfiguration() {
		$this->xmldoc = "";

		$this->values = array(
			'architecture_template' => array( 'type' => 'string', 'value' => 'x86' ),
			'dns_servers' => array( 'type' => 'array', 'value' => array() ),
			'enable_ssh' => array( 'type' => 'bool', 'value' => 0 ),
			'ftp_proxy' => array( 'type' => 'string', 'value' => "" ),
			'http_proxy' => array( 'type' => 'string', 'value' => "" ),
			'install_mode' => array( 'type' => 'string', 'value' => "" ),
			'interactive' => array( 'type' => 'bool', 'value' => 0 ),
			'kernel_modules' => array( 'type' => 'array', 'value' => array() ),
			'log_file' => array( 'type' => 'string', 'value' => "/var/log/installer.log" ),
			'network_broadcast' => array( 'type' => 'string', 'value' => "" ),
			'network_dhcp_options' => array( 'type' => 'string', 'value' => "" ),
			'network_interface' => array( 'type' => 'string', 'value' => "" ),
			'network_ip' => array( 'type' => 'string', 'value' => "" ),
			'network_gateway' => array( 'type' => 'string', 'value' => "" ),
			'network_netmask' => array( 'type' => 'string', 'value' => "" ),
			'network_type' => array( 'type' => 'string', 'value' => "" ),
			'profile_uri' => array( 'type' => 'uri', 'value' => "" ),
			'root_mount_point' => array( 'type' => 'string', 'value' => "/mnt/gentoo" ),
			'root_passwd' => array( 'type' => 'string', 'value' => "" ),
			'rsync_proxy' => array( 'type' => 'string', 'value' => "" ),
			'verbose' => array( 'type' => 'bool', 'value' => 0 ),
		);

		$this->xmlpaths = array(
			'client-configuration/architecture-template' => 'architecture_template',
			'client-configuration/dns-servers' => 'dns_servers',
			'client-configuration/enable-ssh' => 'enable_ssh',
			'client-configuration/ftp-proxy' => 'ftp_proxy',
			'client-configuration/http-proxy' => 'http_proxy',
			'client-configuration/install-mode' => 'install_mode',
			'client-configuration/interactive' => 'interactive',
			'client-configuration/kernel-modules' => 'kernel_modules',
			'client-configuration/log-file' => 'log_file',
			'client-configuration/network-broadcast' => 'network_broadcast',
			'client-configuration/network-interface' => 'network_interface',
			'client-configuration/network-ip' => 'network_ip',
			'client-configuration/network-dhcp-options' => 'network_dhcp_options',
			'client-configuration/network-gateway' => 'network_gateway',
			'client-configuration/network-type' => 'network_type',
			'client-configuration/profile-uri' => 'profile_uri',
			'client-configuration/root-mount-point' => 'root_mount_point',
			'client-configuration/root-passwd' => 'root_passwd',
			'client-configuration/rsync-proxy' => 'rsync_proxy',
			'client-configuration/verbose' => 'verbose',
		);
	}

	function serialize() {
		return $this->_serialize("client-configuration");
	}

}
?>
